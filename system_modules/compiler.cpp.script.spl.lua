spl_require("compiler")

function compiler_cpp()
    local compilerName = spl_get_variable("build.use_cpp_compiler", "any")
    return spl_get("compiler", compiler_target(), compiler_host(), "cpp_compiler", compilerName)[1]
end

function compiler_cpp_runtime(profile)
    profile = profile or compiler_profile()
    local runtime = compiler_runtime()
    if runtime == "static" then 
        if profile == "fulldebug" then
            return "#RuntimeStaticDebug" 
        else
            return "#RuntimeStatic" 
        end
    else
        if profile == "fulldebug" then
            return "#RuntimeDynamicDebug"
        else
            return "#RuntimeDynamic"
        end
    end
end

function compiler_get_variant_folder()
    return "^variant_" .. spl_get("mod_variant") .. "/"
end