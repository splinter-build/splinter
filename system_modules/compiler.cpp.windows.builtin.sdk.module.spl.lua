if not spl_get_variable_bool("build.cpp.allow.builtin", true) then return end

spl_require('compiler')
if compiler_host() ~= "windows" then return end

-- Scan for folders
local sdkversion = nil
local sdkpth = nil
local pathsToTry = { 
        "C:\\Program Files (x86)\\Windows Kits\\10",
}
for _, pthtry in  ipairs(pathsToTry) do
        local subdirs = spl_get("path_dirs", spl_get("path_merge", pthtry, "Include") )
        table.sort(subdirs)
        
        -- check if path has valid header
        if #subdirs > 0 then
                for i=#subdirs, 1,-1 do
                        local subpth = subdirs[i]
                        local npth = spl_get("path_merge", pthtry, "Include", subpth, "um\\winppi.h")
                        if spl_get("path_exists", npth) then
                                -- set final paths 
                                sdkpth = pthtry
                                sdkversion = subpth
                                break
                        end
                end
                if sdkversion ~= nil then break end
        end
end

if sdkversion == nil then spl_log("Could not detect built-in C++ windows SDK.") return end

spl_add_compiler { 
	name = "winsdk-" .. sdkversion, type = "cpp_sdk", target = "windows", prio = 19041, pretty_name = "Native Windows 10 SDK - 10.0.19041.0",
	variables = { version = sdkversion },
	sdk = { type = "winsdk", 
	  includes = { sdkpth .. "/Include/$version/um", sdkpth .. "/Include/$version/ucrt", sdkpth .. "/Include/$version/shared", sdkpth .. "/Include/$version/winrt" }, 
	  libs_x86 = { sdkpth .. "/Lib/$version/ucrt/x86", sdkpth .. "/Lib/$version/um/x86" }, 
	  libs_x64 = { sdkpth .. "/Lib/$version/ucrt/x64", sdkpth .. "/Lib/$version/um/x64" } 
	}
}