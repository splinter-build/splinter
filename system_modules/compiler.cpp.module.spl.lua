-- CPP SDKs (try packages, or use builtin versions)
spl_require('compiler')

if compiler_host() == "windows" then
        if spl_get("mod_location", "compiler.cpp.windows") ~= nil then
                spl_module({ requires = { "compiler.cpp.windows" } })
        else
                spl_module({ requires = { "compiler.cpp.windows.builtin.cl" } })
        end
end
if compiler_host() == "windows" or compiler_target() == "windows"  then
        if spl_get("mod_location", "compiler.cpp.windows.sdk") ~= nil then
                spl_module({ requires = { "compiler.cpp.windows.sdk" } })
        else
                spl_module({ requires = { "compiler.cpp.windows.builtin.sdk" } })
        end
end
if compiler_target() == "emscripten" and compiler_host() == "windows" then
        spl_module({ requires = { "compiler.cpp.windows.emscripten" } })
end

-- Builtin CPP compilers for Linux/Darwin
if spl_get_variable_bool("build.cpp.allow.builtin", true) then 
        if compiler_host() == "linux" or compiler_host() == "mac" then
                spl_module({ requires = { "compiler.cpp.unix.builtin" } })
        end
end

spl_module({
        description="C++ Compiler",
        action="../system_actions/cpp.lua"
})
