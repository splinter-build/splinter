spl_require('compiler')

local clangPath = spl_get("which", "clang")
local clangClPath = spl_get("which", "clang-cl")
local gccPath = spl_get("which", "gcc")
local ldPath = spl_get("which", "ld")             
local host = compiler_host()

-- support mac os gcc convention (no support for groups)
local convention = "gcc"
if host == "mac" then convention = "gcc_mac" end

if gccPath ~= nil then
        spl_add_compiler { name = "host-clang-gcc", type = "cpp_compiler", host_os = host, target = host, prio = 1, pretty_name = "Native compiler - GCC (" .. host .. "-x64)", path = { spl_get("path_folder", gccPath), spl_get("path_folder", ldPath)  },
                compiler = { cc = "gcc", cxx = "g++", link = "g++", lib = "ar", convention = convention, type = "gcc" } }
end
if clangPath ~= nil then
        spl_add_compiler { name = "host-clang-linux", type = "cpp_compiler", host_os = host, target = host, prio = 2, pretty_name = "Native compiler - Clang++ (" .. host .. "-x64)", path = { spl_get("path_folder", clangPath), spl_get("path_folder", ldPath) },
                compiler = { cc = "clang", cxx = "clang++", link = "clang++", lib = "ar", convention = convention, type = "clang" } }
end
if clangClPath ~= nil then
        local lldLinkPath = spl_get("which", "lld-link")
        spl_add_compiler { name = "host-clang-linux-to-windows", type = "cpp_compiler", host_os = "linux", target = "windows", prio = 1, pretty_name = "Native compiler - Clang++ (windows-x64)", path = { spl_get("path_folder", clangClPath), spl_get("path_folder", lldLinkPath) },
                compiler = { cc = "clang-cl", cxx = "clang-cl", link = "lld-link", lib = "llvm-lib", convention = "clangcl", type = "clang"} }
end