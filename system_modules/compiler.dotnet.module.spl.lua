-- Windows
spl_add_compiler {
  type="csharp_compiler", host_os="windows", name="windows-netcoreapp3.0", framework="netcoreapp3.0", prio=300.8, pretty_name=".NET Core 3 Compiler (preview-8)", 
  path= { "dotnetcore" }, source="splCompilerWindowsDotNetCore3"
}

-- Linux
spl_add_compiler {
  type="csharp_compiler", host_os="linux", name="linux-netcoreapp3.0", framework="netcoreapp3.0", prio=300.8, pretty_name=".NET Core 3 Compiler (preview-8)", 
  path= { "dotnetcore" }, source="splCompilerLinuxDotNetCore3"
}

-- Mac OS X
spl_add_compiler {
  type="csharp_compiler", host_os="darwin", name="darwin-netcoreapp3.0", framework="netcoreapp3.0", prio=300.8, pretty_name=".NET Core 3 Compiler (preview-8)", 
  path= { "dotnetcore" }, source="splCompilerDarwinDotNetCore3"
}

spl_module({
  description=".NET Core build resolver",
  requires={"compiler.ninja"},
  action="../system_actions/dotnet.lua"
})