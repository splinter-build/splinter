if not spl_get_variable_bool("build.cpp.allow.builtin", true) then return end

spl_require('compiler')
if compiler_host() ~= "windows" then return end

-- Scan for folders
local pth = nil
local pathsToTry = { }
local supported_vs_versions = { "2021", "2019", "2017" }
for _, version in ipairs(supported_vs_versions) do
        table.insert(pathsToTry, "C:\\Program Files (x86)\\Microsoft Visual Studio\\" .. version .. "\\Enterprise\\VC\\Tools\\MSVC")
        table.insert(pathsToTry, "C:\\Program Files (x86)\\Microsoft Visual Studio\\" .. version .. "\\Professional\\VC\\Tools\\MSVC")
        table.insert(pathsToTry, "C:\\Program Files (x86)\\Microsoft Visual Studio\\" .. version .. "\\Community\\VC\\Tools\\MSVC")
        table.insert(pathsToTry, "C:\\Program Files (x86)\\Microsoft Visual Studio\\" .. version .. "\\BuildTools\\VC\\Tools\\MSVC")
end

for _, pthtry in  ipairs(pathsToTry) do
        local subdirs = spl_get("path_dirs", pthtry)
        table.sort(subdirs)
        
        -- check if path has valid cl.exe
        if #subdirs > 0 then
                local subpth = subdirs[#subdirs]
                local npth = spl_get("path_merge", pthtry, subpth, "bin\\Hostx64\\x64\\cl.exe")
                if spl_get("path_exists", npth) then
                        -- set final path
                        pth = spl_get("path_merge", pthtry, subpth)
                        break
                end
        end
end

if pth == nil then 
        spl_log("Failed to detect built-in C++ windows cl.exe compiler (VS2022/2019/2017).")
else
        -- Add compilers
        spl_add_compiler { name = "msvc-link-windows", type = "cpp_compiler", host_os = "windows", target = "windows", prio = 9, pretty_name = "Native MSVC CL/Link (Windows)", 
                compiler={ cc = "cl", link = "link", lib = "lib", convention = "msvc", type = "cl"}, path = {}, path_x64 = { pth .. "\\bin\\Hostx64\\x64"}, path_x86 = {pth .. "\\bin\\Hostx64\\x86"} }

        spl_add_compiler { 
                name = "builtin-vs-sdk", type = "cpp_sdk", target = "windows", prio = 10, pretty_name = "Native Visual Studio SDK",
                variables = { version = "builtin" },
                sdk = { type = "buildtools", 
                includes = { pth .. "\\include", pth .. "\\atlmfc\\include"}, 
                libs_x86 = { pth .. "\\lib\\x86", pth .. "\\atlmfc\\lib\\x86" }, 
                libs_x64 = { pth .. "\\lib\\x64", pth .. "\\atlmfc\\lib\\x64" } } 
        }
end

-- Scan for LLVM
local llvmPath = "C:\\Program Files\\LLVM\\bin"
if spl_get("path_exists", spl_get("path_merge", llvmPath, "clang-cl.exe")) then
        spl_add_compiler { name = "clang-lld-windows", type = "cpp_compiler", host_os = "windows", target = "windows", prio = 10, pretty_name = "Native Clang++/LLD-link (Windows)", 
                compiler = {cc = "clang-cl", link = "lld-link", lib = "llvm-lib", convention = "clangcl", type = "clang"}, path = {llvmPath} }
end