pathsep = spl_get("os_pathsep")
function compiler_profile()
    local profile = spl_get_variable("build.env.profile", "final"):lower()
    return profile
end
function compiler_symbols()
    local symbols = spl_get_variable_bool("build.env.symbols", true)
    if symbols == true then return {"#Symbols"} else return {} end
end
function compiler_profile_flags(profile)
    local l_flags = {}
    profile = profile or compiler_profile()
    if profile == "debug" or profile == "fulldebug" then
        table.insert(l_flags, "#OptimizeOff")
        table.insert(l_flags, "#NoInline")
        table.insert(l_flags, "#NoASLR")
    else
        table.insert(l_flags, "#OptimizeSpeed")
        table.insert(l_flags, "#NoFramePointer")
        table.insert(l_flags, "#NoBufferSecurityCheck")
    end
    return l_flags
end
function compiler_runtime()
    local runtime = spl_get_variable("build.env.runtime", "dynamic"):lower()
    return runtime
end
function compiler_target_arch()
    local arch = spl_get_variable("build.env.arch", "x64"):lower()
    if arch == "32" or arch == "x32" or arch == "x86" then return "x86" end
    if arch == "64" or arch == "x64" then return "x64" end
    return arch
end
function compiler_target()
    if module_variant() == "host" then return compiler_host() end -- special case, host variant tries to build the whole thing for the host.
    return spl_get_variable("build.env.target", spl_get("os_name")):lower()
end
function compiler_host()
    return spl_get_variable("build.env.host", spl_get("os_name")):lower()
end
function module_name()
    return spl_get("mod_name_sanitized")
end
function module_basename()
    return spl_get("mod_basename")
end
function module_variant()
    return spl_get("mod_variant")
end
function module_variant_suffix()
    local variant = spl_get("mod_variant")
    if variant ~= "" then return ":" .. variant end
    return ""
end
function module_path()
    return spl_get("mod_path")
end
