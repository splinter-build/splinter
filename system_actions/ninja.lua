-- Ninja Build Output System

-- Module definition:
-- module.build = {}
-- module.build.variables = { foo = "bar"}                                      -- Define variables for use in rules or build process, that can be refered to using ${foo}
-- module.build.rules = { foo_rule= { command="echo foo: ${in} > ${out}" } }    -- Define build rules referred to by key name (see ninja rule documentation: https://ninja-build.org/manual.html#ref_rule )
-- module.build.outputs = { { 
--   rule="foo_rule", outputs={"foo.out"}, inputs={"foo.in"}, 
--   implicit_inputs={"foo.impl.in"}, implicit_outputs={"foo.impl.out"}, 
--   order_only_inputs={"foo.order.in"}, variables={bar="baz"}
--  }, { <another one here> }, ...}                                             -- Define build outputs as an array. (see ninja build documentation: https://ninja-build.org/manual.html#ref_outputs https://ninja-build.org/manual.html#ref_dependencies )

local action = {}

function action.init(state)
    state.ninja = {}
    state.ninja.subninjas = {}
    state.ninja.host_os = settings.getSetting("build.env.host", utils.os_name):lower()
    state.ninja.seperator = utils.get_path_seperator(state.ninja.host_os)
    action.clearOutput(state)

    -- set paths
    state.ninja.path = scripts.get_build_dir()
    state.ninja.subpathPrefix = "ninja"
    state.ninja.subpath = utils.path_merge(state.ninja.path, state.ninja.subpathPrefix)

    utils.path_mkdir_recur(state.ninja.subpath)
end
function action.sanitize_name(name)
    return settings.sanitize_build_output_filename_specials_only(name)
end
function action.unload(state)
    log_dbg("Ninja: Writing build.ninja")

    -- write build output
    action.clearOutput(state)
    local ninjaPath = state.ninja.path
    local ninjaBuildFilePath = utils.path_merge(ninjaPath, "build.ninja")
    for _, subninja in ipairs(state.ninja.subninjas) do 
        action.ninja_include(state, state.ninja.subpathPrefix .. state.ninja.seperator .. subninja, true)
    end
    utils.file_write(ninjaBuildFilePath, "w", action.output(state))
	
    -- execute ninja if desired
	local run = settings.getSetting("build.ninja.execute", "true") == "true"
	local args = settings.getSetting("build.ninja.args", "") 
	if run then action.run_ninja(state, args) end
end
function action.append(state, x)
    -- Method 1: Just add to outputblock
    table.insert(state.ninja.outputblock, x)
end
function action.output(state)
    table.insert(state.ninja.outputblock, state.ninja.output)
    state.ninja.output = ""
    return table.concat(state.ninja.outputblock)
end
function action.clearOutput(state)
    state.ninja.output = ""
    state.ninja.outputblock = {}
end
function action.escape(str)
    -- fairly slow (1M iterations in 1.6 seconds on a 40 bytes string)
    -- should be optimized using C
    if type(str) == "table" then print(debug.traceback()) end
    str = str or ""
    str = string.gsub(str,"$([^{])", "$$%1")
    str = string.gsub(str,"([ :])", "$%1")
    return str
end
function action.escapeTable(tbl)
    local ret = {}
    for _, v in ipairs(tbl) do
        table.insert(ret, action.escape(v))
    end
    return ret
end
function action.run_ninja(state, args)
	args = args or ""
	log_dbg("Ninja: Ninja arguments: " .. args)
	local cwd = utils.path_cwd()
    utils.path_chdir(state.ninja.path)
    log_dbg("Ninja: Ninja path: " .. state.ninja.path)
    
    log_info("Ninja: Executing..")

	-- [OS SPECIFIC]
	local suffix = ""
    if state.ninja.host_os == "windows" then suffix = ".exe" end
    local cmd = utils.path_abs(utils.path_merge(gDirs.base_buildsystem, "external/pkg/", "ninja" .. suffix)) .. " " .. args
    log_dbg("Ninja: Command: " .. cmd)
	local ret, state, errorcode = os.execute(cmd)
    actions.build.setLastBuildStatus(errorcode) -- notify build action of latest status
	utils.path_chdir(cwd)
	log_info("Ninja: Done.")
end
function action.ninja_vars(state, vars)
    local sort = {}
    for k,v in pairs(vars) do
        table.insert(sort, k)
    end
    table.sort(sort)
    for _,k in ipairs(sort) do
        local v = vars[k]
        action.append(state, ( k .. " = " .. v .. "\n"))
    end
end
function action.ninja_rule(state, rulename, vars)
    local output = "rule " .. rulename .. "\n"
    for k,v in pairs(vars) do
        output = output .. ("  " .. k .. " = " .. v .. "\n")
    end
    action.append(state, output)
end
function action.ninja_include(state, file, newScope)
    local output = ""
    if newScope == true then output = "subninja " else output = "include " end
    output = output .. file .. "\n"
    action.append(state, output)
end
function action.ninja_build(state, rule, explicit_outputs, explicit_deps, vars, implicit_outputs, implicit_deps, order_only_deps)
    local output = "build "
    if explicit_outputs then 
        output = output .. table.concat(action.escapeTable(explicit_outputs), " ")
    end
    if implicit_outputs then 
        output = output .. " | " .. table.concat(action.escapeTable(implicit_outputs), " ")
    end
    output = output .. ": " .. (rule or "null") .. " "

    if explicit_deps then 
        output = output .. table.concat(action.escapeTable(explicit_deps), " ")
    end
    if implicit_deps then 
        output = output .. " | " .. table.concat(action.escapeTable(implicit_deps), " ")
    end
    if order_only_deps then 
        output = output .. " || " .. table.concat(action.escapeTable(order_only_deps), " ")
    end
    output = output .. "\n"
    if vars then
        local sort = {}
        for k,v in pairs(vars) do
            table.insert(sort, k)
        end
        table.sort(sort)
        for _,k in ipairs(sort) do
            local v = vars[k]
            output = output .. ("  " .. k .. " = " .. v .. "\n")
        end
    end
    action.append(state,output)
end
function action.execute(state)
    action.clearOutput(state)
    log_dbg("Ninja: Processing", state.module.name, "from", state.module_action.name)
    
    if state.module.build then
        -- bin directory
        local vbinWithoutSep = "bin"
        local vbin = vbinWithoutSep .. utils.os_pathsep
        local vbin_ext = ""
        local vlib_ext = ""
        if state.ninja.host_os == "windows" then
            vbin_ext = ".exe"
            vlib_ext = ".lib"
        end

        -- inject standard variables
        local stdVars = {
            bin = vbin,
            bin_folder = vbinWithoutSep,
            bin_ext = vbin_ext,
            lib_ext = vlib_ext,
            mod_name = settings.sanitize_build_output_filename_specials_only(state.module.name),
            mod_basename = settings.sanitize_build_output_filename_specials_only(state.module.basename),
            module = utils.path_normalize(utils.path_rel(state.ninja.path, state.module.path), state.ninja.seperator)  -- relative path
        };

        action.ninja_vars(state, stdVars)

        -- set variables
        if state.module.build.variables then
            action.ninja_vars(state, state.module.build.variables)
        end

        -- add build rules
        if state.module.build.rules then
            for k,rule in pairs(state.module.build.rules) do
                local origCommand 
                if state.ninja.host_os == "windows" and rule.command then
                    origCommand = rule.command
                    rule.command = "cmd.exe /C " .. rule.command
                end
                action.ninja_rule(state, k, rule)
                if state.ninja.host_os == "windows" and rule.command then 
                    rule.command = origCommand
                end
            end
        end
    
        -- add build outputs
        if state.module.build.outputs then
            for _, output in ipairs(state.module.build.outputs) do
                action.ninja_build(state, output.rule, output.outputs, output.inputs, output.variables, output.implicit_outputs, output.implicit_inputs, output.order_only_inputs)
            end
        end
    end

    -- write module output
    local ninjaPath = state.ninja.subpath
    local ninjaFilename = settings.sanitize_build_output_filename_specials_only(state.module.name) .. ".ninja"
    local ninjaFilePath = utils.path_merge(ninjaPath, ninjaFilename )
    
    --local ninjaFilePath = utils.path_merge(ninjaPath, "build.ninja")
    utils.file_write(ninjaFilePath, "w", action.output(state))

    -- insert subninja
    table.insert(state.ninja.subninjas, ninjaFilename)
end
return action