-- Build Installer

-- Module definition:
-- module.install = {}
-- table.insert(module.install, {
--    -- destination folder, defaults to <build_folder>/bin
--    destination = "${bin}"  
--    -- copy sources, relative to module path [flattened]
--    sources = { "file_in_module_path.txt", "another file.txt", { "subfolder/other files.txt"}} 
--    -- copy sources, relative to build folder [flattened]
--    sources_abs = { "${bin}some_project_target.exe" } 
-- }    

local action = {}

local installerRules = {
    windows = {
        command="copy /Y \"${in}\" \"${out}\" > NUL",
        meta = { seperator="\\" }
    },
    unix = {
        command="install -m 644 -D \"${in}\" \"${out}\"",
        meta = { seperator="/" }
    },
    mac = {
        command="install -m 644 \"${in}\" \"${out}\"",
        meta = { seperator="/" }
    }
}
installerRules.linux = installerRules.unix
installerRules.bsd = installerRules.unix
installerRules.mac = installerRules.mac

function action.init(state)
end
function action.unload(state)
end
function action.execute(state)
    local host_os = settings.getSetting("build.env.host", utils.os_name):lower()
    log_dbg("Installer: Generating install build rules for", state.module.name, "(host OS: '".. host_os .."')")  

    local installRule = utils.deep_copy(installerRules[host_os])
    installRule.description = "COPY \"$in\" to \"$out\""
    if(installRule == nil) then
        log_err("Installer: Unsupported OS: '" .. host_os .. "'. Please open installer.lua and add support for this platform.")
    end

    -- ** GRAB META AND REMOVE FROM RULE SO THAT BUILDER DOESN'T SEE THEM
    local meta = installRule.meta
    installRule.meta = nil
    
    -- ** GENERATE BUILD RULES
    if state.module.build == nil then state.module.build = {} end
    if state.module.build.rules == nil then state.module.build.rules = {} end
    if state.module.build.rules.install == nil then state.module.build.rules.install = installRule end
    if state.module.build.outputs == nil then state.module.build.outputs = {} end

    -- Now generate the rules
    for _, srcInstall in pairs(state.module.install or {}) do
        
        local output = utils.path_normalize(srcInstall.destination or "${bin}", meta.seperator)
        
        -- Add sources
        local files = utils.arr_flatten(srcInstall.sources or {})
        for _, src in ipairs(files) do
            local buildRule = { rule = "install", outputs = { output .. meta.seperator .. utils.path_filename(src) }, inputs = {} }
            table.insert(buildRule.inputs, "${module}" .. meta.seperator .. utils.path_normalize(src, meta.seperator))
            table.insert(state.module.build.outputs, buildRule)
        end

        -- Add absolute sources
        local files = utils.arr_flatten(srcInstall.sources_abs or {})
        for _, src in ipairs(files) do
            local buildRule = { rule = "install", outputs = { output .. meta.seperator .. utils.path_filename(src) }, inputs = {} }
            table.insert(buildRule.inputs, utils.path_normalize(src, meta.seperator))
            table.insert(state.module.build.outputs, buildRule)
        end
    end
end
return action