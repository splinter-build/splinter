local action = {}

function action.init(state)
    state.cpp = {}
    state.cpp.compilers = {}
    state.cpp.generate_build = settings.getSetting("build.cpp.build", "true") == "true"
    action.cpp_get_compiler_cached(state)
end

-- description: handle pch (pre computed headers) support
function action.compiler_handle_pch(state, compiler, proxy, lc_compiler_state)
    -- handle pch
    if compiler.pch ~= nil then
        local pchFile = "pch/${mod_name}/" .. compiler.pch.file
        local pch_create_flags, pch_use_flags
        if proxy.convention == "msvc" then
            pch_create_flags = "/Yc" .. compiler.pch.header .. " " .. "/Fp" .. pchFile .. " "
            pch_use_flags = "/Yu" .. compiler.pch.header .. " " .. "/Fp" .. pchFile .. " "
        else
            log_dbg("Unsupported PDB compiler convention (supported: msvc)")
            return
        end

        -- generate pch when source and header are specified
        if compiler.pch.source and compiler.pch.header and compiler.pch.file then 
            -- insert pch compilation and update compiler flags to use pch
            action.insert_source(state, compiler, proxy, "${module}/", compiler.pch.source, pch_create_flags .. lc_compiler_state.ccflags, pch_create_flags .. lc_compiler_state.cppflags, "pch/${mod_name}/", {implicit_outputs={pchFile}})
        end

        -- use (generated or existing) pch
        if compiler.pch.header and compiler.pch.file then 
            -- insert precompiler header usage for other sources
            lc_compiler_state.ccflags = pch_use_flags .. lc_compiler_state.ccflags
            lc_compiler_state.cppflags = pch_use_flags .. lc_compiler_state.cppflags

            -- inject implicit input into compile table to make sure pch is built before everything else
            if(lc_compiler_state.mergeTable == nil) then lc_compiler_state.mergeTable = {} end
            if(lc_compiler_state.mergeTable.implicit_inputs == nil) then lc_compiler_state.mergeTable.implicit_inputs = {} end
            table.insert(lc_compiler_state.mergeTable.implicit_inputs, pchFile)
        end
    end
end

-- description: handle imports from compiler blocks
function action.compiler_handle_import(state, compiler, impModuleName, lc_whenRules)
    local importModule = state.ro_modules[impModuleName]
    local imported = action.get_imported_block(state, impModuleName, lc_whenRules)

    if type(imported.compiler) == "table" then
        -- handle directories
        if type(imported.compiler.directories) == "table" then
            if compiler.directories == nil then compiler.directories = {} end
            local directories = utils.arr_flatten(imported.compiler.directories)
            for i,j in ipairs(directories) do
                if j:sub(1,1) ~= "^" then
                    table.insert(compiler.directories, utils.path_merge(importModule.path, j))
                else
                    table.insert(compiler.directories, j)
                end
            end
        end

        -- handle defines
        if type(imported.compiler.defines) == "table" then 
            if compiler.defines == nil then compiler.defines = {} end
            for i,j in ipairs(imported.compiler.defines) do table.insert(compiler.defines, j) end
        end
    end
end

function action.execute(state)
    log_dbg("Cpp: " .. state.module.name .. ": Processing..")
    action.generate_build_rules(state, scripts.get_build_dir())
end

-- description: handle imports that include other imports
function action.expand_recursive_imports(state, imports, lc_whenRules, cache)
    -- traverse exports and make a list of all imports in sequential order
    local expanded = {}
    
    for _, impModuleName in ipairs(imports) do 
        local importModule = state.ro_modules[impModuleName]
        local imported = action.get_imported_block(state, impModuleName, lc_whenRules)
        if imported.imports ~= nil and type(imported.imports) == "table" then
            local subImports = utils.arr_flatten(imported.imports)
            local expandedImports = utils.arr_append(expanded, action.expand_recursive_imports(state, subImports, lc_whenRules, cache))
        end
        if cache[impModuleName] == nil then
            table.insert(expanded, impModuleName)
            cache[impModuleName] = 1 
        end
    end
    return expanded
end

-- description: get linker output filenames and paths
function action.get_linker_outputs(module, linker, lc_targetConventions, lc_compilerConventions)
    -- determine output type and implicit outputs
    local lc_modname = settings.sanitize_build_output_filename(module.name)
    local lc_libname = (linker.name or lc_modname)
    -- add prefix if needed
    if linker.type == "shared_library" or linker.type == "static_library" then 
        if lc_compilerConventions.libraryForceNamePrefix ~= nil and utils.str_starts_with(lc_libname,  lc_compilerConventions.libraryForceNamePrefix) == false then 
            lc_libname = lc_compilerConventions.libraryForceNamePrefix .. lc_libname
        end
    end
    -- other vars
    local lc_linker_output = "${bin}" .. lc_libname
    local lc_linker_implicit_output = {}
    local lc_lib 
    -- add suffix
    if linker.type == "application" then 
        lc_linker_output = lc_linker_output .. lc_targetConventions.executableSuffix 
    elseif linker.type == "shared_library" then 
        if lc_compilerConventions.dll_createsLib then
            table.insert(lc_linker_implicit_output, lc_linker_output .. lc_targetConventions.librarySuffix ) -- inject .lib file into implicit outputs for dependency graph reasons
            lc_lib = lc_linker_output .. lc_targetConventions.librarySuffix
            lc_libname = lc_libname .. lc_targetConventions.librarySuffix
        else
            lc_lib = lc_linker_output .. lc_targetConventions.dynamicLibrarySuffix
            lc_libname = lc_libname .. lc_targetConventions.dynamicLibrarySuffix
        end
        
        lc_linker_output = lc_linker_output .. lc_targetConventions.dynamicLibrarySuffix
    elseif linker.type == "static_library" then 
        lc_linker_output = lc_linker_output .. lc_targetConventions.librarySuffix 
        lc_libname = lc_libname .. lc_targetConventions.librarySuffix
        lc_lib = lc_linker_output
    end

    return {implicit=utils.iif(#lc_linker_implicit_output > 0, lc_linker_implicit_output, nil), outputs={lc_linker_output}, lib=lc_lib, libname=lc_libname}
end

-- description: find foreign import from the global state
function action.get_imported_block(state, impModuleName, lc_whenRules)
    -- find read only module
    local importModule = state.ro_modules[impModuleName]
    if importModule == nil then log_err_notrace("Cpp: " .. state.module.name, ": Failed to find import module ", impModuleName) return {} end
    if importModule.cpp == nil then return {} end
    if importModule.cpp.imported == nil then return {} end
    if type(importModule.cpp.imported.when) ~= "table" then
        -- return as is
        return importModule.cpp.imported
    else
        -- resolve when rules
        local imported = utils.deep_copy(importModule.cpp.imported)
        action.resolve_when(state, imported, lc_whenRules)
        return imported
    end
end

-- description: helper function to generate a combined list based on prefix and suffix.
--              supports recursion (table in table)
function action.generate_list(list, prefix, suffix)
    local ret = {}
    prefix = prefix or ""
    suffix = suffix or ""
    for _, v in ipairs(list or {}) do
        if type(v) == "table" then 
            table.insert(ret, action.generate_list(v, prefix, suffix))
        else
            table.insert(ret, prefix .. v .. suffix)
        end
    end
    return table.concat(ret, " ")
end

-- description: convert libs from libraries syntax to a library list
function action.generate_libs(state, proxy, libs)
    local list = utils.arr_flatten(libs or {})
    local ret = {}
    for _, v in ipairs(list) do
        local subprefix = v:sub(1,1)
        if subprefix == "@" then
            local subname = v:sub(2)
            if state.module.cpp.linker[subname] == nil then 
                log_err(state.module.name, ": Could not resolve lib ", v, " - Link not found.") 
            else
                local o=action.get_linker_outputs(state.module, state.module.cpp.linker[subname], proxy.targetConventions, proxy.compilerConventions )
                table.insert(ret, o.libname) -- add resolved library 
            end 
        elseif subprefix == "!" then 
            table.insert(ret, v:sub(2)) -- chop prefix, this is a marker saying it's a build output
        else
            table.insert(ret, v )
        end
    end
    return ret
end

-- description: convert libs from libraries syntax to a library dependency list
function action.generate_libs_implicit(state, proxy, libs)
    local list = utils.arr_flatten(libs or {})
    local ret = {}
    for _, v in ipairs(list) do
        local subprefix = v:sub(1,1)
        if subprefix == "@" then
            local subname = v:sub(2)
            if state.module.cpp.linker[subname] == nil then 
                log_err(state.module.name, ": Could not resolve lib ", v, " - Link not found.") 
            else
                local o=action.get_linker_outputs(state.module, state.module.cpp.linker[subname], proxy.targetConventions, proxy.compilerConventions )
                table.insert(ret, "${bin}" .. o.libname) -- add resolved library 
            end 
        elseif subprefix == "!" then 
            table.insert(ret, "${bin}" .. v:sub(2)) -- chop prefix, this is a marker saying it's a build output
        else
            -- no implicit deps for normal libs
        end
    end
    return ret
end

-- description: convert library directories to a new library directory list
function action.generate_lib_dirs(state, proxy, libdirs)
    local linkDirs = action.relativize_paths_to(state.module.path, proxy.buildFolder, libdirs)
    table.insert(linkDirs, "${bin_folder}")
    return linkDirs
end

-- description: convert flags from flag syntax to actual arguments to compiler/linker
function action.generate_flags(state, ret, flags, flagTranslator, globalFlags, handled)
    -- Flags syntax:
    -- #PredefinedFlag : Predefined flag based on flag translator. Silently ignored if flag is unsupported.
    -- @GlobalFlag : Include flags from global flags
    -- Any other flag: Directly passed to the compiler/ linker

    if(type(flags or {}) ~= "table") then log_err("Cpp flags need to be table type.") end

    for _, v in ipairs(flags or {}) do
        if type(v) == "table" then 
            action.generate_flags(state, ret, v, flagTranslator, globalFlags, handled) 
        else
            local firstChar = v:sub(1,1)
            if firstChar == "@" then
                action.generate_flags(state, ret, globalFlags[v:sub(2)], flagTranslator, globalFlags, handled)
            elseif firstChar == "#" then
                local translation = flagTranslator[v:sub(2)]
                if translation ~= nil then 
                    if type(translation) == "function" then translation = translation(state) end
                    table.insert(ret, translation) 
                    handled[v] = true
                end
            else
                table.insert(ret, v)
                handled[v] = true
            end
        end
    end
    return ret
end

-- description: generate formatted linker list
function action.generate_lib_list(state, proxy, k, linker)
    local libList = action.generate_libs(state, proxy, linker.libs)
    -- add prefix
    local str = (proxy.compilerConventions.libraryPrefix or "") 

    -- handle modifiers
    if proxy.compilerConventions.libraryRemovePrefix then
        libList = utils.arr_select(libList, function(x) if(utils.str_starts_with(x, proxy.compilerConventions.libraryRemovePrefix)) then return x:sub(4) else return x end end)
    end
    if proxy.compilerConventions.libraryRemoveExtension then
        libList = utils.arr_select(libList, function(x) return utils.path_noext(x) end)
    end

    -- append list
    str = str .. action.generate_list(libList, proxy.compilerConventions.libraryExplicit, "") 

    -- add suffix
    str = str .. (proxy.compilerConventions.librarySuffix or "") 
    return str
end

function action.sanitize_when(str)
    return utils.str_replace(str, "-", "_")
end

-- description: generate all the "when" flags that are active for the current state
function action.generate_when_rules(state, lc_rootCompiler)
    local ret = {}
    local arch = action.get_arch(state)
    local profile = (settings.getSetting("build.env.profile") or ""):lower()

    ret["profile_" .. action.sanitize_when(profile)] = true
    ret["arch_" .. action.sanitize_when(arch)] = true
    ret["compiler_" .. action.sanitize_when(lc_rootCompiler.cpp_compiler.name)] = true
    ret["target_" .. action.sanitize_when(lc_rootCompiler.target)] = true
    ret["targetarch_" .. action.sanitize_when(lc_rootCompiler.target) .. "_" .. action.sanitize_when(arch)] = true
    ret["host_" .. action.sanitize_when(lc_rootCompiler.host_os)] = true
    ret["type_" .. action.sanitize_when(lc_rootCompiler.cpp_compiler.compiler.type)] = true
    ret["convention_" .. action.sanitize_when(lc_rootCompiler.cpp_compiler.compiler.convention)] = true
    ret["linker_" .. action.sanitize_when(lc_rootCompiler.cpp_compiler.compiler.link)] = true
    return ret
end

-- description: add install_name for Mac OS X builds
function action.generate_dynamic_library_mac_install_name(state, proxy, linker, lc_linkerOutputs)
    if proxy.compilerConventions.libraryDynamicSetInstallName ~= nil and linker.type == "shared_library" then
        local libname = lc_linkerOutputs.libname
        return " " .. proxy.compilerConventions.libraryDynamicSetInstallName .. libname
    end
    return ""
end

-- description: generate build variables for the builder (ninja)
function action.generate_build_variables(state, proxy)
    -- skip if not generating build
    if state.cpp.generate_build == false then return end

    -- ** GENERATE BUILD IF NOT EXISTS
    if state.module.build == nil then state.module.build = {} end

    -- ** GENERATE RULES
    if state.module.build.rules == nil then state.module.build.rules = {} end
    if state.module.build.rules.cc == nil then state.module.build.rules.cc = proxy.compilerConventions.cc end
    if state.module.build.rules.cpp == nil then state.module.build.rules.cpp = proxy.compilerConventions.cpp end
    if state.module.build.rules.lib == nil then state.module.build.rules.lib = proxy.compilerConventions.lib end
    if state.module.build.rules.link == nil then state.module.build.rules.link = proxy.compilerConventions.link end

    -- ** GENERATE VARIABLES
    if state.module.build.variables == nil then state.module.build.variables = {} end

    -- add tools
    for k,v in pairs(proxy.scripts) do state.module.build.variables["tool_" .. k] = v end

    -- convert extra flags
    state.module.build.variables["cpp_extra_cppflags"] = proxy.rootCompiler.cppflags
    state.module.build.variables["cpp_extra_ldflags"] = proxy.rootCompiler.ldflags

    local handledFlags = {}

    local concat_arg = function(str) if str == nil then return "" else return str .. " " end end 

    -- convert compiler blocks
    for k, compiler in pairs(state.module.cpp.compiler) do   
        local allFlags = {proxy.rootCompiler.extraflags, compiler.flags} 
        state.module.build.variables[k .. "_cxx_flags"] = concat_arg(proxy.compilerConventions.extra_cxxflags) ..table.concat(action.generate_flags(state, {}, allFlags, proxy.compilerConventions.flags.cxx or {}, state.module.cpp.flags, handledFlags), " ")
        state.module.build.variables[k .. "_cpp_flags"] = concat_arg(proxy.compilerConventions.extra_cppflags) .. table.concat(action.generate_flags(state, {}, allFlags, proxy.compilerConventions.flags.cpp or {}, state.module.cpp.flags, handledFlags), " ")
        state.module.build.variables[k .. "_cc_flags"] = concat_arg(proxy.compilerConventions.extra_ccflags) ..table.concat(action.generate_flags(state, {}, allFlags, proxy.compilerConventions.flags.cc or {}, state.module.cpp.flags, handledFlags), " ")
        state.module.build.variables[k .. "_cpp_defines"] = action.generate_list(compiler.defines, proxy.compilerConventions.define, proxy.compilerConventions.defineSuffix or "")
        state.module.build.variables[k .. "_cpp_dirs"] = action.generate_list(action.relativize_paths_to(state.module.path, proxy.buildFolder, compiler.directories), proxy.compilerConventions.includedir, proxy.compilerConventions.includedirSuffix)
        state.module.build.variables[k .. "_z_cc_flags"] = "${" .. k .. "_cxx_flags} " .. "${" .. k .. "_cc_flags} " .. "${" .. k .. "_cpp_defines} " .. "${" .. k .. "_cpp_dirs} "
        state.module.build.variables[k .. "_z_cpp_flags"] = "${" .. k .. "_cxx_flags} " .. "${" .. k .. "_cpp_flags} " .. "${" .. k .. "_cpp_defines} " .. "${" .. k .. "_cpp_dirs} "
    end
    -- convert linker blocks
    for k, linker in pairs(state.module.cpp.linker) do
        local allFlags = {proxy.rootCompiler.extraflags, linker.flags} 
        state.module.build.variables[k .. "_link_flags"] = concat_arg(proxy.compilerConventions.extra_linkflags) .. table.concat(action.generate_flags(state, {}, allFlags, proxy.compilerConventions.flags.link, state.module.cpp.flags, handledFlags), " ")
        state.module.build.variables[k .. "_link_sys_libs"] = action.generate_list(linker.system_libs, proxy.compilerConventions.library, "")
        state.module.build.variables[k .. "_link_frameworks"] = action.generate_list(linker.frameworks, proxy.compilerConventions.framework, "")
        state.module.build.variables[k .. "_link_libs"] = action.generate_lib_list(state, proxy, k, linker)
        state.module.build.variables[k .. "_link_dirs"] = action.generate_list(action.generate_lib_dirs(state, proxy, linker.directories), proxy.compilerConventions.libdir, proxy.compilerConventions.libdirSuffix)
    end

    -- ** GENERATE OUTPUTS
    if state.module.build.outputs == nil then state.module.build.outputs = {} end
end

-- description: generate compiler build commands for builder (ninja)
function action.generate_build_compiler(state, key, compiler, proxy)
    if compiler.meta == nil then compiler.meta = {} end
    compiler.meta.outputs = {}

    local lc_compiler_state = {}
    lc_compiler_state.cppflags = "${" .. key .. "_z_cpp_flags}"
    lc_compiler_state.ccflags = "${" .. key .. "_z_cc_flags}"

    -- handle user specified build blob
    if compiler.build and type(compiler.build) == "table" then
        lc_compiler_state.mergeTable = utils.deep_copy(compiler.build)
    end

    -- handle preprocessed headers
    action.compiler_handle_pch(state, compiler, proxy, lc_compiler_state)

    -- object prefix
    local objprefix
    if compiler.prefix_obj == nil then
        local arch = action.get_arch(state)
        local profile = (settings.getSetting("build.env.profile") or ""):lower()
        objprefix = utils.os_pathsep .. profile .. "-" .. arch
    else
        objprefix = compiler.prefix_obj
    end

    -- handle normal sources        
    local moduleSep = "${module}" .. utils.os_pathsep
    local objSep = "obj" .. objprefix .. utils.os_pathsep .. "${mod_name}" .. utils.os_pathsep .. key .. utils.os_pathsep
    local objAbsSep = "obj" .. objprefix .. utils.os_pathsep .. "abs.${mod_name}" .. utils.os_pathsep .. key .. utils.os_pathsep
    for _, src in ipairs(compiler.sources or {}) do
        action.insert_source(state, compiler, proxy, moduleSep, src, lc_compiler_state.ccflags, lc_compiler_state.cppflags, objSep, lc_compiler_state.mergeTable)
    end
    for _, src in ipairs(compiler.sources_abs or {}) do
        action.insert_source(state, compiler, proxy, "", src, lc_compiler_state.ccflags, lc_compiler_state.cppflags, objAbsSep, lc_compiler_state.mergeTable)
    end
end

-- description: generate link commands for builder (ninja)
function action.generate_build_linker(state, k, linker, proxy)
    local lc_linker_inputs = {}
    local lc_linker_implicit_inputs = {"${tool_exec}"}
    local flattenedSources = utils.arr_flatten(linker.sources or {})
    for _, src in ipairs(flattenedSources) do
        local firstChar = src:sub(1,1)
        if firstChar == "@" then
            src = src:sub(2)
            if state.module.cpp.compiler[src] == nil then log_err(state.module.name, ": Failed to find linker source ", k, " from ", src) break end
            for _, ssrc in ipairs(state.module.cpp.compiler[src].meta.outputs) do
                table.insert(lc_linker_inputs, ssrc)
            end
        elseif firstChar == "^" then
            src = src:sub(2)
            table.insert(lc_linker_inputs, src)
        else
            table.insert(lc_linker_inputs, "${module}" .. utils.os_pathsep .. src)
        end
    end

    -- detect output filenames
    local lc_linkerOutputs = action.get_linker_outputs(state.module, linker, proxy.targetConventions, proxy.compilerConventions)
    if linker.meta == nil then linker.meta = {} end
    linker.meta.outputs = lc_linkerOutputs.outputs

    if state.cpp.generate_build then
        -- get build folder
        local buildFolder = scripts.get_build_dir()

        -- inject implicit dependencies
        table.insert(lc_linker_implicit_inputs, action.generate_libs_implicit(state, proxy, linker.libs))

        -- inject user specified inputs
        if linker.implicit_inputs then 
            utils.arr_append(lc_linker_implicit_inputs,  utils.arr_flatten(linker.implicit_inputs))
        end 

        -- flatten implicit dependencies
        lc_linker_implicit_inputs = utils.arr_flatten(lc_linker_implicit_inputs)

        -- order only inputs
        local lc_order_only_inputs = {}

        -- inject user specified inputs
        if linker.order_only_inputs then 
            utils.arr_append(lc_order_only_inputs, utils.arr_flatten(linker.order_only_inputs))
        end 

        local lc_defaultFlags = "${" .. k .. "_link_flags} " .. "${" .. k .. "_link_dirs} " .. "${" .. k .. "_link_libs} " .. "${" .. k .. "_link_sys_libs} " .. "${" .. k .. "_link_frameworks} " .. "${cpp_extra_ldflags} " 

        -- inject mac install flags
        lc_defaultFlags = lc_defaultFlags .. action.generate_dynamic_library_mac_install_name(state, proxy, linker, lc_linkerOutputs)

        -- add appropriate rule
        if linker.type == "application" then
            table.insert(state.module.build.outputs,
            {
                rule = "link",
                inputs = lc_linker_inputs,
                outputs = linker.meta.outputs,
                implicit_inputs = lc_linker_implicit_inputs,
                order_only_inputs = lc_order_only_inputs,
                variables = { flags = lc_defaultFlags }
            })
        elseif linker.type == "shared_library" then
            table.insert(state.module.build.outputs,
            {
                rule = "link",
                inputs = lc_linker_inputs,
                outputs = linker.meta.outputs,
                implicit_inputs = lc_linker_implicit_inputs,
                order_only_inputs = lc_order_only_inputs,
                implicit_outputs = lc_linkerOutputs.implicit,
                variables = { flags = proxy.compilerConventions.flag_sharedlib .. " " .. lc_defaultFlags  }
            })  
        elseif linker.type == "static_library" then
            table.insert(state.module.build.outputs,
            {
                rule = "lib",
                inputs = lc_linker_inputs,
                implicit_inputs = lc_linker_implicit_inputs,
                order_only_inputs = lc_order_only_inputs,
                outputs = linker.meta.outputs,
            })   
        else
            log_err("Cpp: " .. state.module.name .. ": Unknown linker type: " .. (linker.type or "") )
        end
    end
end

-- description: generate all rules and build commands for the current state for the builder (ninja)
function action.generate_build_rules(state, buildFolder)
    -- Main generator function..

    -- ** GENERATE CPP MODULE IF NOT EXISTS
    if state.module.cpp == nil then state.module.cpp = {} end
    if state.module.cpp.compiler == nil then state.module.cpp.compiler = {} end
    if state.module.cpp.linker == nil then state.module.cpp.linker = {} end
    if state.module.cpp.flags == nil then state.module.cpp.flags = {} end

    local lc_force_compiler = state.module.cpp.force_compiler
    local lc_rootCompiler = action.cpp_get_compiler_cached(state, lc_force_compiler)
    if lc_rootCompiler.cpp_compiler == nil then return end
    local lc_convention = lc_rootCompiler.cpp_compiler.compiler.convention

    local compilerConvention = action.v_conventions[lc_convention]
    if type(compilerConvention) == "function" then
        compilerConvention = compilerConvention(state)
    end

    local lc_proxy = {  rootCompiler=lc_rootCompiler, 
                        convention=lc_convention, 
                        compilerType=lc_rootCompiler.cpp_compiler.compiler.type, 
                        compilerConventions=compilerConvention, 
                        targetConventions=lc_rootCompiler.targetConventions,
                        scripts=lc_rootCompiler.lc_scripts,
                        buildFolder=buildFolder }


    -- resolve when rules
    lc_proxy.whenRules = action.generate_when_rules(state, lc_proxy.rootCompiler)
    for k, compiler in pairs(state.module.cpp.compiler) do action.resolve_when(state, compiler, lc_proxy.whenRules) end
    for k, linker in pairs(state.module.cpp.linker) do action.resolve_when(state, linker, lc_proxy.whenRules) end

    -- resolve imports
    action.handle_imports(state, lc_proxy)
 
    -- generate variables
    action.generate_build_variables(state, lc_proxy)

    -- call hook before generating final build commands (after handling when rules, imports and variables; i.e. pre-emit)
    if state.hooks ~= nil and state.hooks.cpp_preemit ~= nil then
        for k, hook in pairs(state.hooks.cpp_preemit) do
            hook(state)
        end
    end

    -- convert compiler blocks
    for k, compiler in pairs(state.module.cpp.compiler) do
        action.generate_build_compiler(state, k, compiler, lc_proxy)
    end

    -- convert linker blocks
    for k, linker in pairs(state.module.cpp.linker) do
       action.generate_build_linker(state, k, linker, lc_proxy)
    end

    -- Inject special rules
    if lc_proxy.compilerType == "clang" then action.inject_clang_specials(state) end
end

-- description: handle all import blocks (for linker and compiler blocks)
function action.handle_imports(state, lc_proxy)
    local  k, k2, k3, compiler, linker, path, module

    local lc_whenRules = lc_proxy.whenRules
    local lc_targetConventions = lc_proxy.targetConventions
    local lc_compilerConventions = lc_proxy.compilerConventions

    -- handle imports for compiler blocks
    for k, compiler in pairs(state.module.cpp.compiler) do
        if type(compiler.imports) == "table" then 
            local flattenedImports = utils.arr_flatten(compiler.imports)
            flattenedImports = action.expand_recursive_imports(state, flattenedImports, lc_whenRules, {})
            if log_level > 2 then
                log_dbg(state.module.name, "Handle compiler imports:", json.encode(utils.arr_flatten(compiler.imports)))
            end
            if compiler.meta == nil then compiler.meta = {} end
            compiler.meta.all_imports = flattenedImports -- write imports into compiler blob
            for k2, module in ipairs(flattenedImports) do
                action.compiler_handle_import(state, compiler, module, lc_whenRules )
            end
        end
    end
    -- handle imports for linker blocks
    for k, linker in pairs(state.module.cpp.linker) do
        local imports = {}
        local seqIndex = 1

        -- add explicit imports
        if type(linker.imports) == "table" then
            local flattenedImports = utils.arr_flatten(linker.imports)
            if log_level > 2 then
                log_dbg(state.module.name, "Handle linker imports:", json.encode(utils.arr_flatten(compiler.imports)))
            end
            for k2, module in ipairs(flattenedImports) do 
                if imports[module] == nil then
                    imports[module] = seqIndex 
                    seqIndex = seqIndex + 1 
                end
            end
        end

        -- add indirect imports from sources
        if type(linker.sources) == "table" then
            local flattenedSources = utils.arr_flatten(linker.sources)
            for k2,path in ipairs(flattenedSources) do
                if type(path) == "string" and path:sub(1,1) == "@" then
                    local compilerBlock = state.module.cpp.compiler[path:sub(2)]
                    if type(compilerBlock.imports) == "table" then
                        local flattenedImports = utils.arr_flatten(compilerBlock.imports)
                        for k3, module in ipairs(flattenedImports) do 
                            if imports[module] == nil then
                                imports[module] = seqIndex 
                                seqIndex = seqIndex + 1 
                            end
                        end
                    end
                end
            end
        end

        if linker.meta == nil then linker.meta = {} end

        -- reconstruct sequential imports
        local seqImports = {}
        for k2, k3 in pairs(imports) do seqImports[k3] = k2 end
        linker.meta.all_imports = seqImports

        -- handle recursion
        linker.meta.all_imports = action.expand_recursive_imports(state, linker.meta.all_imports, lc_whenRules, {})

        -- log recursive imports
        if log_level > 2 then
            log_dbg(state.module.name, "Imports after recursive expansion:",  json.encode(linker.meta.all_imports))
        end

        -- now handle imports
        for k2, module in ipairs(linker.meta.all_imports) do
            action.linker_handle_import(state, linker, module, lc_whenRules, lc_targetConventions, lc_compilerConventions)
        end
    end
end
function action.insert_source(state, compiler, proxy, srcPrefix, src, c_compiler_flags, cpp_compiler_flags, prefix, merge)
    -- expand tables
    if type(src) == "table" then
        for i,j in ipairs(src) do 
            action.insert_source(state, compiler, proxy, srcPrefix, j, c_compiler_flags, cpp_compiler_flags, prefix, merge)
        end
        return
    end

    local ext = utils.path_fileext(src)
    local isCFile = (ext == ".c" or ext == ".s" or ext == ".S" or ext == ".m")
    if ext == ".cpp" or ext == ".cxx" or ext == ".cc" or isCFile then
        local output = (prefix .. src:sub(1, src:len()-ext:len()) .. proxy.targetConventions.objectFileSuffix)
        table.insert(compiler.meta.outputs, output)

        if state.cpp.generate_build then
            local tbl = {
                inputs = { srcPrefix .. src },
                implicit_inputs = utils.arr_flatten({ "${tool_exec}", compiler.implicit_inputs }),
                order_only_inputs = compiler.order_only_inputs,
                outputs = { output },
                rule = utils.iif(isCFile, "cc", "cpp"),
                variables = { flags = utils.iif(isCFile, c_compiler_flags, cpp_compiler_flags) }
            }
            if merge then
                utils.table_merge(tbl, merge)
            end
            table.insert(state.module.build.outputs, tbl)
        end
    end
end

-- description: special case for clang.
function action.inject_clang_specials(state)
    -- CLANG: add diagnostic path switch
    if state.cpp.generate_build then
        for k, compiler in pairs(state.module.cpp.compiler) do 
            state.module.build.variables[k .. "_cxx_flags"] = "-fdiagnostics-absolute-paths " .. state.module.build.variables[k .. "_cxx_flags"]
        end
    end
end

-- description: handle linker imports
function action.linker_handle_import(state, linker, impModuleName, lc_whenRules, lc_targetConventions, lc_compilerConventions)
    local importModule = state.ro_modules[impModuleName]
    local imported = action.get_imported_block(state, impModuleName, lc_whenRules)

    if type(imported.linker) == "table" then
        -- handle directories
        if type(imported.linker.directories) == "table" then
            if linker.directories == nil then linker.directories = {} end
            local directories = utils.arr_flatten(imported.linker.directories)
            for i,j in ipairs(directories) do
                if j:sub(1,1) ~= "^" then
                    table.insert(linker.directories, utils.path_merge(importModule.path, j))
                else
                    table.insert(linker.directories, j)
                end
            end
        end

        -- handle sources
        if type(imported.linker.sources) == "table" and linker.type ~= "static_library" then
            if linker.sources == nil then linker.sources = {} end
            local sources = utils.arr_flatten(imported.linker.sources)
            for i,j in ipairs(sources) do
                table.insert(linker.sources, "^" .. utils.path_merge(importModule.path, j))
            end
        end

        -- handle system_libs
        if type(imported.linker.system_libs) == "table" and linker.type ~= "static_library" then
            if linker.system_libs == nil then linker.system_libs = {} end
            local libs = utils.arr_flatten(imported.linker.system_libs)
            for i,j in ipairs(libs) do table.insert(linker.system_libs, j) end
        end

        -- handle frameworks
        if type(imported.linker.frameworks) == "table" and linker.type ~= "static_library" then
            if linker.frameworks == nil then linker.frameworks = {} end
            local frameworks = utils.arr_flatten(imported.linker.frameworks)
            for i,j in ipairs(frameworks) do table.insert(linker.frameworks, j) end
        end

        -- handle libs
        if type(imported.linker.libs) == "table" and linker.type ~= "static_library" then
            if linker.libs == nil then linker.libs = {} end
            local libs = utils.arr_flatten(imported.linker.libs)
            for i,j in ipairs(libs) do 
                if j:sub(1,1) ~= "@" then
                    table.insert(linker.libs, j)
                else
                    local subname = j:sub(2)
                    if importModule.cpp.linker[subname] == nil then 
                        log_err(state.module.name, ": Could not resolve imported lib ", j, " - Link not found.") 
                    else
                        local o=action.get_linker_outputs(importModule, importModule.cpp.linker[subname], lc_targetConventions, lc_compilerConventions )
                        table.insert(linker.libs, "!" .. o.libname) -- add resolved library (add ! so that it can be interpreted as an implicit input for build ordering)
                    end 
                end
            end
        end
    end
end

-- descritpion: given a block (with .when table) and associated when rules, update block and remove when block
function action.resolve_when(state, block, whenRules)
    if block.when then
        if state.module.cpp.meta == nil then state.module.cpp.meta = {} end
        if not state.module.cpp.meta.__applywhen then
            state.module.cpp.meta.__applywhen = true
            if log_level > 2 then
                log_dbg("Cpp: " .. state.module.name .. ": Applying when rules: " .. json.encode(whenRules))
            end
        end

        -- find when rules
        local apply = {}
        for whenRule, whenApply in pairs(block.when) do
            if whenRules[whenRule] ~= nil then 
                table.insert(apply, whenApply)
            end
        end
        block.when = nil

        -- apply them
        if #apply > 0 then
            for i,j in ipairs(apply) do
                utils.table_merge(block, j )
            end
        end
    end
end

-- description: convert paths from relative to absolute paths.
--   allows for ^ prefix exception that will keep original path intact.
function action.relativize_paths_to(rootDir, relativeTo, paths)
    if paths == nil then paths = {} end
    local ret = {}
    for i,path in ipairs(paths) do
        if path:sub(1,1) == "^" then
            table.insert(ret, path:sub(2))
        else
            table.insert(ret,  utils.path_rel(relativeTo, utils.path_merge(rootDir, path)) )
        end
    end
    return ret
end

g_action = action
    -- C++ configurations
    dofile(utils.path_merge(utils.get_script_folder(), "cpp_conventions.lua"))
    dofile(utils.path_merge(utils.get_script_folder(), "cpp_targets.lua"))

    -- Libraries
    dofile(utils.path_merge(utils.get_script_folder(), "cpp_detect_compiler.lua"))
g_action = nil

return action