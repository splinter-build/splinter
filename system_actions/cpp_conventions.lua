local action = g_action

-- TODO: Implement rpath_link for linux to make resolving sub-libraries automatic.

-- *** C++ Compiler conventions ***
-- Helpers:
-- description: Auto detect architecture
function gcc_clang_arch_autodetect(state)
    local arch = action.get_arch(state)
    if arch == "unknown" then return "" end
    if arch == "x86" then return "-m32" end
    if arch == "x64" then return "-m64" end
    if arch == "arm64" then return "" end
    return "-m" .. action.get_arch(state)
end

-- Conventions:
action.v_conventions = {
    gcc = {
        cc = {
            description = "CC ${out}",
            command = "${tool_cc} -MMD -MF ${out}.d ${flags} ${cpp_extra_cppflags} -c ${in} -o ${out}",
            depfile = "${out}.d",
            deps = "gcc"
        },
        cpp = {
            description = "CPP ${out}",
            command = "${tool_cpp} -MMD -MF ${out}.d ${flags} ${cpp_extra_cppflags} -c ${in} -o ${out}",
            depfile = "${out}.d",
            deps = "gcc"
        },
        link = {
            description = "LINK ${out}",
            command = "${tool_link} @${out}.rsp ${flags} -o ${out}",
            rspfile = "${out}.rsp",
            rspfile_content = "${in_newline}"
        },
        lib = {
            description = "LIB ${out}",
            command = "${tool_lib} rcsT ${out} ${in}",
        },
        define = "-D",
        defineSuffix = "",
        libraryPrefix = "-Wl,--start-group ",
        librarySuffix = " -Wl,--end-group",
        framework = "-Wl,-framework,",
        library = "-l",
        libraryExplicit = "-l:",
        libdir = "-L",
        libdirSuffix = "",
        includedir = "-I",
        includedirSuffix = "",
        flag_sharedlib = "-shared",
        dll_createsLib = false,
        extra_linkflags = "-Wl,-rpath-link,${bin_folder}", -- inject extra rpath-link so that ld can find the binaries
        flags = {
            cxx = {
                ArchSSE             = "-msse",
                ArchSSE2            = "-msse2",
                ArchSSE3            = "-msse3",
                ArchSSE4            = "-msse4",
                ArchSSE4_1          = "-msse4.1",
                ArchSSE4_2          = "-msse4.2",
                ArchAVX             = "-mavx",
                ArchAVX2            = "-mavx2",
                ArchAVX512          = "-mavx512",
                NoWarnings          = "-w",
                PedanticWarnings    = "-Wall -Wextra -pedantic",
                ExtraWarnings       = "-Wall -Wextra",
                FatalWarnings       = "-Werror",
                FloatFast           = "-ffast-math",
                FloatStrict         = "-ffloat-store",
                NoFramePointer      = "-fomit-frame-pointer",
                OptimizeFull        = "-O3",
                OptimizeSize        = "-Os",
                OptimizeSpeed       = "-O2",
                Symbols             = "-g",
                NoExceptions        = "-fno-exceptions",
                NoRTTI              = "-fno-rtti",
                NoInline            = "-fno-inline-functions",
                UnsignedChar        = "-funsigned-char",
                Arch32              = "-m32",
                Arch64              = "-m64",
                ArchARM             = "-marm",
                PIC                 = "-fPIC",
                Arch                = gcc_clang_arch_autodetect,
            },
            cpp = {
                Cpp11               = "-std=c++11",
                Cpp14               = "-std=c++14",
                Cpp17               = "-std=c++17",
                CppLatest           = "-std=c++2a",
            },
            link = {
                RpathOrigin = "-Wl,-rpath,\\$$ORIGIN",
                Arch32 = "-m32",
                Arch64 = "-m64",
                ArchARM = "-marm",
                Arch = gcc_clang_arch_autodetect,
            }
        }
    },
    msvc = {
        cc = {
            description = "CC ${out}",
            command = "${tool_cc} /nologo /FC /showIncludes ${flags} ${cpp_extra_cppflags} /Fo\"${out}\" /c ${in}",
            deps = "msvc"
        },
        cpp = {
            description = "CPP ${out}",
            command = "${tool_cpp} /nologo /FC /showIncludes ${flags} ${cpp_extra_cppflags} /Fo\"${out}\" /c ${in}",
            deps = "msvc"
        },
        link = {
            description = "LINK ${out}",
            command = "${tool_link} /nologo /OUT:${out} ${flags} @${out}.rsp",
            rspfile = "${out}.rsp",
            rspfile_content = "${in_newline}",
            restat = "1"
        },
        lib = {
            description = "LIB ${out}",
            command = "${tool_lib} /nologo /OUT:\"${out}\" @${out}.rsp",
            rspfile = "${out}.rsp",
            rspfile_content = "${in_newline}",
            restat = "1"
        },
        define = "/D\"",
        defineSuffix = "\"",
        framework = "",
        library = "",
        libraryExplicit = "",
        libdir = "/LIBPATH:\"",
        libdirSuffix = "\"",
        includedir = "/I\"",
        includedirSuffix = "\"",
        flag_sharedlib = "/DLL",
        dll_createsLib = true,
        flags = {
            cxx = {
                Default             = "",
                Deterministic       = "/D__DATE__= /D__TIME__= /D__TIMESTAMP__= /Brepro",
                ForceCPP            = "/TP",
                ExtraWarnings       = "/W4",
                PedanticWarnings    = "/W4",
                NoWarnings          = "/w",
                NoNativeWChar       = "/Zc:wchar_t-",
                FloatFast           = "/fp:fast",
                FloatStrict         = "/fp:strict",
                FloatPrecise        = "/fp:precise",
                UnsignedChar        = "/J",
                RuntimeStatic       = "/MT",
                RuntimeDynamic      = "/MD",
                RuntimeStaticDebug  = "/MTd",
                RuntimeDynamicDebug = "/MDd",
                SymbolsPdb          = "/FS /Zi /Fdobj\\${mod_name}.pdb",
                Symbols             = "/Z7",
                OptimizeFull        = "/Ox",
                OptimizeSize        = "/Os",
                OptimizeSpeed       = "/O2",
                OptimizeOff         = "/Od",               
                BigObject           = "/bigobj",
                ArchSSE             = "/arch:SSE",
                ArchSSE2            = "/arch:SSE2",
                ArchAVX             = "/arch:AVX",
                ArchAVX2            = "/arch:AVX2",
                ArchAVX512          = "/arch:AVX512",
                Exceptions          = "/EHsc",
                ExceptionsSEH       = "/EHa",
                FramePointer        = "/Oy",
                NoFramePointer      = "/Oy-",
                FatalWarnings       = "/WX",
                NoFatalWarnings     = "/WX-",
                Intrinsics          = "/Oi",
                NoIntrinsics        = "/Oi-",
                Inlining            = "/Ob2",
                NoInline            = "/Ob0",
                RTTI                = "/GR",
                NoRTTI              = "/GR-",
                BufferSecurityCheck = "/GS",
                NoBufferSecurityCheck = "/GS-",
                RuntimeErrorChecking = "/RTC1",  
                Asan                = "/fsanitize=address",
            },
            cpp = {
                Cpp11               = "/std:c++14", -- std:c++11 not supported, just go with c++14 instead. (src: https://devblogs.microsoft.com/cppblog/standards-version-switches-in-the-compiler/)
                Cpp14               = "/std:c++14", 
                Cpp17               = "/std:c++17", 
                CppLatest           = "/std:c++latest",
            },
            link = {
                Deterministic       = "/Brepro",
                Symbols             = "/DEBUG:FULL",
                OptimizeFull        = "/OPT:REF /OPT:ICF /OPT:LBR",
                OptimizeSize        = "/OPT:REF /OPT:ICF /OPT:LBR",
                OptimizeSpeed       = "/OPT:REF /OPT:ICF /OPT:LBR",
                OptimizeOff         = "/OPT:NOREF /OPT:NOICF /OPT:NOLBR",
                AppConsole          = "/SUBSYSTEM:CONSOLE",
                AppWindows          = "/SUBSYSTEM:WINDOWS",
                IgnorePdbWarning    = "/IGNORE:4099",
                NoIncremental       = "/INCREMENTAL:NO",
                NoASLR              = "/DYNAMICBASE:NO",
                Arch32              = "/MACHINE:X86",
                Arch64              = "/MACHINE:X64",
                ArchARM             = "/MACHINE:ARM",
                Arch                = function(state) 
                    local arch = action.get_arch(state)
                    if arch == "x86" then return "/MACHINE:X86" end
                    if arch == "x64" then return "/MACHINE:X64" end
                    return "/MACHINE:" .. arch
                end,
            }
        }
    }
}

-- Convention: Clang-CL (MSVC with extras)
action.v_conventions.clangcl = function(state)
    local cv = utils.deep_copy(action.v_conventions.msvc)
    utils.table_merge(cv, {
        flags = {
            cxx = {
                Asan        = "", -- not supported currently
                Arch32      = "-m32",
                Arch64      = "-m64",
                ArchARM     = "-marm",
                ArchSSE3            = "-msse3",
                ArchSSE4            = "-msse4",
                ArchSSE4_1          = "-msse4.1",
                ArchSSE4_2          = "-msse4.2",
                Arch        = gcc_clang_arch_autodetect
            },
            cpp = {

            }
        }
    })
    return cv
end

-- Convention: GCC MacOSX style
action.v_conventions.gcc_mac = function(state)
    local cv = utils.deep_copy(action.v_conventions.gcc)
    cv.libraryPrefix = " "
    cv.librarySuffix = " "
    cv.libraryForceNamePrefix = "lib"
    cv.libraryRemoveExtension = true
    cv.libraryRemovePrefix = "lib"
    cv.libraryDynamicSetInstallName = "-install_name @rpath/"
    cv.libraryExplicit = cv.library
    cv.extra_linkflags = ""
    cv.lib.command = "${tool_lib} rcsTL ${out} ${in}" -- support for long filenames
    cv.flags.link.RpathOrigin = "-rpath @executable_path" -- thanks to: https://stackoverflow.com/questions/27506450/clang-change-dependent-shared-library-install-name-at-link-time
    return cv
end

-- Convention: GCC Emscripten Style
action.v_conventions.gcc_emscripten = function(state)
    local cv = utils.deep_copy(action.v_conventions.gcc)
    cv.link = {
        description = "LINK ${out}",
        command = "${tool_link} ${in} ${flags} -o ${out}",
    }
    cv.libraryPrefix = " "
    cv.librarySuffix = " "
    cv.libraryForceNamePrefix = "lib"
    cv.libraryRemoveExtension = true
    cv.libraryRemovePrefix = "lib"
    cv.libraryExplicit = cv.library
    cv.extra_linkflags = ""
    cv.flags.link.RpathOrigin = "" -- RpathOrigin not supported on emscripten
    cv.flags.cxx.Symbols = "-g3"
    cv.flags.link.Symbols = "-g3"
    cv.flags.link.SourceMap = "-gsource-map"
    return cv
end