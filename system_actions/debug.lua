local action = {}

function action.init(state)
    log_dbg("Debug: Initializing..")
end
function action.unload(state)
    log_dbg("Debug: Unloading..")

end
function action.execute(state)
    log_dbg("Debug: Printing module state", state.module.name, "from", state.module_action.name)
    log_dbg(json.encode(state.module))   
end
return action