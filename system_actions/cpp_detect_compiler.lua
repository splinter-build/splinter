
local action = g_action
-- description: find configured compiler based on user settings, and cache the results
action.cpp_get_compiler_cached = function(state, use_compiler)
    if use_compiler == "" or use_compiler == nil then 
        use_compiler = settings.getSetting("build.cpp.compiler", "any") 
    end

    -- create cache key
    state.module.cpp = state.module.cpp or {}
    local force_target = state.module.cpp.force_target or "def"
    local force_arch = state.module.cpp.force_arch or "def"
    local cache_suffix = "_" .. force_target:lower() .. "_" .. force_arch:lower()
    local cache_key = use_compiler .. cache_suffix

    -- return cached compiler
    if state.cpp.compilers[cache_key] then return state.cpp.compilers[cache_key] end

    -- build new cached entry
    state.cpp.compilers[cache_key] = {}
    local compiler = action.cpp_get_compiler(state, use_compiler)
    if compiler == false then return {} end
    state.cpp.compilers[cache_key] = compiler
    state.cpp.compilers[cache_key].lc_scripts = action.cpp_generate_scripts(state, compiler, compiler.host_os, cache_suffix)
    return state.cpp.compilers[cache_key]
end

-- description: find configured compiler based on user settings
action.cpp_get_compiler = function(state, use_compiler)
    local target =       settings.getSetting("build.env.target", utils.os_name):lower()
    local host_os =      settings.getSetting("build.env.host", utils.os_name):lower()

    -- force overwrites
    if state.module.cpp.force_target ~= nil then
        if state.module.cpp.force_target:lower() == "host" then
            target = host_os
        else
            target = state.module.cpp.force_target:lower()
        end
    end
    -- detect compiler
    local compilers =    compilers.detect_filtered_pkgs(target, host_os)
    
    -- retrieve c++ sdk and compiler
    local cpp_use_compiler = use_compiler
    local cpp_compilers= utils.arr_which(compilers, function(x) return x.type == "cpp_compiler" and (cpp_use_compiler == "any" or cpp_use_compiler == x.name) end)

    if #cpp_compilers == 0 then
        log_err_notrace("Cpp: No compiler found: " .. cpp_use_compiler)
        return false
    end

    local compiler_name = cpp_compilers[1].name  
    local cpp_extra_flags = utils.str_split(settings.getSetting("build.cpp.flags.extra", "") .. " " .. settings.getSetting("build.cpp.flags.extra." .. compiler_name, ""), " ")
    local cpp_extra_cppflags = settings.getSetting("build.cpp.flags.compiler", "") .. " " .. settings.getSetting("build.cpp.flags.compiler." .. compiler_name, "")
    local cpp_extra_ldflags = settings.getSetting("build.cpp.flags.linker", "") .. " " .. settings.getSetting("build.cpp.flags.linker." .. compiler_name, "")
    
    local cpp_sdktypes  = {}
    local cpp_sdks = utils.arr_which(compilers, function(x) 
        if x.type == "cpp_sdk" then
            -- only allow a single sdk type
            if cpp_sdktypes[x.sdk.type] == nil then
                cpp_sdktypes[x.sdk.type] = 1
                return true
            end
        end
        return false
    end)


    local targetConventions = nil
    if action.v_targets ~= nil then
        if action.v_targets[target] == nil then
            log_err_notrace("Cpp: No compiler target found: " .. target)
            return false
        end
        targetConventions = action.v_targets[target]
    end

    --
    -- build final object
    return {
        target=target, 
        host_os=host_os, 
        cpp_compiler=cpp_compilers[1],
        cpp_sdks=cpp_sdks, 
        extraflags=cpp_extra_flags or {},
        cppflags=cpp_extra_cppflags,
        ldflags=cpp_extra_ldflags,
        targetConventions =targetConventions,       
    }
end

-- description: get current architecture target
action.get_arch = function(state)
    local arch = tostring(settings.getSetting("build.env.arch", tostring(utils.os_arch))):lower()
    if state ~= nil and state.module.cpp.force_arch ~= nil then arch = state.module.cpp.force_arch:lower() end
    if state ~= nil and state.module.cpp.force_arch == "host" then arch = tostring(utils.os_arch):lower() end
    if arch == "32" or arch == "x32" or arch == "x86" then return "x86" end
    if arch == "64" or arch == "x64" then return "x64" end
    return arch
end

-- description: based on the compiler, determine environment variables like CC/CXX/LD/AR
action.cpp_get_env_vars = function(state, tools)
    local arch = action.get_arch(state)
    local env = {}
    local paths = {}
    local pathSep = ":"
    if(utils.os_name == "Windows") then
        pathSep = ";"
    end

    -- * insert path of splinter execution (luajit)
    table.insert(paths, utils.path_folder(arg[-1]))

    -- * set c++ compiler
    local cpp_compiler = tools.cpp_compiler
    
    if cpp_compiler ~= nil then
        -- update path array
        paths = utils.arr_merge(paths,  utils.arr_select(cpp_compiler.path or {}, function(x) 
            return utils.path_merge(cpp_compiler.folder, x) 
        end) )

        -- update path array with arch
        if cpp_compiler["path_" .. arch] ~= nil then
            paths = utils.arr_merge(paths,  utils.arr_select(cpp_compiler["path_" .. arch]  or {}, function(x) 
                return utils.path_merge(cpp_compiler.folder, x) 
            end) )
        end

        -- set compiler
        env.CC = cpp_compiler.compiler.cc
        env.CPP = cpp_compiler.compiler.cxx or cpp_compiler.compiler.cc
        env.LD = cpp_compiler.compiler.link
        env.LD_LIB = cpp_compiler.compiler.lib

        -- * merge custom environment
        if cpp_compiler.env ~= nil then 
            utils.table_merge(env, cpp_compiler.env )
        end
    end
    
    -- * set c++ sdk
    local cpp_sdk = nil
    local all_includes = ""
    local all_libs = ""
    for _, cpp_sdk in ipairs(tools.cpp_sdks or {}) do 
        local fBuild = function(cpp_sdk, src)
            local includes = ""
            utils.arr_select(src, function(x)
                includes = includes .. utils.path_abs(utils.path_merge(cpp_sdk.folder,x)) .. ";"
            end)
            includes = utils.str_template_replace(includes, cpp_sdk.variables)
            return includes
        end
        -- build includes
        all_includes = all_includes .. fBuild(cpp_sdk, cpp_sdk.sdk.includes)
        if cpp_sdk.sdk["libs_" .. arch] then
            all_libs = all_libs .. fBuild(cpp_sdk, cpp_sdk.sdk["libs_" .. arch])
        end
    end

    -- set envs
    if all_includes ~= "" then  env.INCLUDE = all_includes end
    if all_libs ~= "" then      env.LIB = all_libs end
    
    -- * set path
    -- build/set path env variable paths
    if #paths > 0 then 
        local path = ""

        -- [crinkler support]
        if(utils.os_name == "Windows") then
            path = "C:\\WINDOWS\\SYSTEM32" .. pathSep -- needed for crinkler to detect dlls
        end

        utils.arr_select(paths, function(x)  path = path .. utils.path_abs(x) .. pathSep end)

        env.PATH = path
    end
    log_dbg("Cpp: Env vars: ")
    for i, j in pairs(env) do 
        log_dbg(" ", i, "=", j)
    end
    return env
end


-- description: generate .bat and .sh files for invoking the selected compilers
action.cpp_generate_scripts = function(state, compiler, host_os, cache_suffix)
    local ret = {}
    local env = action.cpp_get_env_vars(state, compiler)
    local sorted_env = utils.table_toarr_kv_sorted(env)
    local prefix = "ninja"
    local builddir = scripts.get_build_dir()
    local scriptName = (compiler.cpp_compiler.name or "unknown_compiler") .. cache_suffix
    local script = ""
    if state.cpp.generate_build == true then
        utils.path_mkdir_recur(utils.path_merge(builddir, prefix))
    end
    if(host_os == "windows") then
        if state.cpp.generate_build == true then
            local newline = "\r\n"
            for i,j in ipairs(sorted_env) do
                script = script .. "@set " .. j[1] .. "=" .. j[2] .. newline
            end
            local data = script .. "@%*"
            utils.file_write_if_unequal(utils.path_merge(builddir, prefix, scriptName .. ".bat"), data)
        end
        ret.exec = "ninja\\" .. scriptName .. ".bat"
    else
        if state.cpp.generate_build == true then
            local newline = "\n"
            script = "#!/bin/sh" .. newline
            for i,j in ipairs(sorted_env) do
                script = script .. "export " .. j[1] .. "=" .. utils.shell_escape(j[2]) .. newline
            end
            local data = script .. "\"${@}\""
            utils.file_write_if_unequal(utils.path_merge(builddir, prefix, scriptName .. ".sh"), data)
        end
        ret.exec = "ninja/" .. scriptName .. ".sh"

        if state.cpp.generate_build == true then
            for i,j in pairs(ret) do os.execute ("cd " .. utils.shell_escape(builddir) .. "; chmod a+x " .. utils.shell_escape(j)) end
        end
    end
    ret.cc = ret.exec .. " " .. env.CC
    ret.cpp = ret.exec .. " " .. env.CPP
    ret.link = ret.exec .. " " .. env.LD
    ret.lib = ret.exec .. " " .. env.LD_LIB
    return ret
end