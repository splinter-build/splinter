local action = {}


function action.init(state)
    log_info("IDEGen-Vstudio: Initializing..")
    state.idegen_vstudio = {}
    state.idegen_vstudio.profile = false
    state.idegen_vstudio.platforms = {x64="x64", x86="Win32"}
    state.idegen_vstudio.profiles = {"FullDebug", "Debug", "Release", "Final"}
    state.idegen_vstudio.projects = {}
    state.idegen_vstudio.buildpath = scripts.get_build_dir()
    state.idegen_vstudio.path = utils.path_merge(state.idegen_vstudio.buildpath, "vstudio")
    state.idegen_vstudio.relpath_buildsystem = utils.path_rel(state.idegen_vstudio.path, gDirs.base_buildsystem)
    state.idegen_vstudio.relpath_user = utils.path_rel(state.idegen_vstudio.path, gDirs.user)
    utils.path_mkdir_recur(state.idegen_vstudio.path)
end
function action.unload(state)
    log_info("IDEGen-Vstudio: Building visual studio projects..")

    -- add allbuild project
    action.add_allbuild(state)

    -- create folders
    local path = state.idegen_vstudio.path

    -- generate pairs
    local vpairs = action.generate_pairs(state)
    local filesWritten = 0

    -- write solution
    if utils.file_write_if_unequal(utils.path_merge(path, "solution.sln"), action.generate_solution(state)) then filesWritten = filesWritten + 1 end

    -- write projects
    for i=1,#state.idegen_vstudio.projects do
        local prj = state.idegen_vstudio.projects[i]
        local filename = (prj.filename or prj.name)
        log_dbg("IDEGen-Vstudio: Generating project", filename)
        if utils.file_write_if_unequal(utils.path_merge(path, filename .. ".vcxproj"), action.generate_project(state, prj, vpairs))                     then filesWritten = filesWritten + 1 end
        if utils.file_write_if_unequal(utils.path_merge(path, filename .. ".vcxproj.filters"), action.generate_project_filters(state, prj, vpairs))     then filesWritten = filesWritten + 1 end
        if utils.file_write_if_unequal(utils.path_merge(path, filename .. ".vcxproj.user"), action.generate_project_user(state, prj, vpairs))           then filesWritten = filesWritten + 1 end
    end

    -- clean data
    state.idegen_vstudio = nil
    log_info("IDEGen-Vstudio: Done.. (files updated: " .. filesWritten .. ")")
end

function action.generate_project_configuration(state, project, config, platform)
    return utils.xml("ProjectConfiguration", {"Include", config .. "|" .. platform}, {
        utils.xml("Configuration", {}, config),
        utils.xml("Platform", {}, platform)
    })
end
function action.generate_project_configuration_propgroup(state, project, config, platform)
    return utils.xml("PropertyGroup", {"Condition", "'$(Configuration)|$(Platform)'=='" .. config .. "|" .. platform .. "'", "Label", "Configuration"}, {
        utils.xml("ConfigurationType", {}, "Makefile"),
        utils.xml("UseDebugLibraries", {}, "false"),
        utils.xml("PlatformToolset", {"Condition", "'$(VisualStudioYear)' == '2012'"}, "v110_xp"),
        utils.xml("PlatformToolset", {"Condition", "'$(VisualStudioYear)' == '2013'"}, "v120_xp"),
        utils.xml("PlatformToolset", {"Condition", "'$(VisualStudioYear)' == '2015'"}, "v140"),
        utils.xml("PlatformToolset", {"Condition", "'$(VisualStudioYear)' == '2017'"}, "v141"),
        utils.xml("PlatformToolset", {"Condition", "'$(VisualStudioYear)' == '2019'"}, "v142"),
        utils.xml("PlatformToolset", {"Condition", "'$(VisualStudioYear)' == '20XX'"}, "$(DefaultPlatformToolset)"),
    })
end

function action.generate_build_folder(state, config, platform)
    local eplatform = platform
    if platform == "Win32" then eplatform = "x86" end
    return eplatform .. "-" .. config, eplatform
end

function action.get_build_path(state, config, platform)
    --local folder= action.generate_build_folder(state, config, platform)
    --return utils.path_merge(gDirs.local_build, folder)
    return gDirs.local_build
end

function action.generate_project_nmake(state, project, config, platform)
    local folder, eplatform = action.generate_build_folder(state, config, platform)
    --local setenv = "cd \"$(SolutionDir)" .. state.idegen_vstudio.relpath_user .. "\" &amp;&amp; echo sett build.env.prefix " .. folder .. "; sett build.env.arch " .. eplatform .. "; sett build.env.profile " .. config .. "; "
    local setenv = "cd \"$(SolutionDir)" .. state.idegen_vstudio.relpath_user .. "\" &amp;&amp; echo sett build.env.arch " .. eplatform .. "; sett build.env.profile " .. config .. "; "
    local suffix = " | \"$(SolutionDir)" .. state.idegen_vstudio.relpath_buildsystem .. "\\splinter.bat\""
    local buildPath = action.get_build_path(state, config, platform)
    local outputRelative = utils.path_rel(state.idegen_vstudio.path, utils.path_merge(buildPath, project.output or "unknown.dll"))

    return utils.xml("PropertyGroup", {"Condition", "'$(Configuration)|$(Platform)'=='" .. config .. "|" .. platform .. "'"}, {   
        utils.xml("NMakeBuildCommandLine", {}, setenv ..  "build " .. project.name .. suffix),
        utils.xml("NMakeOutput", {}, outputRelative),
        utils.xml("NMakeCleanCommandLine", {}, setenv ..  "sett build.ninja.args -t clean; build " .. project.name .. suffix),
        utils.xml("NMakePreprocessorDefinitions", {}, table.concat(project.defines or {}, ";") .. ";$(NMakePreprocessorDefinitions)"),
        utils.xml("NMakeIncludeSearchPath", {}, table.concat(project.include_directories or {}, ";") .. ";$(NMakeIncludeSearchPath)"),
    })
end
function action.generate_project_propsheet(state, project, config, platform)
    return utils.xml("ImportGroup", {"Label", "PropertySheets", "Condition", "'$(Configuration)|$(Platform)'=='" .. config .. "|" .. platform .. "'"}, {
        utils.xml("Import", {"Project","$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props","Condition", "exists('$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props')", "Label", "LocalAppDataPlatform" })
    })
end

function action.generate_project(state, project, vpairs)
    -- generate data based on config/platform
    local d1 = {}
    local d2 = {}
    local d3 = {}
    local d4 = {}
    for i, j in pairs(vpairs) do
        local arr = utils.str_split(j, "|")
        local config = arr[1]
        local platform = arr[2]
        table.insert(d1, action.generate_project_configuration(state, project, config, platform) )
        table.insert(d2, action.generate_project_configuration_propgroup(state, project, config, platform) )
        table.insert(d3, action.generate_project_propsheet(state, project, config, platform) )
        table.insert(d4, action.generate_project_nmake(state, project, config, platform) )
    end

    -- generate itemgroups
    local nonegroup = utils.xml("ItemGroup", {}, {})
    local cppgroup = utils.xml("ItemGroup", {}, {})
    local hdrgroup = utils.xml("ItemGroup", {}, {})
    local itemgroups = {nonegroup, cppgroup, hdrgroup}
    
    if state.idegen_vstudio.profile then log_dbg("IDEGen-Vstudio: -- Generating files section..") end
    for _, filePair in ipairs(project.files or {}) do
        local file = filePair[2]
        if(utils.str_ends_with(file, ".hxx") or utils.str_ends_with(file, ".h") or utils.str_ends_with(file, ".hpp")) then
            table.insert(hdrgroup.children, utils.xml("ClInclude", {"Include", file}))
        elseif(utils.str_ends_with(file, ".cpp") or utils.str_ends_with(file, ".c") or utils.str_ends_with(file, ".cxx")) then
            table.insert(cppgroup.children, utils.xml("ClCompile", {"Include", file}))
        else
            table.insert(nonegroup.children, utils.xml("None", {"Include", file}))
        end
    end

    -- put it in the template
    local data = 
        utils.xml("Project", {"DefaultTargets", "Build", "xmlns", "http://schemas.microsoft.com/developer/msbuild/2003"}, {
            utils.xml("PropertyGroup", {"Label", "UserMacros"}, {
                utils.xml("VisualStudioYear", {"Condition", "'$(VisualStudioVersion)' == '11.0' Or '$(PlatformToolsetVersion)' == '110' Or '$(MSBuildToolsVersion)' ==  '4.0'"}, "2012"),
                utils.xml("VisualStudioYear", {"Condition", "'$(VisualStudioVersion)' == '12.0' Or '$(PlatformToolsetVersion)' == '120' Or '$(MSBuildToolsVersion)' ==  '12.0'"}, "2013"),
                utils.xml("VisualStudioYear", {"Condition", "'$(VisualStudioVersion)' == '14.0' Or '$(PlatformToolsetVersion)' == '140' Or '$(MSBuildToolsVersion)' ==  '14.0'"}, "2015"),
                utils.xml("VisualStudioYear", {"Condition", "'$(VisualStudioVersion)' == '15.0' Or '$(PlatformToolsetVersion)' == '141' Or '$(MSBuildToolsVersion)' ==  '15.0'"}, "2017"),
                utils.xml("VisualStudioYear", {"Condition", "'$(VisualStudioVersion)' == '16.0' Or '$(PlatformToolsetVersion)' == '142' Or '$(MSBuildToolsVersion)' ==  '16.0'"}, "2019"),
                utils.xml("VisualStudioYear", {"Condition", "'$(VisualStudioYear)' == ''"}, "20XX"),
            }),
            utils.xml("ItemGroup", {"Label", "ProjectConfigurations"}, {
                d1
            }),
            utils.xml("PropertyGroup", {"Label", "Globals"}, {
                utils.xml("VCProjectVersion", {}, "16.0"),
                utils.xml("ProjectGuid", {}, project.guid),
                utils.xml("Keyword", {}, "Win32Proj")
            }),
            utils.xml("Import", {"Project", "$(VCTargetsPath)\\Microsoft.Cpp.Default.props"}, ""),
            d2,
            utils.xml("Import", {"Project", "$(VCTargetsPath)\\Microsoft.Cpp.props"}),
            utils.xml("ImportGroup", {"Label", "ExtensionSettings"}, {}),
            utils.xml("ImportGroup", {"Label", "Shared"}, {}),
            d3,
            utils.xml("PropertyGroup", {}, {
                utils.xml("DisableFastUpToDateCheck", {}, "true")
            }),
            d4,
            utils.xml("ItemDefinitionGroup", {}, {}),
            itemgroups,
            utils.xml("Import", {"Project", "$(VCTargetsPath)\\Microsoft.Cpp.targets"}),
            utils.xml("ItemGroup", {"Label", "ExtensionTargets"}, {}),
        })

    if state.idegen_vstudio.profile then log_dbg("IDEGen-Vstudio: -- Generating XML..") end

    -- generate final xml
    local xmltbl = {}
    utils.xmlgen(data, xmltbl)
    return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" .. table.concat(xmltbl)
end

function action.generate_project_filters(state, project, vpairs)
    -- generate itemgroups
    local nonegroup = utils.xml("ItemGroup", {}, {})
    local cppgroup = utils.xml("ItemGroup", {}, {})
    local hdrgroup = utils.xml("ItemGroup", {}, {})
    local itemgroups = {nonegroup, cppgroup, hdrgroup}  

    local filters = {}

    if state.idegen_vstudio.profile then log_dbg("IDEGen-Vstudio: -- Generating files section..") end
    for _, filePair in ipairs(project.files or {}) do
        local file = filePair[2]
        local filter = utils.path_folder(filePair[1])
        filter = utils.str_replace(filter, "/", "\\") -- for unix/mac machines
        local filterBlock = nil
        if filter ~= "" then filterBlock = {utils.xml("Filter", {}, filter)} end

        if(utils.str_ends_with(file, ".hxx") or utils.str_ends_with(file, ".h") or utils.str_ends_with(file, ".hpp")) then
            table.insert(hdrgroup.children, utils.xml("ClInclude", {"Include", file}, filterBlock))
        elseif(utils.str_ends_with(file, ".cpp") or utils.str_ends_with(file, ".c") or utils.str_ends_with(file, ".cxx")) then
            table.insert(cppgroup.children, utils.xml("ClCompile", {"Include", file}, filterBlock))
        else
            table.insert(nonegroup.children, utils.xml("None", {"Include", file}, filterBlock))
        end

        if filter ~= "" then filters[filter] = true end
    end

    -- generate filters
    local filtergroup = utils.xml("ItemGroup", {}, {})
    for filter, _ in pairs(filters) do
        table.insert(filtergroup.children, utils.xml("Filter", {"Include", filter}, {utils.xml("UniqueIdentifier",{}, action.generate_guid("filter_" .. filter))}))
    end

    -- put it in the template
    local data = 
    utils.xml("Project", {"ToolsVersion", "4.0", "xmlns", "http://schemas.microsoft.com/developer/msbuild/2003"}, {
        itemgroups,
        filtergroup
    })

    -- generate final xml
    local xmltbl = {}
    utils.xmlgen(data, xmltbl)
    return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" .. table.concat(xmltbl)
end

function action.generate_project_user(state, project, vpairs)
    -- put it in the template
    local data = 
    utils.xml("Project", {"ToolsVersion", "Current", "xmlns", "http://schemas.microsoft.com/developer/msbuild/2003"}, {
        utils.xml("PropertyGroup", {}),
    })

    -- generate final xml
    local xmltbl = {}
    utils.xmlgen(data, xmltbl)
    return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" .. table.concat(xmltbl)
end
function action.generate_solution(state)
    
    local Pairs = action.generate_pairs(state)

    -- generate data (SolutionConfigurationPlatforms)
    local SolutionConfigurationPlatforms = "\r\n"
    for i, _ in pairs(Pairs) do
        SolutionConfigurationPlatforms = SolutionConfigurationPlatforms .. "\t\t" .. i .. "=" .. i .. "\r\n"
    end

    -- generate data (ProjectConfigurationPlatforms)
    local ProjectConfigurationPlatforms = "\r\n"
    local Projects = "\r\n"
    for _, prj in ipairs(state.idegen_vstudio.projects) do
        for i, j in pairs(Pairs) do
            ProjectConfigurationPlatforms = ProjectConfigurationPlatforms .. "\t\t" .. prj.guid .. "." ..  i .. ".ActiveCfg = " .. j .. "\r\n"
            if prj.name == state.idegen_vstudio.primary_name then -- only build primary project
                ProjectConfigurationPlatforms = ProjectConfigurationPlatforms .. "\t\t" .. prj.guid .. "." ..  i .. ".Build.0 = " .. j .. "\r\n"
            end
        end
        Projects = Projects .. "Project(\"" .. action.generate_guid("guid_prj_link_" .. prj.name) .. "\") = \"" .. prj.name .. "\", \"" .. (prj.filename or prj.name) .. ".vcxproj\", \"" .. prj.guid .. "\"\r\n"
        Projects = Projects .. "EndProject\r\n"
    end

    -- generate final string (WARNING: Don't reformat this, it's extremely sensitive to whitespaces/positioning!)
    return table.concat({"\r\n",[[
Microsoft Visual Studio Solution File, Format Version 12.00
# Visual Studio Version 16
VisualStudioVersion = 16.0.29806.167
MinimumVisualStudioVersion = 10.0.40219.1]], Projects, 
[[Global
	GlobalSection(SolutionConfigurationPlatforms) = preSolution]], SolutionConfigurationPlatforms,
[[	EndGlobalSection
	GlobalSection(ProjectConfigurationPlatforms) = postSolution]], ProjectConfigurationPlatforms,
[[	EndGlobalSection
	GlobalSection(SolutionProperties) = preSolution
		HideSolutionNode = FALSE
	EndGlobalSection
	GlobalSection(ExtensibilityGlobals) = postSolution
		SolutionGuid = ]], action.generate_guid("solution") , "\r\n", 
[[	EndGlobalSection
EndGlobal
]]})

end

function action.add_allbuild(state)
    local project = {}
    project.name = state.idegen_vstudio.projects[1].name
    project.filename = "build_all"
    for i = 1, #state.idegen_vstudio.projects do
        project.name = project.name .. " " .. state.idegen_vstudio.projects[i].name
    end
    project.guid = action.generate_guid("guid_prj_base_" .. project.name)
    table.insert(state.idegen_vstudio.projects, project)
    state.idegen_vstudio.primary_name = project.name
end

function action.execute(state)
    log_dbg("IDEGen-Vstudio: " .. state.module.name .. " preprocessing project..")
    local project = {}
    project.name = state.module.name
    project.guid = action.generate_guid("guid_prj_base_" .. project.name)
    project.defines = {}
    project.include_directories = {}
    project.files = {}

    local prefixPath = utils.path_rel(state.idegen_vstudio.path, state.module.path)
    local function select_flatten_source(v) return utils.path_normalize(prefixPath .. utils.os_pathsep .. v) end
    local function select_file_flatten_source(v) return {utils.path_normalize(v), utils.path_normalize(prefixPath .. utils.os_pathsep .. v)} end
    local function directory_flatten_source(v) return utils.path_rel(state.idegen_vstudio.path, utils.path_merge(state.module.path, v)) end

    -- extract cpp data
    if state.module.cpp and state.module.cpp.compiler then
        for _, compiler in pairs(state.module.cpp.compiler) do
            utils.arr_append(project.files, utils.arr_select(utils.arr_flatten(compiler.sources or {}), select_file_flatten_source))
            utils.arr_append(project.defines, utils.arr_flatten(compiler.defines or {}))
            utils.arr_append(project.include_directories, utils.arr_select(utils.arr_flatten(compiler.directories or {}), directory_flatten_source))
        end
    end
    if state.module.cpp and state.module.cpp.linker then
        for _, linker in pairs(state.module.cpp.linker) do
            if linker.meta and linker.meta.outputs and #linker.meta.outputs >= 1 then
                project.output = utils.str_replace(linker.meta.outputs[1], "${bin}", "bin/")
            end
        end
    end

    -- extract misc files
    local ide_glob_default = {".inl", ".hpp", ".h", ".hxx", ".lua", ".asm", ".hlsl"}
    if not state.module.ide then
        local basePath = state.module.path
        local path = state.idegen_vstudio.path

        for _, glob in ipairs(ide_glob_default) do
            utils.arr_append(project.files, utils.arr_select(fscache.glob_rel( basePath, "", glob, true ), select_file_flatten_source) )
        end
    end

    -- post process defines
    project.defines = utils.arr_unique(project.defines) -- remove non unique entries
    table.sort(project.defines)

    table.insert(state.idegen_vstudio.projects, project)  
    state.idegen_vstudio.primary_name = project.name
end

-- helpers
function action.generate_pairs(state)
    local tbl = {}
    for _, i in ipairs(state.idegen_vstudio.profiles) do
        for j, k in pairs(state.idegen_vstudio.platforms) do
            tbl[i .. "|" .. j] = i .. "|" .. k
        end
    end
    return tbl
end
function action.generate_guid(key)
    local str = md5.sumhexa(key):upper()
    return "{" .. str:sub(1,8) .. "-" .. str:sub(9,12) .. "-" .. str:sub(13,16) .. "-" .. str:sub(17,20) .. "-" .. str:sub(21) .. "}"
end

return action