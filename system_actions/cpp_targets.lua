-- *** C++ Compiler targets ***
-- Targets:
g_action.v_targets = {
    windows = {
        executableSuffix        = ".exe",
        objectFileSuffix        = ".obj",
        librarySuffix           = ".lib",
        dynamicLibrarySuffix    = ".dll",
        pchSuffix               = ".pch",
        pdbSuffix               = ".pdb",
    },
    linux = {
        executableSuffix        = "",
        objectFileSuffix        = ".o",
        librarySuffix           = ".a",
        dynamicLibrarySuffix    = ".so",
        pchSuffix               = ".pch",
        pdbSuffix               = ".debug",
    },
    mac = {
        executableSuffix        = "",
        objectFileSuffix        = ".o",
        librarySuffix           = ".a",
        dynamicLibrarySuffix    = ".dylib",
        pchSuffix               = ".pch",
        pdbSuffix               = ".debug",
    },
    emscripten = {
        executableSuffix        = ".html",
        objectFileSuffix        = ".o",
        librarySuffix           = ".a",
        dynamicLibrarySuffix    = ".so",
        pchSuffix               = ".pch",
        pdbSuffix               = ".pdb",
    }
}
