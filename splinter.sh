#!/bin/bash

# grab execution path context
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR_SYSTEM="$DIR"

# update external dependencies
if [ "$OS" == "Windows_NT" ]; then
	if [ ! -f "$DIR_SYSTEM/external/pkg/luajit.exe" ] || [ "$1" == "update" ]; then
		"$DIR_SYSTEM/external/download-deps.bat"
	fi
else
	if [ ! -f "$DIR_SYSTEM/external/pkg/.updated" ] || [ "$1" == "update" ]; then
		if [ "$1" == "update" ]; then shift; fi
		chmod a+x "$DIR_SYSTEM/external/download-deps.sh"
		"$DIR_SYSTEM/external/download-deps.sh" "${@}" || exit 1
		touch $DIR_SYSTEM/external/pkg/.updated
	fi
fi

# set lua module paths
if [ "$OS" == "Windows_NT" ]; then
	# mangle linux path to windows path
	export LUA_pth="$DIR_SYSTEM/external/pkg"
	export LUA_winpth=$(echo "$LUA_pth" | sed -e 's/^\///' -e 's/\//\\/g' -e 's/^./\0:/')
	export LUA_CPATH="./?.dll;$LUA_winpth/?.dll"
else
	export LUA_CPATH="./?.so;./?.dylib;$DIR_SYSTEM/external/pkg/?.so;$DIR_SYSTEM/external/pkg/?.dylib"
fi

# start luajit and run qs
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$DIR_SYSTEM/external/pkg"
export DYLD_LIBRARY_PATH="$LD_LIBRARY_PATH" # needed since catalina
exec $DIR_SYSTEM/external/pkg/luajit $DIR_SYSTEM/lua/cli/main.lua "${@}"
