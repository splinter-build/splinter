local sources = {}

sources.v_state = {}
sources.v_state.sources = {}

-- [[[[[[[[[[[[[[   CLI INTEROP   ]]]]]]]]]]]]]]

function sources.cli_register(clistate)
    cli.register(clistate, "listModuleSource", "~*:args:all", function(input)      sources.listModuleSource(input.parsed["args"]) end)
    cli.register(clistate, "listModuleSources", "~*:args:all", function(input)     sources.listModuleSourceAll(input.parsed["args"]) end)
    --cli.register(clistate, "update", "~*:args:all", function(input)                    sources.updateSource(input.parsed["args"]) end)
    --cli.register(clistate, "git", "~*:args:all", function(input)                       sources.git(sources.getSources(), input.parsed["args"]) end)

    -- flush caches when package config changes
    settings.hookSettingChanges(function(clobbered)
        if(clobbered == "*" or utils.str_starts_with(clobbered, "sources")) then
            sources.v_state.packageListCache = nil
        end
    end)
end


function sources.getModuleSource(module)
    local sourceFile = sources.findModuleSource(module)
    if sourceFile == nil then return nil end

    -- check cache
    if sources.v_state.sources[sourceFile] then
        return sources.v_state.sources[sourceFile]
    end

    -- load from disk
    local sandbox=utils.build_env_sandbox()
    local success, ret =  utils.eval_untrusted_file(sourceFile, sandbox)
    if success == false then
        log_err ("Source load", ret)
        return nil
    end
    sources.v_state.sources[sourceFile] = ret
    return ret
end

function sources.findModuleSource(module)
    local locations = modules.getModuleLocations()
    if locations[module] == nil then return nil end
    local path = locations[module]

    local path=utils.path_split(path)
    path[#path] = nil
    local pathlen = #path
    for i= 1,pathlen do
        local sourceFilename = utils.path_merge(utils.path_merge(table.unpack(path)), "source.spl.lua")
        if utils.file_exists(sourceFilename) then
            return sourceFilename
        end
        path[#path] = nil
    end
    return nil
end

function sources.listModuleSource(module)
    source = sources.getModuleSource(module)
    print(json.encode(source)) -- NDEBUG
end

function sources.listModuleSourceAll(module)
    local pmods = modules.getModuleLocations()
    local pmodsarr = {}
    for i, j in pairs(pmods) do
        local elem = {}
        elem[1] = i
        elem[2] = j
        elem[3] = sources.findModuleSource(i) or "-"
        table.insert(pmodsarr, elem)
    end
    table.sort(pmodsarr, function(a,b) return a[1] < b[1] end)
    utils.pretty_print(settings.get_pretty_print_mode(),  {"NAME", "PATH", "SOURCE"}, utils.arr_select(pmodsarr, function(x) return {x[1], x[2], x[3]} end))
end


-- [[[[[[[[[[[[[[[[[[[[[ SOURCE DETECTION ]]]]]]]]]]]]]]]]]]]]]

function sources.get_development_sources()
    local defValue = 1
    if os.getenv("CI") ~= nil then
        defValue = 0
    end
    return settings.getSetting("sources.dev", defValue) > 0
end

function sources.get_source_folder(destination, target)
    if target == "global" then 
        resultDir = utils.path_merge(sources.get_root_abs_global(), destination)
    else
        resultDir = utils.path_merge(sources.get_root_abs_local(), destination)
    end
    return resultDir
end

function sources.get_root_abs_global()
    return gDirs.global_modules
end

function sources.get_root_abs_local()
    return gDirs.local_modules
end

-- ** HTTP wrappers
function sources.wget(cmd, verbose)
    local exec = "wget"
    if(utils.is_windows()) then
        exec = utils.path_merge(gDirs.base_buildsystem, "external/Wget/wget.exe")
    end
    return utils.exec(exec .. " " .. cmd .. " 2>&1", function(line) if verbose then print(line) end end) -- NDEBUG
end

function sources.unzip(cmd, verbose)
    local exec = "unzip"
    if(utils.is_windows()) then
        exec = utils.path_merge(gDirs.base_buildsystem, "external/Unzip/unzip.exe")
    end
    return utils.exec(exec .. " " .. cmd .. " 2>&1", function(line) if verbose then print(line) end end) -- NDEBUG
end

function sources.sevenz(cmd, verbose)
    local exec = "7za"
    if(utils.is_windows()) then
        exec = utils.path_merge(gDirs.base_buildsystem, "external/7za/7za.exe")
    end
    return utils.exec(exec .. " " .. cmd .. " 2>&1", function(line) if verbose then print(line) end end) -- NDEBUG
end

-- ** DOWNLOAD FROM GIT
function sources.update_git_env()
    -- don't skip smudging normally
	ext.setenv("GIT_LFS_SKIP_SMUDGE", "0"); 
	
	-- suppress git ssh warnings about fingerprints and asking for yes/no
	ext.setenv("GIT_SSH_COMMAND", "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=quiet")
end

function sources.git(psources, cmd)
    sources.update_git_env()
    sources.updateState(psources)
    for k,v in pairs(psources) do
        repeat -- ugly workaround for no continue -_-'
            if (v.exists == false) then break end
            if (v.artifact ~= nil) then break end -- artifacts don't use git directly

            -- check if git package
            local sourcedef = sources.getSourceDefFromSource(v)
            if sourcedef.type ~= "gitlab" and sourcedef.type ~= "git" then break end

            -- run git command
            log_info("Running Git \"" .. cmd .. "\" for Package " .. v.name)
            local ecmd = "pushd \"" .. v.folder .. "\"\ngit " .. cmd
            utils.jobAdd("local ecmd=... utils = require \"base/utils\" utils.execmultiline(ecmd)", ecmd)
            utils.jobWaitIfFull()
            break
        until true
    end
end

function sources.unload()
    sources.v_state.sources = {}
    sources.v_state.sourcedefs = {}
end

function sources.parse(nsources, nsourcedefs)
    -- clone and augment sources with paths
    for name,source in pairs(nsources) do
        sources.v_state.sources[name] = utils.deep_copy(source)
        local targetSource = sources.v_state.sources[name]
        targetSource.folder = sources.get_source_folder(name, source.target)
    end

    -- add source defs
    for name,sourcedef in pairs(nsourcedefs) do
        sources.v_state.sourcedefs[name] = utils.deep_copy(sourcedef)
    end
end

function sources.get_token(pkgSrc)
    if pkgSrc.tokenPath ~= nil then
        local finalPath = utils.str_template_replace(pkgSrc.tokenPath, gDirs)
        if utils.file_exists(finalPath) == false then
            print("Failed to find token:", pkgSrc.tokenPath, finalPath) -- NDEBUG
            return "UNKNOWN"
        end
        return utils.file_read(finalPath)
    end
    if pkgSrc.token ~= nil then
        return pkgSrc.token
    end
    return "UNKNOWN"
end

-- ** DOWNLOAD FROM GITLAB
function sources.resolve_gitlab(srcDef)
    local verbose = false
    if srcDef.sourceDef == nil or srcDef.sourceDef.type ~= "gitlab" then 
        return false
    end

    -- try to install this package
	local lfs = srcDef.source.lfs or false
    local artifact = srcDef.source.artifact
    local branch = srcDef.source.branch or "master"
    local auth = "-nv --show-progress --progress=dot:mega --header=" .. utils.shell_escape("Private-Token: " .. sources.get_token(srcDef.sourceDef)) .. " "

    local pkgFolder = srcDef.source.folder
    utils.path_mkdir(pkgFolder)

    local pkgCacheMarker = utils.path_merge(pkgFolder, ".cache.txt")
    local pkgArchive = utils.path_merge(pkgFolder, "archive.zip") 
    local cacheData = utils.file_read(pkgCacheMarker)

    if sources.get_development_sources() and srcDef.sourceDef.gitUrlPrefix ~= nil and artifact == nil then
        if srcDef.source.exists == true then
            -- only update this package
            local thispkg = {}
            thispkg[srcDef.source.name] = srcDef.source
            sources.git(thispkg, "pull")
            local lfs = srcDef.source.lfs or false
            if lfs == true then
                sources.git(thispkg, "lfs pull")
            end
            return true
        else
            -- fetch directly through git
            local gitUrl = srcDef.sourceDef.gitUrlPrefix .. srcDef.source.repo .. (srcDef.sourceDef.giturlSuffix or "")

            sources.update_git_env()
            
            -- super slow for clone, use git lfs pull instead
            ext.setenv("GIT_LFS_SKIP_SMUDGE", "1"); 

            local cmd = "git clone " .. utils.shell_escape(gitUrl) .. " " .. pkgFolder .. " " .. settings.getSetting("sources.args.gitclone", "--depth 1")
            cmd = cmd .. "\npushd \"" .. pkgFolder .. "\""
            if lfs == true then
                cmd = cmd .. "\ngit lfs pull"
            end

            utils.jobAdd("local ecmd=... utils = require \"base/utils\" utils.execmultiline(ecmd)", cmd)
            utils.jobWaitIfFull()
        end
    elseif(artifact == nil) then
        local archiveUrl =  srcDef.sourceDef.url .. "/api/v4/projects/" .. utils.urlencode(srcDef.source.repo) .. "/repository/archive.zip?sha=" .. utils.urlencode(branch)
        local lines=sources.wget( auth .. "-S --spider " .. utils.shell_escape(archiveUrl), verbose )
        local filename=utils.arr_select(lines, function(l) return l:match("Content%-Disposition: attachment; filename=\"(.*).zip\"") end)
        local pkgSubfolder = utils.path_merge(pkgFolder, filename[1])

        if cacheData ~= filename[1] then
            log_dbg("Downloading source", srcDef.source.name)
            utils.path_mkdir(pkgFolder)
            utils.remove_folder(pkgFolder)
            utils.path_mkdir(pkgFolder)
            local lines=sources.wget( auth .. "-O " .. utils.shell_escape(pkgArchive) .. " " ..  utils.shell_escape(archiveUrl), true )
            log_dbg("Unpacking source", srcDef.source.name)
            sources.unzip("-u -X " .. utils.shell_escape(pkgArchive) .. " -d " .. utils.shell_escape(pkgFolder), verbose)
            log_dbg("Moving source contents", srcDef.source.name)
            utils.move_folder_contents(pkgSubfolder, pkgFolder )
            utils.remove_folder(pkgSubfolder)
            os.remove(pkgArchive)
            utils.file_write(pkgCacheMarker, "w", filename[1])
        else
            log_dbg("Already has latest source", srcDef.source.name, filename[1])
        end
    else
        local archiveUrl = srcDef.sourceDef.url .. "/api/v4/projects/" .. utils.urlencode(srcDef.source.repo) .. "/jobs/artifacts/" .. utils.urlencode(branch) .. "/download?job=" .. utils.urlencode(artifact)
        local lines=sources.wget( auth .. "-S --spider " .. utils.shell_escape(archiveUrl), verbose )
        local modified=utils.arr_select(lines, function(l) return l:match("Last%-Modified: (.*)") end)
        if cacheData ~= modified[1] then
            log_dbg("Downloading source", srcDef.source.name)
            utils.path_mkdir(pkgFolder)
            utils.remove_folder(pkgFolder)
            utils.path_mkdir(pkgFolder)
            local lines=sources.wget( auth .. "-O " .. utils.shell_escape(pkgArchive) .. " " ..  utils.shell_escape(archiveUrl), true )
            log_dbg("Unpacking source", srcDef.source.name)
            sources.unzip("-u -X " .. utils.shell_escape(pkgArchive) .. " -d " .. utils.shell_escape(pkgFolder), verbose)
            os.remove(pkgArchive)
            utils.file_write(pkgCacheMarker, "w", modified[1])
        else
            log_dbg("Already has latest source", srcDef.id, modified[1])
        end
    end
    return true
end

-- ** UPDATER
function sources.updateSource(name)
    -- make sure source folder exists
    utils.path_mkdir(sources.get_root_abs_global())
    utils.path_mkdir(sources.get_root_abs_local())

    -- update source installed states
    local src = sources.getFilteredSources(name)
    if #src == 0 then log_info("No source found with name", name) return end

    sources.updateState(src)
    for k,v in pairs(src) do
        local sourceDef = sources.getSourceDefFromSource(v)

        if sourceDef == nil then log_info("Error: Missing sourcedef (name, sourcedef)", v.name, v.sourcedef)
        else
            local srcDef= {source=v, sourceDef=sourceDef}
            log_info("Updating source", srcDef.source.name)

            -- Resolve.. - List of resolvers here:
            local resolved = false
            if resolved == false then resolved =  sources.resolve_gitlab(srcDef) end
            if resolved == false then log_info("Error: No valid resolver for source", srcDef.source.name) end
        end
    end

    -- wait for jobs to finish
    utils.jobWaitForAll()
end

return sources
