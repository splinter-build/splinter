local modules = {}


modules.v_state = {}
modules.v_state.loadedList = {}
modules.v_state.loaded = {}
modules.v_state.sources = {}

-- [[[[[[[[[[[[[[   CLI INTEROP   ]]]]]]]]]]]]]]

function modules.cli_register(clistate)
    cli.register(clistate, "listLoadedModules", "~*:args:all",          function(input) modules.listLoaded(input.parsed["args"]) end)
    cli.register(clistate, "listLoadedModulesDetails", "~*:args:all",   function(input) modules.listSingle(input.parsed["args"]) end)
    cli.register(clistate, "listModules", "~*:args:all",                function(input) modules.listLocalModules(input.parsed["args"]) end)
    cli.register(clistate, "loadModule", "~*:args:all",                 function(input) modules.load({input.parsed["args"]}, true, true) end )
end

-- Loaded modules
function modules.getLoadedModules()
    return modules.v_state.loaded
end
function modules.getLoadedModule(args, silent)
    local mods = modules.getLoadedModules()
    local mod = mods[args]
    if silent ~= true and mod == nil then
        log_err_notrace("No such module found (" .. tostring(args) .. ").")
        return nil
    end
    return mod 
end
function modules.insert_require(mod, require)
    local origRequire = require
    table.insert(mod.meta.p_requires, require)
end
function modules._handlePreprocessRequire(mod, cache, requireLookup, j)
    if utils.str_starts_with(j, "match:") then
        local matchStr = utils.str_skip(j, "match:")

        -- build match cache element
        if(cache[j] == nil) then
            log_dbg("Matching module regex", matchStr)
            local c = {}
            for k,l in pairs(modules.getModuleLocations()) do
                if(utils.str_re_ismatch(k, matchStr)) then
                    table.insert(c, k)
                end
            end
            cache[j] = c
        end

        -- use cache
        local c = cache[j]
        for k,l in ipairs(c) do
            if requireLookup[l] == nil then
                requireLookup[l] = true
                modules.insert_require(mod, l)
            end
        end
    else
        local l=j
        if  requireLookup[l] == nil then
            requireLookup[l] = true
            modules.insert_require(mod, l)
        end
    end
end
function modules._preprocessRequireTable(mod, cache, requireLookup, tbl)
    -- unroll matches recursively
    for i,j in ipairs(tbl or {}) do
        if(type(j) == "table") then
            modules._preprocessRequireTable(mod, cache, requireLookup, j)
        else
            modules._handlePreprocessRequire(mod,cache,requireLookup,j)
        end
    end
end
function modules.preprocessRequire(mod, cache)
    local requireLookup = {}
    if mod.meta == nil then mod.meta = {} end
    mod.meta.p_requires = {}
    modules._preprocessRequireTable(mod, cache, requireLookup, mod.requires)
end
function modules.parse(pmodules)
    local loadedModules = modules.getLoadedModules()

    for i,j in pairs(pmodules) do
        loadedModules[i] = j
    end

    local cache = {}
    for i,j in pairs(pmodules) do
        modules.preprocessRequire(j, cache)
    end
end
function modules.loadRequirements(pmodules)
    for modname,mod in pairs(pmodules) do
        if #mod.meta.p_requires > 0 then
            modules.load(mod.meta.p_requires, false, true)
        end
    end
end
function modules.updateState(pmodules)
    for i,j in pairs(pmodules) do
        if j.source ~= nil then
            j.exists = sources.hasSourceInstalled(j.source)
            local actualSource = sources.getUpdatedSource(j.source)
            j.folder = actualSource.folder
        else
            j.exists = true
            j.folder = ""
        end
    end
end
function modules.walkRequireGraph(vmod, func, state)
    if state == nil then state = {} state.depth = 0 state.custom={} state.parent=nil end
    
    local mods = modules.getLoadedModules()
    local mod = mods[vmod]
    if mod == nil then print("ERROR: Can't find module", vmod, "from", tostring(state.parent)) return end -- NDEBUG

    local ret = func(mod, state)

    if ret ~= false then
        for _,i in ipairs(mod.meta.p_requires) do
            state.parent = vmod
            state.depth = state.depth + 1
            modules.walkRequireGraph(i, func, state)
            state.depth = state.depth - 1
        end
    end
end
function modules.listLoaded(args)
    local pmods = modules.getLoadedModules()
    local pmodsarr = utils.table_toarr_value(pmods)
    table.sort(pmodsarr, function(a,b) return a.name < b.name end)
    utils.pretty_print(settings.get_pretty_print_mode(),  {"NAME", "DESC", "PATH", "SOURCE"}, utils.arr_select(pmodsarr, function(x) return {x.name, x.description or "-", x.path, x.source or "-"} end))
end
function modules.listSingle(args)
    local mods = modules.getLoadedModules()
    local mod = mods[args]
    if mod == nil then
        print("No such module found.") -- NDEBUG
        return false
    end

    print("Name: " .. mod.name) -- NDEBUG
    print("Source: " .. (mod.source or "N/A")) -- NDEBUG
    print("Source exists: " .. tostring(sources.hasSource(mod.source))) -- NDEBUG
    print("Source installed: " .. tostring(sources.hasSourceInstalled(mod.source))) -- NDEBUG
    modules.walkRequireGraph(mod.name, function(mod, state)
        if state.custom[mod.name] ~= nil then return end
        io.write("  " .. utils.str_repeat("  ", state.depth) .. mod.name) 
        io.write("\n")
        state.custom[mod.name] = true 
    end)
    if next(mod.meta.p_requires) ~= nil then
        print("Requires:") -- NDEBUG
    end
    for _,i in ipairs(mod.meta.p_requires) do
        local yn = "N"
        if mods[i] ~= nil then yn = "Y" end
        print("  [" .. yn .. "] " .. i) -- NDEBUG
    end
end
function modules.getSources(mod)
    -- Find required mods
    local requiredMods = {}
    modules.walkRequireGraph(mod.name, function(mod, state)
        if(requiredMods[mod.name] == nil or requiredMods[mod.name] < state.depth) then 
            requiredMods[mod.name] = state.depth 
        end
    end)

    -- Sort based on depth
    local requiredModsList = {}
    for name,depth in pairs(requiredMods) do
        if requiredModsList[depth] == nil then requiredModsList[depth] = {} end
        table.insert(requiredModsList[depth], name)
    end

    print(json.encode(requiredModsList)) -- NDEBUG
end
function modules.expandWildcards(mods)
    cache.update()
    local lst = {}
    for _, modFile in pairs(mods) do
        if string.find(modFile, "*") then
            -- handle wildcards
            local matchStr = string.gsub(utils.pattern_escape(modFile), "%%%*", ".*")
            for sourceName, sourceFile in pairs(modules.v_state.sources) do
                if utils.str_re_ismatch(sourceName, matchStr) then
                    table.insert(lst, sourceName)
                end
            end
        else
            table.insert(lst, modFile)
        end
    end
    return lst
end
function modules.load(mods, allowDuplicate, allowRecursive)
    cache.update()
    local lst = {}
    for _, modFile in pairs(mods) do
        if modFile ~= "" and modFile ~= nil then
            -- VARIANT HANDLING
            local identifier = modFile
            local variantParse = utils.str_split(modFile, ":")
            local variant
            
            if #variantParse > 1 then
                modFile = variantParse[1]
                variant = variantParse[2]
            end
            local modSource = modules.v_state.sources[modFile] 
           
            if modSource == nil then
                ret = false
                log_info("Failed to load module: ", modFile)
                return false
            end
            if allowDuplicate == true or modules.v_state.loadedList[identifier] == nil  then
                table.insert(lst, {file=modFile, source=modSource, variant=variant, id=identifier})
            end
        end
    end

    -- load modules
    for _, modPair in pairs(lst) do
        log_dbg("Loading module", modPair.file, modPair.source, modPair.variant, modPair.id)
        scripts.setActiveModule(modPair.id, modPair.source)
        scripts.parseFile(modPair.source)
        scripts.setActiveModule(nil)
        modules.v_state.loadedList[modPair.id] = modPair.source
    end

    sources.parse(scripts.get_sources(), scripts.get_source_definitions())
    scripts.reset_sources()
    scripts.reset_source_definitions()

    compilers.parse(scripts.get_compilers())
    scripts.reset_compilers()

    local newModules = scripts.get_modules()
    scripts.reset_modules()

    modules.parse(newModules)

    if allowRecursive then
        modules.loadRequirements(newModules)
    end

    return true
end
function modules.unloadLoaded()
    modules.v_state.loadedList = {}
    modules.v_state.loaded = {} 
end

-- Potential modules (sources)
function modules.getModuleLocations()
    cache.update(false)
    return modules.v_state.sources
end
function modules.getModuleLocation(moduleName)
    return modules.getModuleLocations()[moduleName]
end
function modules.addLocationToList(moduleName, moduleSource)
    modules.v_state.sources[moduleName] = moduleSource
end
function modules.listLocalModules(args)
    local pmods = modules.getModuleLocations()
    local pmodsarr = utils.table_toarr_kv(pmods)
    table.sort(pmodsarr, function(a,b) return a[1] < b[1] end)
    utils.pretty_print(settings.get_pretty_print_mode(),  {"NAME", "PATH"}, utils.arr_select(pmodsarr, function(x) return {x[1], x[2]} end))
end

return modules