local scripts = {}

function scripts.get_sources()              return scripts._state.sources       end
function scripts.get_source_definitions()   return scripts._state.sourcedefs    end
function scripts.get_modules()              return scripts._state.modules       end
function scripts.get_compilers()            return scripts._state.compilers     end
function scripts.get_custom_prefix()        return settings.getSetting("build.env.prefix", "") end
function scripts.get_build_dir()            return utils.path_merge(gDirs.local_build, scripts.get_custom_prefix()) end

-- API Interface
function scripts.spl_module_require(defs)
    assert(type(defs) == "table")
    assert(#defs == 1)
    local modname = scripts._state.activemodule
    if(modname == nil) then log_err("Can't set module: No active module currently available!") return end
    
    local module = scripts._state.modules[modname]
    table.insert(module.requires, defs[1])
end

function scripts.spl_module(module, opts)
    opts = opts or {}
    local modname = scripts._state.activemodule
    if(module == nil) then
        return scripts._state.modules[modname]
    end
    if(type(module) ~= "table") then log_err("Can't set module: Argument not a table") return end
    if(modname == nil) then log_err("Can't set module: No active module currently available!") return end
    if(module.requires == nil) then module.requires = {} end

    if modname:find(":") then
        -- VARIANT HANDLING
        local nameSplit = utils.str_split(modname, ":")
        module.basename = nameSplit[1]
        module.variant = nameSplit[2]
    else
        -- no variant.
        module.basename = modname
        module.variant = ""
    end

    module.name = modname
    module.scriptpath = scripts._state.activemodulepath
    module.path = utils.path_folder(module.scriptpath)
    if scripts._state.modules[modname] == nil then
        scripts._state.modules[modname] = module
    else
        if opts.override_array then 
            utils.table_merge_override_array(scripts._state.modules[modname], module)
        else
            utils.table_merge(scripts._state.modules[modname], module)
        end
    end
end

function scripts.spl_add_compiler(compiler)
    assert(compiler.name ~= nil)
    assert(compiler.type ~= nil)
    assert(compiler.prio ~= nil)
    table.insert(scripts._state.compilers,compiler)
end

local spl_get_tbl = {
    merge = function(arg1, arg2, arg3, arg4) 
        utils.table_merge(arg1, arg2)
    end,
    flatten = function(arg1, arg2, arg3, arg4)
        return utils.arr_flatten(arg1)
    end,
    deep_copy = function(arg1, arg2, arg3, arg4)
        return utils.deep_copy(arg1)
    end,
    mod_name = function(arg1, arg2, arg3, arg4)
        return scripts._state.activemodule
    end,
    mod_name_sanitized = function(arg1, arg2, arg3, arg4)
        return settings.sanitize_build_output_filename_specials_only(scripts._state.activemodule)
    end,
    mod_basename = function(arg1, arg2, arg3, arg4)
        return scripts._state.modules[scripts._state.activemodule].basename
    end,
    mod_variant = function(arg1, arg2, arg3, arg4)
        return scripts._state.modules[scripts._state.activemodule].variant
    end,
    mod_path = function(arg1, arg2, arg3, arg4)
        return utils.path_merge(utils.path_folder(scripts._state.activemodulepath), arg1, arg2, arg3, arg4)
    end,
    build_path = function(arg1, arg2, arg3, arg4)
        return utils.path_merge(scripts.get_build_dir(), arg1, arg2, arg3, arg4)
    end,
    mod_location = function(arg1, arg2, arg3, arg4)
        local locs = modules.getModuleLocations()
        return locs[arg1]
    end,
    which = function(arg1, arg2, arg3, arg4)
        return utils.exec('which ' .. utils.shell_escape(arg1))[1]
    end,
    path_exists = function(arg1, arg2, arg3, arg4)
        return utils.path_exists(arg1)
    end,
    path_folder = function(arg1, arg2, arg3, arg4)
        return utils.path_folder(arg1)
    end,
    path_filename = function(arg1, arg2, arg3, arg4)
        return utils.path_filename(arg1)
    end,
    path_dirs = function(arg1, arg2, arg3, arg4)
        return utils.path_listdir(arg1, 1, false)
    end,
    path_files = function(arg1, arg2, arg3, arg4)
        return utils.path_listdir(arg1, 0, false)
    end,
    path_merge = function(arg1, arg2, arg3, arg4)
        return utils.path_merge(arg1, arg2, arg3, arg4)
    end,
    path_rel = function(arg1, arg2, arg3, arg4)
        return utils.path_rel(arg1, arg2)
    end,
    os_name = function(arg1, arg2, arg3, arg4)
        return utils.os_name
    end,
    os_pathsep = function(arg1, arg2, arg3, arg4)
        return utils.os_pathsep 
    end,
    compiler = function(arg1, arg2, arg3, arg4)
        -- target, host_os, type, cpp_compiler
        local compilers =    compilers.detect_filtered_pkgs(arg1, arg2) 
        local cpp_compilers = utils.arr_which(compilers, function(x) return x.type == arg3 and (arg4 == "any" or arg4 == x.name) end)
        return utils.deep_copy(cpp_compilers)
    end
}

spl_get_tbl["json.encode"] = function(arg1, arg2, arg3, arg4)
    return json.encode(arg1)
end

spl_get_tbl["json.decode"] = function(arg1, arg2, arg3, arg4)
    return json.decode(arg1)
end

function scripts.spl_get(varname, arg1, arg2, arg3, arg4)
    if varname == nil or spl_get_tbl[varname] == nil then log_err("spl_get: unknown varname: " .. varname) return false end
    return spl_get_tbl[varname](arg1, arg2, arg3, arg4)
end

function scripts.spl_get_variable(var, default)
    return settings.getSetting(var, default)
end

function scripts.spl_get_variable_bool(var, default)
    local v= settings.getSetting(var, default)
    v = tostring(v)
    if v == "1" or v == "true" or v == "True" or v == "TRUE" then return true end
    return false
end

function scripts.spl_glob(path, suffix, recursive)
    local modname = scripts._state.activemodule
    if(modname == nil) then log_err("Can't glob module: No active module currently available!") return end
    local basePath = scripts._state.modules[modname].path
    return fscache.glob_rel( basePath, path, suffix, recursive )
end

function scripts.spl_require(scriptName, optional)
    local env = scripts._state.activesandbox
    if(env.spl_required_scripts[scriptName] ~= nil) then return end -- already included, skip
    env.spl_required_scripts[scriptName] = true

    local scriptFile = scripts._pstate.detect.scripts[scriptName]
    if(scriptFile == nil) then if optional ~= true then log_err_notrace("Can't find script: " .. scriptName) end return false end
    
    local dofile=dofile
    
    local chunk, error
    if scripts._state.cachedScripts[scriptFile] == nil then
        log_dbg("Loading script", scriptName, scriptFile)
        chunk, error = loadfile(scriptFile)
        if chunk == nil then log_err_notrace("Script parse error", scriptName, error) return end
        scripts._state.cachedScripts[scriptFile] = chunk
    else
        chunk = scripts._state.cachedScripts[scriptFile]    
    end

    setfenv(chunk, env)
    chunk()
    --log_dbg("Done executing script", scriptName, scriptFile)
    return true
end

function scripts.spl_merge(a,b)
    utils.table_merge(a,b)
end

function scripts.spl_log(...)
    log_info(...)
end

-- Module Parser
function scripts.parse(filename)
    local sandbox = utils.build_env_sandbox()
    scripts._state.activesandbox = sandbox
    sandbox.spl_required_scripts = {}
    sandbox.spl_add_compiler = scripts.spl_add_compiler
    sandbox.spl_get_variable = scripts.spl_get_variable
    sandbox.spl_get_variable_bool = scripts.spl_get_variable_bool
    sandbox.spl_module = scripts.spl_module
    sandbox.spl_module_require = scripts.spl_module_require
    sandbox.spl_merge = scripts.spl_merge
    sandbox.spl_glob = scripts.spl_glob
    sandbox.spl_get = scripts.spl_get
    sandbox.spl_require = scripts.spl_require
    sandbox.spl_log = scripts.spl_log

    local success, ret =  utils.eval_untrusted_file(filename, sandbox)
    if success == false then
        print ("ERROR: " .. ret)
        return false
    end
    return true
end

function scripts.parseFile(filename)
    return scripts.parse(filename)
end

function scripts.setActiveModule(moduleName, modulePath)
    scripts._state.activemodule = moduleName
    scripts._state.activemodulepath = modulePath
    if(moduleName ~= nil) then scripts.spl_module({}) end -- set default module
end
function scripts.reset()
    scripts._state = {}
    scripts._state.activemodule = nil
    scripts._state.activesandbox = nil
    scripts._state.sourcedefs = {}
    scripts._state.sources = {}
    scripts._state.modules = {}
    scripts._state.compilers = {}
    scripts._state.cachedScripts = {}

    -- persistent state
    if(scripts._pstate == nil) then scripts._pstate = {} end
    if(scripts._pstate.detect == nil) then scripts._pstate.detect = {} end
    if(scripts._pstate.detect.scripts == nil) then scripts._pstate.detect.scripts = {} end
end
function scripts.reset_sources() scripts._state.sources = {} end
function scripts.reset_source_definitions() scripts._state.sourcedefs = {} end
function scripts.reset_compilers() scripts._state.compilers = {} end
function scripts.reset_modules() scripts._state.modules = {} end

function scripts.detect(folders)
    scripts._pstate.detect = {}
    scripts._pstate.detect.scripts = {}

    local files
    local moduleSuffix = ".module.spl.lua"
    local moduleSuffixLen = moduleSuffix:len()
    local scriptSuffix = ".script.spl.lua"
    local scriptSuffixLen = scriptSuffix:len()
    for _, folder in pairs(folders) do
        files = fscache.glob(folder, ".spl.lua");
        for _, file in pairs(files) do
            if(utils.str_ends_with(file, moduleSuffix)) then
                local filename = utils.path_filename(file)
                modules.addLocationToList(filename:sub(1, filename:len()-moduleSuffixLen ), file)
            elseif(utils.str_ends_with(file, scriptSuffix)) then
                local filename = utils.path_filename(file)
                local name = filename:sub(1, filename:len()-scriptSuffixLen)
                scripts._pstate.detect.scripts[name] = file
            end
        end
    end
end

scripts.reset()

return scripts