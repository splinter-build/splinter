
local fscache = {}
fscache.state = {}
fscache.state.trees = {}
fscache.state.enabled = true

function fscache.addCache(path)
    path = utils.path_normalize(utils.path_abs(path))

    -- check if not already cached
    if(fscache.state.trees[path] ~= nil) then return end

    if fscache.state.enabled then -- QQQ: Disable cache for now..
        local cache = ext.dirlib_new(path)
        fscache.state.trees[path] = cache
    end
end

function fscache.glob(path, suffix, recursive)
    path = utils.path_normalize(utils.path_abs(path))

    local pathlen = path:len()
    local longestlen = 0

    local dir
    for i,j in pairs(fscache.state.trees) do
        local subtreelen = i:len()
        
        if (subtreelen >= longestlen) and (pathlen >= subtreelen) and path:sub(1, subtreelen) == i then
            longestlen = subtreelen
            dir = ext.dirlib_dir(j, utils.path_rel(i, path))
        end
    end

    -- scan live
    local cache
    if(dir == nil) then
        if fscache.state.enabled then
            log_dbg("FSCache: Performance warning! Scanning outside of cache: ", path) -- QQQ: cache disabled
        end
        cache=ext.dirlib_new(path)
        dir=ext.dirlib_dir(cache)
    end
    local files
    if recursive == true or recursive == nil then
        files = ext.dirlib_dir_iteratesuffix(dir, suffix)
    else
        local folder = ext.dirlib_dir_namefull(dir)
        local tfiles = ext.dirlib_dir_files(dir)
        files = {}
        for i,j in ipairs(tfiles) do
            if utils.str_ends_with(j, suffix) then
                table.insert(files, folder .. utils.os_pathsep .. j)
            end
        end
    end
    
    if cache ~= nil then
        ext.dirlib_free(cache)
    end

    return files
end

function fscache.glob_rel(relativeTo, path, suffix, recursive)
    if relativeTo == nil then relativeTo = "" end
    local relAbsPath = utils.path_abs(relativeTo)
    local absPath = utils.path_abs(utils.path_merge(relAbsPath, path))
    
    local relModPath = utils.path_rel(relAbsPath, absPath)
    local glob = fscache.glob(absPath,suffix, recursive)
    local cnt = #glob
    local absPathLen = absPath:len()
    if #relModPath == 0 then absPathLen = absPathLen + 2 else absPathLen = absPathLen + 1 end
    for i= 1,cnt do
        glob[i] = relModPath .. glob[i]:sub(absPathLen)
    end
    return glob
end

function fscache.unload()
    for i,j in pairs(fscache.state.trees) do
        ext.dirlib_free(j)
    end
    fscache.state.trees = {}
end

return fscache