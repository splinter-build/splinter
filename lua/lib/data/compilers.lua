local compilers = {}

compilers.v_state = {}

-- [[[[[[[[[[[[[[   CLI INTEROP   ]]]]]]]]]]]]]]

function compilers.cli_register(clistate)
    cli.register(clistate, "listLoadedCompilers", "~*:args:nil", function(input) compilers.list(input.parsed["args"]) end)
    cli.register(clistate, "listLoadedCompilerDetails", "~*:args:all", function(input) compilers.listSingle(input.parsed["args"]) end)
end

function compilers.getCompilers()
    if compilers.v_state.compilersDirty then
        table.sort(compilers.v_state.compilers, function(x, x2) return (x.prio or 0) > (x2.prio or 0) end)
        compilers.v_state.compilersDirty = false
    end
    return compilers.v_state.compilers
end

function compilers.unload()
    compilers.v_state.compilersDirty = true
    compilers.v_state.compilers = {}
    compilers.v_state.compilerByName = {}
end

function compilers.parse(pcompilers)
    for i,j in pairs(pcompilers) do
        if compilers.v_state.compilerByName[j.name] == nil then
            compilers.v_state.compilersDirty = true
            table.insert(compilers.v_state.compilers, j)
            compilers.v_state.compilerByName[j.name] = j
        end
    end
end

function compilers.updateState(compilers)
    for i,j in pairs(compilers) do
        if j.module ~= nil then
            local module = modules.getModuleLocation(j.module)
            j.exists = module ~= nil
            j.folder = utils.path_folder(module)
        else
            j.exists = true
            j.folder = ""
        end
    end
end

-- [[[[[[[[[[[[[[[[[[[[[ BUILTIN sources ]]]]]]]]]]]]]]]]]]]]]
function compilers.detect_pkgs()
    return compilers.getCompilers()
end

function compilers.detect_filtered_pkgs(target, host_os)
    local pkgs = compilers.detect_pkgs()

    -- filter by target
    if target ~= nil then
        pkgs = utils.arr_which(compilers.getCompilers(), function(x) 
            if (host_os ~= nil and x.host_os ~= nil) then 
                if (x.host_os ~= host_os) then return false end
            end
            return x.target == target or x.target == nil or x.target == "all"
        end)
    end

    compilers.updateState(pkgs)

    return pkgs
end

function compilers.format_download_pkg(pkg)
    local source = sources.v_state.sources.sources[pkg.source]
    if source == nil then return "ERROR: Invalid source - not found." end

    if source.type == "gitlab" then
        local extra = ""
        if pkg.artifact then extra = ", Artifact: " .. pkg.artifact end
        return "Gitlab Repo: " .. (pkg.repo or "ERROR") .. extra
    else
        return "ERROR: Unknown source type: " .. source.type
    end
end

function compilers.list(args)
    if(args == "nil") then args = nil end
    local pcompilers = compilers.detect_filtered_pkgs(args)
    utils.pretty_print(settings.get_pretty_print_mode(),  {"TYPE", "HOST", "TARGET", "NAME", "DESC", "MODULE", "EXISTS", "PRIO"}, utils.arr_select(pcompilers, function(x) return {x.type, x.host_os or "all", x.target or "all", x.name, x.pretty_name or x.name, x.module or "-", x.exists, x.prio} end))
end

function compilers.listSingle(name)
    local compilers = compilers.v_state.compilerByName[name]
    if compilers == nil then print("Unknown compiler specified.") return nil end -- NDEBUG
    print(json.encode(compilers)) -- NDEBUG
end

compilers.unload()

return compilers
