local cli={}
function cli.init()
    local state = {}
    state.functions = {}
    state.history = {}
    state.historyMaxSize = 50

    if cli.isUserInput() and cli.historyLoaded == nil then
        if linenoise and linenoise.historyfree then linenoise.historyfree() end
        cli.do_load_history_linenoise(state)
        cli.historyLoaded = true
    end

    return state
end
function cli.initAutocompleteCallbacks(clistate)
    g_aclstate = clistate
    local clbs = {}
    clbs.write_line = function(v) 
        g_state = cli.parse(g_aclstate, v)
        io.write(" > ") 
        if(g_state.cmdHandler == nil) then
            ext.setcolor("red")
            ext.setcolor("bright")
            io.write(v)
        else
            ext.setcolor("reset")
            io.write(g_state.cmdString)
            if(g_state.input.ok == false) then
                ext.setcolor("red")
                ext.setcolor("bright")
            end
            io.write(v:sub(#g_state.cmdString + 1))
        end
        ext.setcolor("reset")
    end
    clbs.output = function(v)
        ext.setcolor("reset")
        if(g_state.input ~= nil and g_state.input.err ~= nil) then
            io.write("Error: " .. g_state.input.err)
        else
            print("                                                                        ") -- NDEBUG
        end
    end
    clbs.autocomplete = function(str)
        local tbl = {}
        local strLower = str:lower()
        if(utils.str_contains(str, " ") == false) then
            -- autocomplete for commands
            for k,v in pairs(g_aclstate.functions) do
                local kLower = k:lower()
                if(utils.str_starts_with(kLower, strLower)) then
                    table.insert(tbl, v.name) 
                end
            end
        else
            -- command autocompletion
            local parsed = cli.parse(g_aclstate, str)
            local lastParsed = parsed[#parsed]
            if(lastParsed.cmdHandler.autocomplete) then
                tbl = utils.try(function() 
                    return lastParsed.cmdHandler.autocomplete(lastParsed)
                end, function(err)
                    print("error occurred:", err) -- NDEBUG
                end)
            end
        end
        return tbl
    end
    return clbs
end

-- func_args syntax:
-- example: s:field_1|~i:field_2:42|~*:field_3:foo_bar
-- * <args> = <field>[|<field>] ..
-- * <field> = [<optional>]<type>:<field_name>[:<default_value>]
-- * seperate fields by pipe '|' characters
-- * <optional> = start with '~' if there should be a default value. otherwise the field is required.
-- * <type> is
--     'i' : integer
--     's' : string
--     '*' : remainder string (combine remaining tokens into one string)
-- * <field_name> is user defined. Will be used as the name to expose the field by
-- * :<default_value> is only required when the <optional> flag is set (~).
--   It will serve as the default value if nothing has been provided by the user.

function cli.register(state, func_name, func_args, func_callback)
    local funcstate = {}
    funcstate.name = func_name
    funcstate.args = func_args
    funcstate.callback = func_callback
    state.functions[func_name:upper()] = funcstate
end

function cli.registerAutocomplete(state, func_name, func_autocomplete)
    if(state.functions[func_name:upper()] == nil) then error("registerAutocomplete: invalid func name: " .. func_name) end
    state.functions[func_name:upper()].autocomplete = func_autocomplete 
end

function cli.parseInput(func, input)
    local inputRet = {}
    inputRet.ok = false
    inputRet.args = input
    local expectedArgs = utils.str_split(func.args, "|")

    inputRet.ok = true
    inputRet.err = ""
    inputRet.errFields = {}
    inputRet.parsed = {}
    inputRet.parsedArray = {}
    for i=1,#expectedArgs do
        local val = inputRet.args[i]
        local argReq = utils.str_split(expectedArgs[i], ":")
        
        if val == nil then
            if utils.str_starts_with(argReq[1], "~") then
                -- optional support
                val = argReq[3]
                argReq[1] = argReq[1]:sub(2)
            else
                inputRet.err = inputRet.err .. "Field \"" .. argReq[2] .. "\" is non-optional but the provided value was Null.\n"
                table.insert(inputRet.errFields, argReq[2])
                inputRet.ok = false
                break
            end
        else
            if utils.str_starts_with(argReq[1], "~") then
                argReq[1] = argReq[1]:sub(2)
            end
        end
        
        if argReq[1] == "i" then
            -- integer
            local no = tonumber(val)
            if( no ~= nil) then
                inputRet.parsed[argReq[2]] = no
            else
                inputRet.err = inputRet.err .. "Field \"" .. argReq[2] .. "\" expected a number, but got \"" .. val .. "\" instead.\n"
                table.insert(inputRet.errFields, argReq[2])
                inputRet.ok = false
            end
        elseif argReq[1] == "s" then
            inputRet.parsed[argReq[2]] = val
        elseif argReq[1] == "*" then
            local ret = {}
            table.insert(ret, val)
            inputRet.parsed[argReq[2]] = val
            for j = i+1, #inputRet.args do
                inputRet.parsed[argReq[2]] = inputRet.parsed[argReq[2]] .. " " .. inputRet.args[j]
                table.insert(ret, inputRet.args[j])
            end
            inputRet.parsedArray[argReq[2]] = ret
        else
            print("unknown field type " .. argReq[2] .. " -> " .. argReq[1]) -- NDEBUG
        end            
    end

    if (inputRet.err == "") then inputRet.err = nil end
    if (#inputRet.errFields == 0) then inputRet.errFields = nil end
    return inputRet
end
function cli.argparse(input)
    return utils.str_argparse(input, " ", "\"", ";")
end
function cli.parse(state, input)
    if state == nil then log_err("no state") end
    if input == nil or input == "" or utils.str_trim(input) == "" then 
        local parsed = {}  
        parsed.err = "Input string was empty."
        parsed.errCode = 1
        return {parsed}
    end

    -- parse data
    local ret = {}
    local argdata = cli.argparse(input)
    for _, cmd in ipairs(argdata) do
        local parsed = {}
        parsed.cmdString = cmd[1]
        local subargs = {}
        for i=2, #cmd do table.insert(subargs, cmd[i]) end
        parsed.cmdArgs = subargs

        -- check for func handler
        local func = state.functions[utils.str_trim(parsed.cmdString):upper()]
        if func ~= nil then
            parsed.cmdHandler = func
            parsed.input = cli.parseInput(func, subargs)
        else
            parsed.input = {}
            parsed.input.ok = false
            parsed.input.err = "Unknown function.\n"
        end
        table.insert(ret, parsed)
    end
    return ret
end

function cli.cli_execute_file_autocomplete(parsed)
    -- fetch all possible files
    local scriptpaths = {}
    table.insert(scriptpaths, utils.path_merge(gDirs.user, "scripts"))
    table.insert(scriptpaths, utils.path_merge(gDirs.base_buildsystem, "scripts"))

    local files = {}
    for _, path in ipairs(scriptpaths) do
        utils.table_merge(files, fscache.glob_rel( path, "", ".txt", true ))
    end

    files = utils.arr_select(files, function(x) return utils.path_noext(x) end)

    local totalTbl = files

    -- autocomplete logic
    local tbl = {}
    local prefix = parsed.cmdString .. " "
    
    local argLower = ""
    if parsed.cmdArgs and parsed.cmdArgs[1] then argLower = parsed.cmdArgs[1]:lower() end

    for _,k in ipairs(totalTbl) do
        if utils.str_starts_with(k:lower(), (argLower)) then
            table.insert(tbl, prefix .. k .. " ")
        end
    end
    table.sort(tbl)
    
    return tbl
end

function cli.cli_execute_file(clistate, dbg, input)
    local cmds = utils.str_split(input.parsed.path, " ")
    for _, cmd in ipairs(cmds) do  
        -- try user folder
        local scriptpath = utils.path_merge(gDirs.user, "scripts", cmd .. ".txt")
        local data = utils.file_read(scriptpath)
        if data == nil then
            -- try system folder
            scriptpath = utils.path_merge(gDirs.base_buildsystem, "scripts", cmd .. ".txt")
            data = utils.file_read(scriptpath)
        end
        if data == nil then print ("Script " .. scriptpath .. " not found.") return nil end
        local lines = utils.str_split(data, "\n")
        for _, v in ipairs(lines) do
            v = utils.str_trim(v)
            if (v ~= "") then
                if dbg then
                    print("# " .. v) -- NDEBUG
                end
                local parse = cli.parse(clistate, v)
                cli.execute(clistate, parse)
            end
        end
    end
end
function cli.register_default(clistate)
    cli.register(clistate, "lua", "~*:command:print('empty lua command')", function(input)
        utils.eval(input.parsed["command"])
    end)
    cli.register(clistate, "exit", "~*:command:null", function(input)
        os.exit(0)
    end)
    cli.register(clistate, "exec", "~*:path:test", function(input)
        cli.cli_execute_file(clistate, false, input)
    end)
    cli.register(clistate, "execd", "~*:path:test", function(input)
        cli.cli_execute_file(clistate, true, input)
    end)
    cli.register(clistate, "#", "~*:command:null", function(input)
        -- comment. no-op.
    end)

    cli.registerAutocomplete(clistate, "exec", cli.cli_execute_file_autocomplete)
    cli.registerAutocomplete(clistate, "execd", cli.cli_execute_file_autocomplete)
end
function cli.execute(state, parseInputArray)
    local parseinput
    for i, parseInput in ipairs(parseInputArray) do
        if parseInput.input then
            log_dbg("Command execution: ", parseInput.cmdString, json.encode(parseInput.input.args))
        end

        if(parseInput.input == nil) then
            return false
        end
        if(parseInput.input.ok ~= true) then
            io.write("Error: " .. parseInput.input.err)
            return false
        end
        if parseInput.cmdHandler.callback(parseInput.input) == false then
            return false
        end
    end
    return true
end
function cli.isUserInput()
    return ext.istty()
end

function cli.input(state, clbs)
    local v = nil
    if cli.isUserInput() then
        if linenoise ~= nil then
            v = cli.interactive_input_linenoise(state, clbs)
        else
            v = cli.interactive_input_manual(state,clbs)
        end
    else
        v = io.read("*l")
    end
    return v
end

-- ** History data
function cli.get_history_filename(state)
    utils.path_mkdir(gDirs.local_config)
    local filename = utils.path_merge(gDirs.local_config, "cli-history.json")
    return filename
end
function cli.save_history(state, file)
    utils.file_write(file, "wb", json.encode(state.history))
end
function cli.load_history(state, file)
    local data=utils.file_read(file)
    if data == nil then return end
    state.history = json.decode(data)
end
function cli.add_history(state, line)
    if line == "exit" or line == "reload" then return end -- special case, don't add "exit" or "reload" command..

    -- write history in array and remove an entry if overflowed
    if state.history[#state.history] ~= line then
        table.insert(state.history, line)
        while #state.history > state.historyMaxSize do table.remove(state.history, 1) end
    end

    -- write history to file
    cli.save_history(state, cli.get_history_filename())
end
function cli.do_load_history_linenoise(state)
    cli.load_history(state, cli.get_history_filename())

    -- rebuild history
    if linenoise ~= nil then
        for _, line in ipairs(state.history) do
            linenoise.historyadd(line)
        end
    end
end

-- ** Input implementations
function cli.interactive_input_linenoise(state, clbs)
    local v = nil
    if(clbs == nil) then clbs = {} end

    local function autocomplete(completion,str)
        if(clbs.autocomplete) then 
           local res = clbs.autocomplete(str)
           table.sort(res)
           for _, v in ipairs(res) do completion:add(v) end
        end
    end

    linenoise.setcompletion(autocomplete)
    local line = linenoise.linenoise(" > ")
    if line ~= nil then
        cli.add_history(state, line)
        linenoise.historyadd(line)
    end
    return line
end

function cli.interactive_input_manual(state, clbs)
    local v = nil
    if(clbs == nil) then clbs = {} end
    -- reset state
    ext.setcolor("reset")

    -- blank output lines and make some spacing
    local lineblank=7
    for i=1,lineblank do
        print("                             "); -- NDEBUG
    end
    local x,y = ext.wherexy()
    y = y - lineblank

    -- write line to tty
    ext.gotoxy(x,y)
    v = ""
    if clbs.write_line then
        clbs.write_line(v)
    else
        io.write(" > " .. v)
    end

    while(true) do
        -- input logic
        local lastchar = ext.getch()

        if lastchar == 0x7f or lastchar == 8 then
            v = v:sub(1, -2)
        elseif lastchar >= 32 and lastchar <= 126 then
            local lastcharstr = string.char(lastchar)
            v = v .. lastcharstr
        elseif lastchar == 10 or lastchar == -1 or lastchar == 13 then
            print("                                       "); -- NDEBUG
            x,y = ext.wherexy()
            for i=1,lineblank-1 do
                print("                                       "); -- NDEBUG
            end
            ext.gotoxy(x,y)
            break
        elseif lastchar == 0 then
            local backslash = ext.getch()
    -- windows f1 to f10
        elseif lastchar == 27 or lastchar == 224 then
            if lastchar == 27 then
                local backslash = ext.getch()
            end
            local code = string.char(ext.getch())
            if code == "A" or code == "H" then
                -- up pressed
            elseif code == "B" or code == "P" then
                -- down pressed
            elseif code == "C" or code == "M" then
                -- right pressed
            elseif code == "D" or code == "K" then
                -- left pressed
            else
                local ncode=ext.getch()
            end
        elseif lastchar == 9 then
            if clbs.tabpressed then
                clbs.tabpressed(v)
            else
                v = v .. "\t"
            end
        else
            print("control character " .. lastchar) -- NDEBUG
        end

        -- output logic
        ext.gotoxy(x,y)
        if clbs.write_line then
            clbs.write_line(v)
        else
            io.write(" > " .. v)
        end
        io.flush()
        local vx,vy = ext.wherexy()
        print("   ") -- NDEBUG
        if clbs.output then 
            clbs.output(v)
        end
        ext.gotoxy(vx,vy)
    end
    if(v == "") then v = nil end
    return v
end

return cli
