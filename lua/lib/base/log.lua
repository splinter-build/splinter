if log_level == nil then
    if debugging == true then
        log_level = 2
    else
        log_level = 1
    end
end

if ext ~= nil and ext.seconds ~= nil then
    log_gettime = function() return ext.seconds() end
else
    log_gettime = function() return os.clock() end
end

if log_basetime == nil then
    log_basetime = log_gettime()
end

local function log_str_repeat(s, n)
    local r = ""
    for i=1,n do
        r = r .. s
    end
    return r
end

log = function (level, levelDesc, ...)   
    local tracedump = false

    -- make tracedump if level = -1, and pretend its level 0
    if level == -1 then 
        level = 0 
        tracedump = true 
    end

    if level <= log_level then 
        local timestr = string.format("%.3f", log_gettime() - log_basetime)
        local tbl = table.pack(...)
        for i=1,#tbl do tbl[i] = tbl[i] or "" end
        io.stderr:write("[" .. levelDesc .. "] ", timestr, log_str_repeat(" ", 10-timestr:len()), table.concat(tbl, "\t"), "\n")
        if(tracedump == true) then
            io.stderr:write(debug.traceback(), "\n")
        end
    end
end

log_dbg = function(...)         log(3, "dbg", ...) end
log_info = function(...)        log(2, "info", ...) end
log_warn = function(...)        log(1, "warn", ...) end
log_err = function(...)         log(-1, "err", ...) end
log_err_notrace = function(...) log(-1, "err", ...) end
log_fatal = function(...)       log(-1, "fatal", ...) error("FATAL ERROR.") end
log_time_reset = function()     log_basetime = log_gettime() end

if _oassert == nil then
_oassert = assert
_oerror = error
end
assert = function(x, y) if(x) then else print(debug.traceback()) _oerror(y or "Assertion failed!") end end -- NDEBUG
error = function(...) print(debug.traceback()) _oerror(...) end -- NDEBUG
