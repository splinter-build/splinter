local utils = {}

function utils.get_os()
    local raw_os_name, raw_arch_name = nil, ''

    -- LuaJIT shortcut
    if jit ~= nil  and jit.os ~= nil  and jit.arch ~= nil  then
        raw_os_name = jit.os
        raw_arch_name = jit.arch
    else
        if raw_os_name == nil then
            -- Windows
            local env_OS = os.getenv('OS')
            local env_ARCH = os.getenv('PROCESSOR_ARCHITECTURE')
            if env_OS and env_ARCH then
                raw_os_name, raw_arch_name = env_OS, env_ARCH
            end
        end
        if raw_os_name == nil then
            -- is popen supported?
            local popen_status, popen_result = pcall(io.popen, "")
            if popen_status then
                popen_result:close()
                -- Unix-based OS
                raw_os_name = io.popen('uname -s','r'):read('*l')
                raw_arch_name = io.popen('uname -m','r'):read('*l')
            end
        end
    end

    raw_os_name = (raw_os_name):lower()
    raw_arch_name = (raw_arch_name):lower()

    local os_patterns = {
        ['windows'] = 'Windows',
        ['linux'] = 'Linux',
        ['mac'] = 'Mac',
        ['darwin'] = 'Mac',
        ['osx'] = 'Mac', 
        ['^mingw'] = 'Windows',
        ['^cygwin'] = 'Windows',
        ['bsd$'] = 'BSD',
        ['SunOS'] = 'Solaris',
    }

    local arch_patterns = {
        ['^x86$'] = 'x86',
        ['i[%d]86'] = 'x86',
        ['amd64'] = 'x64',
        ['x86_64'] = 'x64',
        ['x64'] = 'x64',
        ['Power Macintosh'] = 'powerpc',
        ['arm64'] = 'arm64',
        ['^armv'] = 'arm',
        ['^mips'] = 'mips',
    }

    local arch_bits_patterns = {
        ['^x86$'] = 32,
        ['i[%d]86'] = 32,
        ['amd64'] = 64,
        ['arm64'] = 64,
        ['x86_64'] = 64,
        ['x64'] = 64,
        ['Power Macintosh'] = 32,
        ['^armv'] = 32,
        ['^mips'] = 32,
    }

    local os_name, arch_name, arch_bits = 'unknown', 'unknown', 64

    for pattern, name in pairs(os_patterns) do
        if raw_os_name:match(pattern) then
            os_name = name
            break
        end
    end
    
    for pattern, name in pairs(arch_patterns) do
        if raw_arch_name:match(pattern) then
            arch_name = name
            break
        end
    end
    for pattern, bits in pairs(arch_bits_patterns) do
        if raw_arch_name:match(pattern) then
            arch_bits = bits
            break
        end
    end
    return os_name, arch_name, arch_bits
end

function utils.get_path_seperator(os)
    if os == nil then os = utils.get_os() end
    if(os == "WINDOWS" or os == "Windows" or os == "windows") then
        return "\\"
    else
        return "/"
    end
end

-- OS detection
if utils.os_name == nil then
    utils.os_name, utils.os_arch, utils.os_bits = utils.get_os()
    utils.os_pathsep = utils.get_path_seperator()
end

-- advanced lua control flow
function utils.try(fnc, fncCatch)
    local res = table.pack(pcall(fnc))
    if res[1] == false then
        if fncCatch ~= nil then
            fncCatch(res[2]) -- send error message to catch function
        end
        return nil
    else
        table.remove(res, 1)
        return table.unpack(res)
    end
end
function utils.eval(cmd)
    local func,err = load(cmd, "eval", "t", _ENV)
    if func == nil then 
        print (err) 
    else
        local status, perr = pcall(func)
        if status then
            if(perr ~= nil) then
                print(json.encode(perr)) -- NDEBUG
            end
        else
            print(perr) -- NDEBUG
        end 
    end 
end


function utils.build_env_sandbox()
    local sandbox
    sandbox = {}
    sandbox.table = {}
    sandbox.table.concat = table.concat
    sandbox.table.insert = table.insert
    sandbox.table.remove = table.remove
    sandbox.table.sort = table.sort
    sandbox.table.maxn = table.maxn
    sandbox.next = next
    sandbox.pairs = pairs
    sandbox.ipairs = ipairs
    sandbox.tonumber = tonumber
    sandbox.tostring = tostring
    sandbox.type = type
    sandbox.print = print
    sandbox.assert = assert
    sandbox.error = error
    sandbox.unpack = unpack
    sandbox.select = select
    sandbox.pcall = pcall
    sandbox.string = {}
    sandbox.string.byte = string.byte
    sandbox.string.char = string.char
    sandbox.string.find = string.find
    sandbox.string.format = string.format
    sandbox.string.gmatch = string.gmatch
    sandbox.string.gsub = string.gsub
    sandbox.string.len = string.len
    sandbox.string.lower = string.lower
    sandbox.string.match = string.match
    sandbox.string.rep = string.rep
    sandbox.string.reverse = string.reverse
    sandbox.string.sub = string.sub
    sandbox.string.upper = string.upper
    sandbox._G = sandbox
    sandbox._ENV = sandbox
    sandbox.isSandbox = 1
    sandbox.debug = {}
    sandbox.debug.traceback = function() return debug.traceback() end
    return sandbox
end


function utils.eval_untrusted(untrusted_code, venv, chunkName)
    local untrusted_function, message = load(untrusted_code, chunkName, 't', venv)
    if not untrusted_function then return false, message end
    return pcall(untrusted_function)
end

function utils.eval_untrusted_file(untrusted_file, venv)
    local untrusted_function, message = loadfile(untrusted_file, 't', venv)
    if not untrusted_function then return false, message end
    return pcall(untrusted_function)
end

local system_modules = {
    "fiber",
    "ffi",
    "io",
    "console",
    "digest",
    "json",
    "uri",
    "jit.dis_x64",
    "box.internal.gc",
    "box._lib",
    "crypto",
    "net.box",
    "internal.argparse",
    "log",
    "jit.opt",
    "uuid",
    "fio",
    "pwd",
    "internal.trigger",
    "jit.p",
    "jit.vmdef",
    "os",
    "string",
    "debug",
    "jit.profile",
    "socket",
    "box.internal.sequence",
    "tap",
    "coroutine",
    "net.box.lib",
    "jit.dump",
    "pickle",
    "msgpack",
    "jit.dis_x86",
    "box.backup",
    "jit",
    "jit.v",
    "buffer",
    "box",
    "yaml",
    "xlog",
    "errno",
    "bit",
    "box.internal",
    "jit.zone",
    "table",
    "msgpackffi",
    "jit.util",
    "csv",
    "help.en_US",
    "jit.bcsave",
    "box.internal.session",
    "jit.bc",
    "math",
    "fun",
    "table.new",
    "tarantool",
    "http.client",
    "http.codes",
    "http.lib",
    "http.mime_types",
    "http.server",
    "title",
    "_G",
    "table.clear",
    "help",
    "strict",
    "package",
    "clock",
    "iconv"
}
function utils.modules_unload()
    for module_name, module_table in pairs(package.loaded) do
        if not utils.arr_contains_value(system_modules, module_name) then
            if type(module_table) ~= 'table' then
                module_table = {}
            else
                if module_table.unload ~= nil then
                    pcall(module_table.unload)
                end
            end

            package.loaded[module_name] = nil
        end
    end
end

function utils.get_script_path()
    local str = debug.getinfo(2, "S").source:sub(2)
    return str
end

function utils.get_script_folder()
    local str = debug.getinfo(2, "S").source:sub(2)
    return utils.path_folder(str)
end


function utils.exec(cmd, fnc)    
    local function launch(cmd) 
        if true then 
            -- popen method
            local f = io.popen(cmd)
            local ret = {}
            for line in f:lines() do
                if fnc ~= nil then fnc(line) end
                table.insert(ret, line)
            end
            f:close()
            return ret
        else
            -- write to tempfile method
            local n = utils.tmpname ()
            os.execute (cmd .. " > \"" .. n .. "\"")
            local ret = {}
            for line in io.lines (n) do
                if fnc ~= nil then fnc(line) end
                table.insert(ret, line)
            end
            os.remove (n)
            return ret
        end
    end

    if type(cmd) == "string" then
        return launch(cmd)
    else
        local nret = {}
        for _, v in ipairs(cmd) do
            table.insert(nret, launch(cmd))
        end
        return nret
    end
end
function utils.tmpname()
	-- more stable version of os.tmpname
	if utils.is_windows() then
		local tmpfolder = os.getenv("TEMP") or ""
		return utils.path_merge(tmpfolder, utils.path_filename(os.tmpname()))
	else
		return os.tmpname ()
	end
end
function utils.execmultiline(cmd)
    if utils.is_windows() then
        -- write to tempfile method
        local n = utils.tmpname() .. ".bat"
        utils.file_write(n, "w", "@echo off\n" .. cmd)
        os.execute (n)
        os.remove (n)
        return ret
    else
        -- write to tempfile method
        local n = utils.tmpname() .. ".sh"
        utils.file_write(n, "w", "#!/bin/sh\n" .. cmd)
        os.execute ("chmod a+x " .. utils.shell_escape(n))
        os.execute (n)
        os.remove (n)
        return ret
    end
end
function utils.remove_folder(folder)
    if(folder == nil or utils.str_trim(folder) == "" or utils.str_trim(folder) == "/") then return end
    if utils.is_windows() then
        os.execute("rmdir /s /q " .. utils.shell_escape(folder) .. " > NUL")
    elseif utils.is_unix() then
        os.execute("rm -Rf " .. utils.shell_escape(folder))
    end
end
function utils.move_folder_contents(folder1, folder2)
    if(folder1 == nil or utils.str_trim(folder1) == "" or utils.str_trim(folder1) == "/") then return end
    if(folder2 == nil or utils.str_trim(folder2) == "" or utils.str_trim(folder2) == "/") then return end

    if utils.is_windows() then
        os.execute("robocopy " .. utils.shell_escape(folder1) .. " " .. utils.shell_escape(folder2) .. " /s /mov > NUL")
    elseif utils.is_unix() then
        os.execute("mv -f "  .. folder1 .. "/* " .. utils.shell_escape(folder2) )
    end
end
function utils.is_unix()
    return utils.os_name == "Linux" or utils.os_name == "BSD" or utils.os_name == "Solaris" or utils.os_name == "Mac"
end
function utils.is_linux()
    return utils.os_name == "Linux"
end
function utils.is_windows()
    return utils.os_name == "Windows"
end
function utils.is_darwin()
    return utils.os_name == "Mac"
end
function utils.shell_escape(str)
    str = string.gsub(str,"\\", "\\\\")
    str = string.gsub(str,"\"", "\\\"")
    return "\"" .. str .. "\""
end
function utils.pattern_escape(str)
    str = string.gsub(str, "([%(%)%.%&%%%+%-%*%?%[%]%^%$])", "%%%1")
    return str
end
function utils.file_exists(filename)
    local f = io.open(filename, "rb")
    if f == nil then return false end
    f:close()
    return true
end
function utils.file_read(filename)
    local f = io.open(filename, "rb")
    if f == nil then return nil end
    local ret = f:read("*all")
    f:close()
    return ret
end
function utils.file_write(filename, mode, data)
    local f = io.open(filename, mode)
    if f == nil then return false end
    local ret = f:write(data)
    f:close()
    return true
end
function utils.file_write_if_unequal(filename, data)
    local data_file = utils.file_read(filename)
    if (data_file ~= data) then
        utils.file_write(filename, "wb", data)
        return true
    end
    return false
end
function utils.str_re_ismatch(str, patt)
    if(type(str) ~= "string") then log_err("invalid data") end
    return string.match(str, patt) ~= nil
end
function utils.str_re_matches(str, patt)
    if(type(str) ~= "string") then log_err("invalid data") end
    local res = {}
    local iter = str:gmatch(patt)
    while true do
        local arr = table.pack(iter())
        if #arr == 0 then break end
        table.insert(res, utils.arr_select(arr, function(x) return tostring(x) end))
    end
    return res
end
function utils.str_starts_with(str, start)
    if(type(str) ~= "string") then log_err("invalid data") end
    return string.sub(str, 1, #start) == start
end
function utils.str_skip(str, start)
    if(type(str) ~= "string") then log_err("invalid data") end
    return str:sub( #start + 1)
end
function utils.str_ends_with(str, ending)
    if(type(str) ~= "string") then log_err("invalid data") end
    return ending == "" or str:sub(-#ending) == ending
end
function utils.str_contains(str, val, index)
    if(type(str) ~= "string") then log_err("invalid data") end
    return str:find(val, index or 1, true) ~= nil
end
function utils.str_trim(s)
    if(type(s) ~= "string") then log_err("invalid data") end
    return s:match "^%s*(.-)%s*$"
end
function utils.str_repeat(s, n)
    if(type(s) ~= "string") then log_err("invalid data") end
    if(type(n) ~= "number") then log_err("invalid data") end
    local r = ""
    for i=1,n do
        r = r .. s
    end
    return r
end
function utils.str_padright(x, n, ch)
    local needed = n - tostring(x):len()
    if needed > 0 then
        return x .. utils.str_repeat(ch, needed)
    else 
        return x
    end
end
function utils.str_padleft(x, n, ch)
    local needed = n - tostring(x):len()
    if needed > 0 then
        return utils.str_repeat(ch, needed) .. x 
    else 
        return x
    end
end
function utils.str_random_table(charset)
    local charTable = {}
    for c in charset:gmatch(".") do
        table.insert(charTable, c)
    end
    return charTable
end
function utils.str_random(charTable, length)
    local randomString = ""
    for i = 1, length do
        randomString = randomString .. charTable[math.random(1, #charTable)]
    end
    return randomString
end
function utils.str_split(str,delimiter)
    if(type(str) ~= "string") then log_err("invalid data") end
    local result = { }
    local from  = 1
    local delim_from, delim_to = string.find( str, delimiter, from  )
    while delim_from do
      table.insert( result, string.sub( str, from , delim_from-1 ) )
      from  = delim_to + 1
      delim_from, delim_to = string.find( str, delimiter, from  )
    end
    table.insert( result, string.sub( str, from  ) )
    return result
end

function utils.str_split_pat(str, pat)
    if(type(str) ~= "string") then log_err("invalid data") end
    local t = {}  
    local fpat = "(.-)" .. pat
    local last_end = 1
    local s, e, cap = str:find(fpat, 1)
    while s do
       table.insert(t,cap)
       last_end = e+1
       s, e, cap = str:find(fpat, last_end)
    end
    if last_end <= #str then
       cap = str:sub(last_end)
       table.insert(t, cap)
    end
    return t
 end
function utils.str_argparse(str, sep, qchar, multichar)
    local ret = {}
    if str == nil then return nil end
    if(type(str) ~= "string") then log_err("invalid data") end
    local quotechar= "\""
    local escapechar = "\\"
    if qchar ~= nil then quotechar = qchar end
    local args = {}
    local arg = ""
    local state = -1
    local i=1
    while i <= #str do
        local c = str:sub(i,i)
        if state == -1 then
            -- skip initial whitechar mode
            if c ~= "\t" and c ~= " " then arg = arg .. c; state = 0 end
        elseif state == 0 then
            -- normal mode
            if c == sep then
                table.insert(args, arg)
                arg = ""
            elseif c == quotechar then
                state = 1
            elseif c == multichar then
                -- insert arg, then insert args array into self
                table.insert(args, arg)
                arg = ""
                table.insert(ret, args)
                args = {}
                state = -1 -- return back to skip whitespace mode
            else 
                arg = arg .. c
            end
        elseif state == 1 then
            -- quote mode
            local nchar = str:sub(i+1,i+1)
            if (c == escapechar and nchar  == quotechar) then
                arg = arg .. quotechar
                i = i + 1
            elseif (c == quotechar) then
                state = 0
                --table.insert(args, arg)
                --arg = ""
            else
                arg = arg .. c
            end
        end
        i = i + 1
    end
    if arg ~= "" then
        table.insert(args,arg)
    end
    table.insert(ret, args)
    return ret
end
function utils.str_common_substr(strList)
    local shortest, prefix, first = math.huge, ""
    for _, str in pairs(strList) do
        if str:len() < shortest then shortest = str:len() end
    end
    for strPos = 1, shortest do
        if strList[1] then
            first = strList[1]:sub(strPos, strPos)
        else
            return prefix
        end
        for listPos = 2, #strList do
            if strList[listPos]:sub(strPos, strPos) ~= first then
                return prefix
            end
        end
        prefix = prefix .. first
    end
    return prefix
end
function utils.slice(tbl, first, last, step)
    local sliced = {}
    for i = (first or 1), (last or #tbl), (step or 1) do sliced[#sliced+1] = tbl[i] end
    return sliced
end
function utils.fromhex(str)
    if(type(str) ~= "string") then log_err("invalid data") end
    return (str:gsub('..', function (cc)
        return string.char(tonumber(cc, 16))
    end))
end

function utils.tohex(str)
    if(type(str) ~= "string") then log_err("invalid data") end
    return (str:gsub('.', function (c)
        return string.format('%02X', string.byte(c))
    end))
end

function utils.escapePattern(str)
    if(type(str) ~= "string") then log_err("invalid data") end
    return str:gsub("[%(%)%.%%%+%-%*%?%[%^%$%]]", "%%%1")
end

function utils.str_replace(str, search, replace)
    if(type(str) ~= "string") then log_err("invalid data") end
    local vreplace =  replace:gsub("%%", "%%%%")
    return str:gsub(utils.escapePattern(search), vreplace)
end

function utils.path_exists(x)
    if lfs.attributes(x) == nil then return false end
    return true
end

function utils.path_split(x)
    return utils.str_split_pat(x, "[\\/]")
end

function utils.path_normalize(x, sep) 
    local v =""

    if sep == nil then sep = utils.os_pathsep end
    if(sep == "\\") then
        v = table.pack(string.gsub(x, "/", sep))[1]
    else
        v = table.pack(string.gsub(x, "\\", sep))[1]
    end
    
    v = v:gsub(sep .. sep, sep)

    if v ~= sep then 
        while(utils.str_ends_with(v, sep) == true)  do
            v = v:sub(1, #v-1)
        end
    end

    if string.find(v, sep .. "..", 1, true) then
        local arr = utils.str_split(v, sep)

        -- remove .. occurrences that are unnecessary
        local pos = 0
        while true do
            -- increment and dont go too far
            pos = pos + 1
            if(pos + 1 > #arr) then break end

            -- check for superfluous ..
            if(arr[pos] ~= ".." and arr[pos+1] == "..") then
                table.remove(arr,pos)
                table.remove(arr,pos)
                pos = pos - 1
            end
        end
        v = table.concat(arr, sep)
    end

    return v
end
function utils.path_merge(...)
    local assemble = ""
    local f = table.pack(...)
    for i,v in ipairs(f) do
        if v == "." then
            -- do nothing.
        elseif(utils.path_is_abs(v)) then 
            local nassemble = utils.path_normalize(v)
            if utils.is_windows() and nassemble:sub(1,1) == "\\" then
                -- find drive string
                local drivestr = ""
                if(assemble:sub(2,2) == ":") then
                    drivestr = assemble:sub(1,3)
                else
                    drivestr = utils.path_cwd():sub(1,3)
                end

                assemble = utils.path_merge(drivestr, nassemble:sub(2))
            else
                assemble = nassemble
            end
            
        else
            if i == 1 then
                assemble = utils.path_normalize(v)
            else
                assemble = utils.path_normalize(assemble .. utils.os_pathsep .. v)
            end
        end
    end

    local v = utils.path_normalize(assemble)
    if utils.is_windows() and v:len() == 2 and v:sub(2,2) == ":" then
        v = v .. utils.os_pathsep
    end
    return v
end

function lfs_scanfolder(tbl, dir, mode, recur, dirrel, depth)
    if lfs.attributes(dir) == nil then return end
    if depth <= 0 then return end

    local it, itobj = lfs.dir(dir)

    while true do
        local file = itobj:next()
        if file == nil then break end
        if file ~= "." and file ~= ".." then
            --local attr = nil
            local attr = lfs.attributes(utils.path_merge(dir, file)).mode

            if(attr == "directory") then
                -- this is a directory
                if(mode == 1) then
                    table.insert(tbl, dirrel .. file)
                end
                if(recur) then
                    local newdirrel = nil
                    if dirrel == "" then 
                        newdirrel = file .. "/"
                    else
                        newdirrel = dirrel .. file .. "/"
                    end
                    lfs_scanfolder(tbl, utils.path_merge(dir, file), mode, recur,newdirrel, depth - 1)
                end
            else
                if(mode == 0) then
                    table.insert(tbl, dirrel .. file)
                end
            end
        end
    end
end

function utils.path_listdir(dir, mode, recur, depthLimit)
    
    dir = utils.path_abs(dir)
    
    -- lfs support
    if(lfs ~= nil and lfs.dir ~= nil) then
        if depthLimit == nil then depthLimit = 128 end
        local tbl = {}
        lfs_scanfolder(tbl, dir, mode, recur, "", depthLimit)
        return tbl
    end

    -- native support
    local tbl = {}
    if(utils.is_windows()) then
        local vmode = "/ad"
        if mode == 0 then vmode = "/a:-d" end -- files
        if recur then vmode = vmode .. " /s" end
        local cmd = 'dir "' .. dir .. '" /b ' .. vmode .. " 2> NUL"
        log_dbg("Listing " .. dir .. " with cmd: " .. cmd)
        local opened = io.popen(cmd)
        local data = opened:read('*all')
        local dataSplit = utils.str_split(data, "\n")
        for i = 1, #dataSplit do
            local entry = dataSplit[i]
            if recur then 
                table.insert(tbl, utils.path_rel(dir, entry)) 
            else
                table.insert(tbl, entry) 
            end
        end
    else
        local vrecur = "-maxdepth 0 "
        local vmode = "-type d "
        if recur then vrecur = "" end
        if mode == 0 then vmode = "-type f " end
        for entry in io.popen('find "' .. dir .. '" ' .. vrecur .. vmode):lines() do
            table.insert(tbl, entry) 
       end
    end
    return tbl
end
function utils.path_is_abs(P)
    if type(P) ~= "string" then log_err("invalid string") end
    if P == nil or P:len() < 1 then return false end
    if(utils.is_windows()) then
        return P:sub(1,1) == '/' or P:sub(1,1) =='\\' or P:sub(2,2)==':'
    else
        return P:sub(1,1) == '/'
    end
end
function utils.path_cwd()
    if lfs ~= nil and lfs.currentdir ~= nil then
        return lfs.currentdir()
    end
    if(utils.is_windows()) then
        current_dir=utils.exec("echo %CD%")[1]
    else
        current_dir=utils.exec("cd")[1]
    end
    return current_dir
end
function utils.path_modtime(P)
    if lfs ~= nil and lfs.attributes ~= nil then
        local attr= lfs.attributes(P)
        if attr then return attr.modification end
        return nil
    else
        error("NO IMPLEMENTATION FOUND FOR utils.path_modtime")
    end
end
function utils.path_mkdir(P)
    if lfs ~= nil and lfs.mkdir ~= nil then
        lfs.mkdir(P)
    else
        error("NO IMPLEMENTATION FOUND FOR utils.path_mkdir")
    end
end
function utils.path_mkdir_recur(P)
    local absP = utils.path_abs(P)
    local Psplit = utils.path_split(absP)
    if(utils.is_windows()) then
        local combine = Psplit[1] 
        for i=2,#Psplit do
            combine = combine .. "\\" .. Psplit[i] 
            utils.path_mkdir(combine)
        end
    else
        local combine = ""
        for i=1,#Psplit do
            combine = combine .. "/" .. Psplit[i] 
            utils.path_mkdir(combine)
        end
    end
end
function utils.path_chdir(P)
    if lfs ~= nil and lfs.chdir ~= nil then
        lfs.chdir(P)
    else
        error("NO IMPLEMENTATION FOUND FOR utils.path_chdir")
    end
end
function utils.path_abs(P)
    if P == nil or P:len() < 1 then return utils.path_cwd() end
    return utils.path_merge(utils.path_cwd(), P)
end
function utils.path_rel(x, y)
    local absx = utils.path_abs(x)
    local absy = utils.path_abs(y)
    
    if(absy:sub(1, absx:len() + 1) == (absx .. utils.os_pathsep)) then
        return absy:sub(absx:len()+2)
    else
        if(utils.is_windows()) then
            if absx:sub(1,2):lower() ~= absy:sub(1,2):lower() then
                return absy
            end
        end

        -- find common folders
        local abssplitx = utils.path_split(absx)
        local abssplity = utils.path_split(absy)  
        local commonlen = 0
        local abssplitlen = math.min(#abssplitx, #abssplity)
        local i
        for i = 1,abssplitlen do
            if abssplitx[i]:lower() ~= abssplity[i]:lower() then break end
            commonlen = commonlen + abssplitx[i]:len() + 1
        end

        -- write relative path
        local pathback = absx:sub(commonlen+1)
        local pathforward = absy:sub(commonlen+1)
        if(pathback == "") then 
            return pathforward
        else
            pathback = utils.str_repeat(".." .. utils.os_pathsep, utils.str_count_occurrences(pathback, {"/", "\\"})+1)
            pathback = pathback:sub(1, pathback:len()-1)
            return utils.path_normalize(pathback .. utils.os_pathsep .. pathforward)
        end       
    end
end


-- get filename
function utils.path_folder(url)
    url = url or ""
    local dt = url:match("^(.*)[\\/].+$")
    if dt == nil then return "" end
    return dt
  end
function utils.path_filename(url)
  local dt = url:match("^.*[\\/](.+)$")
  if dt == nil then return url end
  return dt
end
function utils.path_filename_noext(url)
    return utils.path_noext(utils.path_filename(url))
end
function utils.path_noext(url)
    local ext = utils.path_fileext(url)
    if ext ~= nil and ext ~= "" then return url:sub(1, url:len()-ext:len()) end
    return url
end

-- get file extension
function utils.path_fileext(url)
    local noext = utils.path_filename(url)
	return noext:match("^.+(%..+)$")
end

function utils.table_is_numeric(array)
    for k, _ in pairs(array) do
        if type(k) ~= "number" then
            return false
        end
    end
    return true --Found nothing but numbers !
end

-- Save copied tables in `copies`, indexed by original table.
function utils.deep_copy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[utils.deep_copy(orig_key)] = utils.deep_copy(orig_value)
        end
        setmetatable(copy, utils.deep_copy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function utils.arr_clone(org)
    return {table.unpack(org)}
end
function utils.arr_append(v1, v2)
    local i,j
    if type(v2) == "table" then 
        for i,j in ipairs(v2) do
            table.insert(v1, j)
        end
    else
        table.insert(v1, v2)
    end
    return v1
end

function utils.arr_reverse(v)
    local rv = {}
    local cnt = #v
    for i=cnt,1,-1 do
        table.insert(rv, v[i])
    end
    return rv
end

function utils.arr_flatten(org)
    local i,j, _i, _j
    local hasArray = false
    if org == nil then return {} end
    for i,j in ipairs(org) do
        if type(j) == "table" then hasArray = true break end
    end

    if hasArray then 
        local ret = {}
        for i, j in ipairs(org) do 
            if type(j) == "table" then
                local flattened = utils.arr_flatten(j)
                for _i, _j in ipairs(flattened) do table.insert(ret, _j) end
            else
                table.insert(ret, j)
            end
        end
        return ret
    else
        return org
    end
end

function utils.table_merge(t1, t2)
    if(type(t1) ~= "table") then log_err("invalid data: t1") return end
    if(type(t2) ~= "table") then log_err("invalid data: t2") return end
    -- key based merge
    for k,v2 in pairs(t2) do
        if type(v2) == "table" then
            local v1 = t1[k]
            if type(v1) == "table" then
                if #v2 > 0 then
                    -- assigned value is a table with array count..
                    local _, _v
                    for _, _v in ipairs(v2) do table.insert(v1, _v) end  -- otherwise, add elements to back of the table
                else
                    -- assigned value is a hash key table.. recurse..
                    utils.table_merge(v1, v2)
                end
            else
                t1[k] = v2
            end
        else
            t1[k] = v2
        end
    end
    return t1
end


function utils.table_merge_override_array(t1, t2)
    if(type(t1) ~= "table") then log_err("invalid data: t1") return end
    if(type(t2) ~= "table") then log_err("invalid data: t2") return end
    -- key based merge
    for k,v2 in pairs(t2) do
        if type(v2) == "table" then
            local v1 = t1[k]
            if type(v1) == "table" then
                if #v2 > 0 then
                    -- assigned value is a table with array count..
                    local _, _v
                    t1[k] = {} -- reset table
                    v1 = t1[k]
                    for _, _v in ipairs(v2) do table.insert(v1, _v) end  -- otherwise, add elements to back of the table
                else
                    -- assigned value is a hash key table.. recurse..
                    utils.table_merge_override_array(v1, v2)
                end
            else
                t1[k] = v2
            end
        else
            t1[k] = v2
        end
    end
    return t1
end

function utils.table_toarr_key(t)
    local ret = {}
    for v,_ in pairs(t) do table.insert(ret, v) end
    return ret
end

function utils.table_toarr_value(t)
    local ret = {}
    for _,v in pairs(t) do table.insert(ret, v) end
    return ret
end

function utils.table_toarr_kv(t)
    local ret = {}
    for k,v in pairs(t) do table.insert(ret, {k,v}) end
    return ret
end

function utils.table_toarr_kv_sorted(t)
    local ret = {}
    for k,v in pairs(t) do table.insert(ret, {k,v}) end
    table.sort(ret, function(a,b) return a[1] < b[1] end)
    return ret
end

function utils.table_toarr_select(tbl, fnc)
    local narr = {}
    for k,v in pairs(tbl) do
        local ret = fnc(k,v)
        if(ret ~= nil) then
            table.insert(narr, ret)
        end
    end
    return narr
end
function utils.arr_explode(arr, fnc)
    local ret = {}
    if arr == nil or #arr == 0 then return nil end
    
    local vars = table.pack(fnc(1, arr[1]))
    for i = 1, #vars do
        table.insert(ret, {vars[i]})
    end

    for i = 2, #arr do
        local vars = table.pack(fnc(i, arr[i]))
        for j = 1, #vars do
            table.insert(ret[j], vars[j])
        end
    end
    return ret
end
function utils.arr_mutate(arr, fnc)
    for k, v in ipairs(arr) do
        local val =  fnc(v)
        if val ~= nil then
            arr[k] = val;
        end
    end
    return arr
end
function utils.arr_select(arr, fnc)
    local narr = {}
    assert(arr, "invalid array")
    for k, v in ipairs(arr) do
        local val = table.pack(fnc(v))
        if #val > 0 then
            for _, v in ipairs(val) do
                table.insert(narr, v)
            end
        end
    end
    return narr
end
function utils.arr_unique(arr)
    local i=0
    local cnt=#arr
    local tbl = {}
    for i=1, cnt do
        tbl[arr[i]] = 1
    end
    local retArr = {}
    for k,v in pairs(tbl) do
        table.insert(retArr, k)
    end
    return retArr
end
function utils.arr_which(arr, fnc)
    
    local narr = {}
    assert(arr, "invalid array")
    for k,v in ipairs(arr) do
        if(fnc(v) == true) then
            table.insert(narr, v)
        end
    end
    return narr
end
function utils.arr_contains_value(arr, val)
    if (arr == nil) then return false end
    if (val == nil) then return true end
    for k,v in pairs(arr) do
        if(v == val) then
            return true
        end
    end
    return false
end
function utils.arr_merge(...)
    local narr = {}
    local args = table.pack(...)
    if #args == 1 then return args[1] end

    for _, v in ipairs(args) do
        for _, v2 in ipairs(v) do
            table.insert(narr, v2)
        end
    end
    return narr
end
function utils.arr_to_table(arr, fnc)
    local ntbl = {}
    for k,v in ipairs(arr) do
        local nk,nv=fnc(v)
        if(nk ~= nil) then
            ntbl[nk] = nv
        end
    end
    return ntbl
end
function utils.arr_to_table_true(arr)
    local ntbl = {}
    for k,v in ipairs(arr) do
        ntbl[v] = true
    end
    return ntbl
end
function utils.str_template_replace(str, kvs )
    if(kvs == nil) then return str end
    for k,v in pairs(kvs) do
        str = str:gsub("$" .. k, v)
    end
    return str
end

function utils.str_count_occurrences(str, occ)
    local cnt = 0
    for c in string.gmatch(str, ".") do
        for i,j in pairs(occ) do
            if j == c then cnt = cnt + 1 end
        end
    end
    return cnt
end

function utils.urlencode(url)
    local char_to_hex = function(c)
        return string.format("%%%02X", string.byte(c))
    end
    if url == nil then
      return
    end
    url = url:gsub("\n", "\r\n")
    url = url:gsub("([^%w ])", char_to_hex)
    url = url:gsub(" ", "+")
    return url
end
  
  
function utils.urldecode(url)
    local hex_to_char = function(x)
        return string.char(tonumber(x, 16))
    end

    if url == nil then
        return
    end
    url = url:gsub("+", " ")
    url = url:gsub("%%(%x%x)", hex_to_char)
    return url
end

function utils.hexdump(bytes)
    local nstr = ""
    for b in string.gmatch(bytes, ".") do
        nstr = nstr .. string.format("%02X ", string.byte(b))
    end
    nstr = nstr .. string.rep("   ", 10 - string.len(bytes) + 1)
    nstr = nstr .. string.gsub(bytes, "%c", ".") ..  "\n"
    return nstr
end

function utils.iif(expr, onTrue, onFalse)
    if expr then return onTrue else return onFalse end
end

-- CLI table pretty printer
function utils.pretty_print(mode, headers, data)
    if(mode == "json") then
        print(json.encode({headers=headers, data=data})) -- NDEBUG
    else
        local hdt = {}
        local padding = 2
        local padchar = ' '

        -- build headers and measure strings
        for i, v in ipairs(headers) do
            local header = {name=v, width=v:len(), align="left"}
            if(utils.str_starts_with(header.name, "~")) then 
                header.align="right"  
                header.name = header.name:sub(2)
            end

            for di, dv in ipairs(data) do
                local width = tostring(dv[i]):len()
                if(width > header.width) then header.width = width end
            end
            table.insert(hdt, header)
        end

        -- write header to console
        for i, header in ipairs(hdt) do
            if header.align == "left" then
                io.write(utils.str_padright(header.name, header.width + padding, padchar))
            elseif header.align == "right" then
                io.write(utils.str_padleft(header.name, header.width , padchar) .. utils.str_repeat(padchar, 2))
            end
        end
        print("") -- NDEBUG

        -- write dashes
        local dashme = function()
            for i, header in ipairs(hdt) do
                io.write(utils.str_padright("", header.width + padding, '='))
            end
            print("") -- NDEBUG
        end
        dashme()

        -- write data to console
        for i = 1, #data do
            local row = data[i]
            for j = 1, #row do
                local header = hdt[j]

                if header.align == "left" then
                    io.write(utils.str_padright(tostring(row[j]), header.width + padding, padchar))
                elseif header.align == "right" then
                    io.write(utils.str_padleft(tostring(row[j]), header.width , padchar) .. utils.str_repeat(padchar, 2))
                end
            end
            print("") -- NDEBUG
        end

        dashme()
    end
end

-- Job system
gJobList = {}
gJobsActiveMax = 8

function utils.jobSetMax(maxCount)
    gJobsActiveMax = maxCount
end

function utils.jobAdd(code, ...)
    -- mirror package path and cpath so that require will work correctly
    code = "package.path = " .. utils.shell_escape(package.path) .. "\n" .. "package.cpath = " .. utils.shell_escape(package.cpath) .. "\n" .. code
    
    local thread = llthreads.new(code, ...)
    gJobList[thread] = thread
    utils.jobUpdate()
end
function utils.jobUpdate()
    -- count alive
    local count = 0
    local deadjobs = {}
    for i, j in pairs(gJobList) do
        if j:started() then 
            if j:alive() then 
                count = count + 1 
            else
                if j:joinable() then
                    table.insert(deadjobs, j)
                end
            end
        end
    end

    -- remove dead jobs
    for i = 1, #deadjobs do
        deadjobs[i]:join()
        gJobList[deadjobs[i]] = nil
    end

    -- start jobs
    if count < gJobsActiveMax then
        for i, j in pairs(gJobList) do
            if j:started() == false then 
                j:start()  -- start detached
                count = count + 1
                if count >= gJobsActiveMax then break end
            end
        end
    end
   
    -- return number of alive/started threads
    return count
end
function utils.jobWaitForAll()
    while utils.jobUpdate() > 0 do
        ext.sleep(10) -- wait for 10 msecs
    end
end
function utils.jobWaitIfFull()
    while utils.jobUpdate() == gJobsActiveMax do
        ext.sleep(10) -- wait for 10 msecs
    end
end

-- XML
function utils.xml(name, props, children)
    return {xmlnode=true, name=name, props=props, children=children}
end
function utils.xmlgen(node, outtbl, level)
    if not node.xmlnode then return "" end
    if level == nil then level = 0 end
    local data = utils.str_repeat(" ", level) .. "<" .. node.name
    if(#node.props > 0) then
        for i=1,#node.props,2 do
            data = data .. " " .. node.props[i] .. "=\"" .. node.props[i+1] .. "\""
        end
    end
    if node.children == nil then
        data = data .. " />"
    else
        data = data .. ">"

        if(type(node.children) == "string") then
            data = data .. node.children
        else
            -- recurse
            data = data .. "\r\n"

            -- flush data to tbl
            table.insert(outtbl, data)
            data = ""

            for _, child in ipairs(node.children) do
                if(child.xmlnode) then
                     utils.xmlgen(child, outtbl, level + 1 )
                     table.insert(outtbl, "\r\n")
                else
                    for _, cchild in ipairs(child) do
                        utils.xmlgen(cchild, outtbl, level + 1) 
                        table.insert(outtbl, "\r\n")
                    end
                end
            end
            data = data .. utils.str_repeat(" ", level)
        end
    
        data = data .. "</" .. node.name .. ">"
    end

    -- flush data to tbl
    table.insert(outtbl, data)
    data = ""
end

return utils
