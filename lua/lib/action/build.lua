local action = {}
action.v_state = {}

function action.cli_register(clistate)
    local action_autocomplete = function(parsed)
        local tbl = {}
        local totalTbl = modules.getModuleLocations()
        local prefix = parsed.cmdString .. " "
        
        local argLower = ""
        if parsed.cmdArgs and parsed.cmdArgs[1] then argLower = parsed.cmdArgs[1]:lower() end

        for k,v in pairs(totalTbl) do
            if utils.str_starts_with(k:lower(), (argLower)) then
                table.insert(tbl, prefix .. k .. " ")
            end
        end
        table.sort(tbl)
        
        return tbl
    end

    cli.register(clistate, "build", "~*:args:all", function(input) action.exec(input, "build", true) end)
    cli.register(clistate, "buildExit", "~*:null:null", function(input) 
        if(action.getLastBuildStatus() ~= 0) then 
            log_info("Build failure detected, error code: " .. action.getLastBuildStatus())
            os.exit(action.getLastBuildStatus()) 
        end
    end)
    cli.registerAutocomplete(clistate, "build", action_autocomplete)
end

function action.loadAction(state, actionPath)
    if state.actions == nil then state.actions = {} end
    local vaction=state.actions[actionPath]

    if vaction == nil then
        -- load action
        log_dbg("Loading action", actionPath)
        local loader, loaderError=loadfile(actionPath)
        local success, result, result2 = pcall(loader)
        if success == false then
            log_info("Action failure", actionPath, loaderError, result)
            state.actions[actionPath] = {}
            return {}
        else
            state.actions[actionPath] = result
            if state.actionOrder == nil then state.actionOrder = {} end
            table.insert(state.actionOrder, actionPath)
            if result.init then
                result.init(state)
            end
            return result
        end
    end
    return vaction
end

function action.unloadAll(state)
    if state.actionOrder == nil then return end
    for _,actionPath in ipairs(state.actionOrder) do
        local action =  state.actions[actionPath]
        if action.unload then
            action.unload(state)
        end
    end
end

function action.hasAction(state, module)
    local mod = modules.getLoadedModule(module, true)
    if mod == nil then return false end
    if(mod.action == nil) then return false end
    return true
end


function action.doAction(state, module, mode)
    if state.blacklist[module] then return end
    local mod = modules.getLoadedModule(module, true)
    if mod == nil then return false end
    if(mod.action == nil) then return false end

    state.module_action = mod

    local actionPath = utils.path_merge(mod.path, mod.action)
    local action = action.loadAction(state, actionPath)
    
    -- actually execute the action
    if(action.execute ~= nil) then
        action.execute(state, mode)
    end
end

function action.doExec(state, module, recurse)
    local mod = modules.getLoadedModule(module, true)
    if mod == nil then return end

    -- create final modlist including append list
    local modlist
    if recurse then 
        modlist = utils.deep_copy(mod.meta.p_requires)
        utils.arr_append(modlist, state.appendlist)
    else
        modlist = {}
        utils.arr_append(modlist, state.appendlist)
    end

    -- build waves
    local i = 1
    for _,modname in pairs(modlist) do
        if action.hasAction(state, modname) then
            if state.executewave[i] == nil then state.executewave[i] = {} end
            table.insert(state.executewave[i], { mod, modname })
            i = i + 1
        end
    end
end

function action.executeWaves(state, mode)
    -- start executing all modules in the proper order
    state.ro_modules = modules.getLoadedModules()
    local waveID, wave, waveAction
    for waveID, wave in ipairs(state.executewave) do
        for _, waveAction in ipairs(wave) do
            state.module = waveAction[1] -- set module name
            action.doAction(state, waveAction[2], mode)
        end
    end
end

function action.buildGraph(graph, module, cache, recurse)
    if cache == nil then cache = {} end
    local mod = modules.getLoadedModule(module, true)
    if mod == nil then return end

    -- recurse
    if recurse then 
        for _,modname in pairs(mod.meta.p_requires) do
            action.buildGraph(graph, modname, cache, recurse)
        end
    end

    -- insert module into graph
    if(cache[module] == nil) then
        table.insert(graph, module)
        cache[module] = 1
    end
end

function action.setLastBuildStatus(code)
    action.lastBuildStatus = code
end

function action.getLastBuildStatus()
    return action.lastBuildStatus or 0
end

function action.exec(args, mode, recurse)
    local moduleList = modules.expandWildcards(args.args)
    local state = { blacklist = {}, appendlist = {} }

    -- load blacklist
    state.blacklist = utils.arr_to_table_true(utils.str_split(settings.getSetting("action.blacklist", ""), " "))
    state.appendlist = utils.str_split(settings.getSetting("action.append", ""), " ")

    modules.unloadLoaded()
    scripts.reset()
    modules.load(moduleList, false, recurse)
    modules.load(state.appendlist, false, recurse)

    -- build modules list
    local graph = {}
    local cache = {}
    for i,module in ipairs(moduleList) do
        action.buildGraph(graph, module, cache, recurse)
    end

    -- build waves
    state.executewave = {}
    for i,j in ipairs(graph) do 
        action.doExec(state, j, recurse) 
    end
    action.executeWaves(state, mode)

    action.unloadAll(state)
end

return action