local action = {}
action.v_state = {}

-- run expects:
-- module.execute = {
--     filename = "^bin/{name}}",
--     path = "^bin",
--     args = ""    
-- }

function action.cli_register(clistate)
    local action_autocomplete = function(parsed)
        local tbl = {}
        local totalTbl = modules.getModuleLocations()
        local prefix = parsed.cmdString .. " "
        
        local argLower = ""
        if parsed.cmdArgs and parsed.cmdArgs[1] then argLower = parsed.cmdArgs[1]:lower() end

        for k,v in pairs(totalTbl) do
            if utils.str_starts_with(k:lower(), (argLower)) then
                table.insert(tbl, prefix .. k .. " ")
            end
        end
        table.sort(tbl)
        
        return tbl
    end

    cli.register(clistate, "run", "s:name|~*:args", function(input) action.exec(input, "run") end)
    cli.register(clistate, "buildRun", "s:name|~*:args", 
    function(input) 
        actions.build.exec({args={input.parsed.name}}, "build", true) -- external action, see build.lua
        if actions.build.getLastBuildStatus() == 0 then
            action.exec(input, "run") 
        else
            log_info("Skipping run. Build returned error code: " .. actions.build.getLastBuildStatus())
        end
    end)
    cli.registerAutocomplete(clistate, "run", action_autocomplete)
    cli.registerAutocomplete(clistate, "buildRun", action_autocomplete)
end

-- get formatted path. prefixing with ^ means build path. no prefix means relative to module directory.
function action.getPath(mod, src)
    src = src or ""
    local firstChar = src:sub(1,1)
    if firstChar == "^" then
        return utils.path_merge(scripts.get_build_dir(), src:sub(2))
    else
        return utils.path_merge(mod.path, src)
    end
end

function action.normalExecute(state, module, mode, args, mod)
    local cwd = utils.path_cwd()
    utils.path_chdir(mod.execute.path_processed)
    local totalcmd = ""
    if(mod.execute.args ~= "") then
        totalcmd = mod.execute.filename_processed .. " " .. mod.execute.args
    else
        totalcmd = mod.execute.filename_processed
    end

    for _, arg in ipairs(args) do
        totalcmd = totalcmd .. " \"" .. utils.str_replace(arg, "\"", "\\\"") .. "\""
    end

    -- insert extra arguments
    log_info("Working directory: " .. mod.execute.path_processed)
    log_info("Command: " .. totalcmd)
    os.execute(totalcmd)
    utils.path_chdir(cwd)
end

function action.doExecute(state, module, mode, args)
    local mod = modules.getLoadedModule(module, true)
    if mod == nil then return false end

    if mod.execute == nil then 
        log_warn("Module " .. module .. " has no execute information. Cannot run.")
        return
    end

    -- build execution blob
    mod.execute = mod.execute or {}
    mod.execute.filename = mod.execute.filename or ("^bin/" ..  settings.sanitize_build_output_filename(mod.name))
    mod.execute.filename_processed = action.getPath(mod, mod.execute.filename)
    mod.execute.path = mod.execute.path or ("^bin")
    mod.execute.path_processed = action.getPath(mod, mod.execute.path)
    mod.execute.args = mod.execute.args or ""

    if action.isEmscripten(state) then
        action.emscriptenExecute(state, module, mode, args, mod)
    else
        action.normalExecute(state, module, mode, args, mod)
    end
end


function action.exec(args, mode)
    local moduleList = modules.expandWildcards({args.parsed.name})
    local state = { blacklist = {}, appendlist = {} }

    -- load modules (add recursion when emscripten is enabled to detect compiler)
    modules.unloadLoaded()
    scripts.reset()
    modules.load(moduleList, false, action.needRecursion(state))

    -- build modules list
    local graph = {}
    local cache = {}
    for i,module in ipairs(moduleList) do
        action.doExecute(state, module, mode, args.parsedArray.args) 
    end
end

-- Emscripten support
function action.isEmscripten(state)
    return settings.getSetting("build.env.target") == "emscripten"
end

action.needRecursion = action.isEmscripten

function action.detectCppCompiler(state, module)
    state.cpp = {}
    state.cpp.compilers = {}
    state.cpp.generate_build = false
    state.module = module
    return action.cpp_get_compiler_cached(state)
end

function action.emscriptenExecute(state, module, mode, args, mod)
    -- detect cpp compiler
    local cppCompiler = action.detectCppCompiler(state, mod)
    local cppCompilerScripts = cppCompiler.lc_scripts

    -- settings
    local totalcmd = ""
    local detached = false

    -- host os
    local host = utils.os_name:lower()

    if detached and host == "windows" then 
        totalcmd = "start "
    end

    totalcmd = totalcmd .. cppCompilerScripts.exec .. " emrun " .. mod.execute.filename_processed .. ".html"

    for _, arg in ipairs(args) do
        totalcmd = totalcmd .. " \"" .. utils.str_replace(arg, "\"", "\\\"") .. "\""
    end

    if detached and host == "linux" then 
        totalcmd = totalcmd .. " &"
    end

    local cwd = utils.path_cwd()
    utils.path_chdir(scripts.get_build_dir())
    log_info("Working directory: " .. scripts.get_build_dir())
    log_info("Command: " .. totalcmd)
    os.execute(totalcmd)
    utils.path_chdir(cwd)
end

-- Load dependencies
g_action = action
    dofile(utils.path_merge(utils.get_script_folder(), "../../../system_actions/cpp_detect_compiler.lua"))
g_action = nil

return action