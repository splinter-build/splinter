-- extend package path
if gDirs == nil then gDirs = {}
    gDirs.script = arg[0]:gsub("\\", "/"):match("(.*/)")
    gDirs.lua = arg[0]:gsub("\\", "/"):match("(.*/).*/")
    package.path = '' .. gDirs.lua .. '/lib/?.lua;' .. package.path
end

-- load c libraries
ext = require('ext')
lfs = require('lfs')
linenoise = require('linenoise')
llthreads = require('llthreads')

-- load lua libraries
require('base/log')

json = require('base/json')
utils = require('base/utils')
md5 = require('base/md5')
yaml = require('base/yaml')
cli = require('base/cli')

random = require('base/random')
fscache = require('data/fscache')
scripts = require('data/scripts')
sources = require('data/sources')
compilers = require('data/compilers')
modules = require('data/modules')
settings = require('data/settings')
actions = {}
actions.build = require('action/build')
actions.run = require('action/run')

local outputs = {}

-- directory management
if gDirs.user == nil then gDirs.user = utils.path_cwd() end
gDirs.lua = utils.path_abs(gDirs.lua)
gDirs.script = utils.path_abs(gDirs.script)
gDirs.base = utils.path_merge(gDirs.script, "..", "..")
gDirs.base_buildsystem = gDirs.base
gDirs.local_config = utils.path_merge(gDirs.user, ".config")
gDirs.local_build = utils.path_merge(gDirs.user, ".build")
gDirs.local_modules = gDirs.user
gDirs.global_modules = utils.path_merge(gDirs.base_buildsystem, "modules")
gDirs.system_config = utils.path_merge(gDirs.base, ".config")
gDirs.system_modules = utils.path_merge(gDirs.base, "system_modules")

-- load configuration & settings
settings.loadSettings( gDirs.system_config, gDirs.local_config )

-- hook log level
settings.hookSettingChanges(function(clobbered)
    if(clobbered == "*" or clobbered == "log.level") then
        log_level = settings.getSetting("log.level", 2)
    end
end)

-- show banner on interactive mode
if(cli.isUserInput()) then
    --ext.clrscr()
    print (" *** Splinter CLI -----------------------")
end

-- load scripts
cache = {}
function cache.setDirty()
    cache.dirty = true
end
function cache.reload(silent)
    if silent ~= true then log_dbg("Scripts: Building FS cache..") end
    fscache.addCache(gDirs.system_modules)
    fscache.addCache(gDirs.global_modules)
    fscache.addCache(gDirs.local_modules)
    if silent ~= true then log_dbg("Scripts: Detecting scripts in FS cache..") end
    scripts.detect({gDirs.system_modules, gDirs.global_modules, gDirs.local_modules})
    if silent ~= true then log_dbg("Scripts: Updated FS cache..") end
    cache.dirty = false
end
function cache.update(silent)
    if cache.dirty then cache.reload(silent) end
end
cache.setDirty()

-- create cli state
local clistate = cli.init()
cli.register_default(clistate)

-- register reload & some other commands
cli.register(clistate, "reload", "~*:null:null", function(input) 
    g_reload = true
    g_running = false
 end)
cli.register(clistate, "listDirs", "~*:null:null", function(input)
    local data = utils.table_toarr_select(gDirs, function(k, v) return {k, v} end)
    table.sort(data, function(a,b) return a[1] < b[1] end)
    utils.pretty_print(settings.get_pretty_print_mode(),  {"ID", "PATH"}, data )
end)
cli.register(clistate, "e", "~*:arg:echo OK.", function(input) 
    local cwd = utils.path_cwd()
    utils.path_chdir(gDirs.local_build)
    os.execute(input.parsed['arg'])
    utils.path_chdir(cwd)
end)

-- register cli modules
for actionName, action in pairs(actions) do
    action.cli_register(clistate)
end
settings.cli_register(clistate)
sources.cli_register(clistate)
compilers.cli_register(clistate)
modules.cli_register(clistate)

function run(input)
    local parse = cli.parse(clistate, input)

    utils.try(function() 
        cli.execute(clistate, parse)
    end, function(err)
        print("error occurred:", err) -- NDEBUG
    end)
end

-- handle cli
local clb = cli.initAutocompleteCallbacks(clistate);
g_running = true
g_reload = false
while g_running do
    local input = cli.input(clistate, clb)
    
    if input == nil and cli.isUserInput() == false then break end
    if cli.isUserInput() then
        log_time_reset()
    end

    run(input)
end

if g_reload then
    utils.modules_unload()
    collectgarbage()
    dofile(utils.path_merge(gDirs.script, "main.lua"))
end


