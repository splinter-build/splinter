#!/bin/bash

mkdir -p .pkgsrc
pushd .pkgsrc

download_pkg()
{
    if [ ! -f "$1/.git/fulldl" ]; then
        rm -Rf $1
        git clone --depth 1 $2 $1 || exit 1
        touch $1/.git/fulldl
    else
        pushd $1; git pull; popd
    fi
}

#download 
download()
{
    download_pkg luajit "https://gitlab.com/splinter-build/externals/luajit.git"
    download_pkg luamodules "https://gitlab.com/splinter-build/externals/luamodules.git"
    download_pkg ninja "https://gitlab.com/splinter-build/externals/ninja.git"
}

if [ "$1" == "download" ]; then
shift
download
fi

# build
export MACOSX_DEPLOYMENT_TARGET=10.9

pushd luajit
make -j4 ${@} || exit 1
popd
pushd luamodules
make -j4 ${@} || exit 2
popd
pushd ninja
make -j4 ${@} || exit 3
popd
export PKG_FOLDER=../pkg
rm -Rf $PKG_FOLDER/*.so
rm -Rf $PKG_FOLDER/luajit
rm -Rf $PKG_FOLDER/scpp
mkdir -p $PKG_FOLDER
cp -f luajit/src/luajit $PKG_FOLDER
cp -f luajit/src/libluajit.so $PKG_FOLDER
cp -f luamodules/bin/*.dylib $PKG_FOLDER
cp -f ninja/bin/ninja $PKG_FOLDER
popd

