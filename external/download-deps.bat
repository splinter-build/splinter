@echo off
set URL=https://gitlab.com/api/v4/projects

taskkill /F /IM luajit.exe /T

pushd %~dp0
mkdir pkg
pushd pkg

REM continue..
..\wget\wget "http://splinter-build.gitlab.io/read_token.txt" -O TOKEN.TXT
set /p TOKEN=<TOKEN.TXT

..\wget\wget "%URL%/splinter-build%%2Fexternals%%2Fluajit/jobs/artifacts/v2.1/download?private_token=%TOKEN%&job=build-windows" -O luajit.zip
..\wget\wget "%URL%/splinter-build%%2Fexternals%%2Fluamodules/jobs/artifacts/master/download?private_token=%TOKEN%&job=build-windows" -O modules.zip
..\wget\wget "%URL%/splinter-build%%2Fexternals%%2Fninja/jobs/artifacts/master/download?private_token=%TOKEN%&job=build-windows" -O ninja.zip

..\unzip\unzip -o luajit.zip
..\unzip\unzip -o modules.zip
..\unzip\unzip -o ninja.zip

del /q /f *.zip
move /Y bin\*.dll .
move /Y bin\*.exe .

popd
popd
