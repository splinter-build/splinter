#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR="${DIR}/.."

# mac support
if [[ "$OSTYPE" == "darwin"* ]]; then
    pushd "$DIR/external"
    chmod a+x build-deps-macosx.sh
    ./build-deps-macosx.sh download "${@}" || exit 1
    exit 0
    popd
fi

if [ "$TARGET" == "" ]; then
	export ARCH=`uname -m`
	export TARGET=linux
	if [ "$ARCH" == "armv7l" ]; then export TARGET=linux-armv7; fi
	if [ "$ARCH" == "armv8" ]; then export TARGET=linux-arm64; fi
	if [ "$ARCH" == "aarch64" ]; then export TARGET=linux-arm64; fi
fi
export URL=https://gitlab.com/api/v4/projects

die()
{
	echo -- ERROR: ${@}
	exit 1
}

# Detect unzip, or use built in one
which unzip > /dev/null || die This script requires unzip to be installed!

# Detect wget
which wget > /dev/null || die This script requires wget to be installed!

# Grab token
export TOKEN=`wget http://splinter-build.gitlab.io/read_token.txt -O - 2> /dev/null`

if [[ "$TOKEN" == "" ]]; then die "Could not download read-only token."; fi

download_pkg()
{
    mkdir -p ".${1}"
    pushd ".${1}" &> /dev/null
    wget --no-check-certificate -nv -N "$URL/${2}/jobs/artifacts/${3}/download?private_token=${TOKEN}&job=build-${TARGET}" -O ${1}.zip &> /dev/null
    unzip -o ${1}.zip > /dev/null
    popd &> /dev/null
}

download()
{
    mkdir -p "$DIR/external/pkg"
    pushd "$DIR/external/pkg" &> /dev/null

    ( download_pkg luajit "splinter-build%2Fexternals%2Fluajit" v2.1 ) &
    ( download_pkg luamodules "splinter-build%2Fexternals%2Fluamodules" master ) &
    ( download_pkg ninja "splinter-build%2Fexternals%2Fninja" master ) &

    wait
    mv -f .luajit/bin/* . &> /dev/null
    mv -f .luamodules/bin/* . &> /dev/null
    mv -f .luamodules/misc . &> /dev/null
    mv -f .ninja/bin/* . &> /dev/null
    rm -Rf .luajit
    rm -Rf .luamodules
    rm -Rf .ninja
    if [ "$TARGET" == "linux" ]; then
       chmod a+x luajit
       chmod a+x ninja
    fi
    popd &> /dev/null
}

download

